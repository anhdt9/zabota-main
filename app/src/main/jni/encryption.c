//
// Created by Tuan Nguyen on 1/25/16.
//

#include "encryption.h"
#include "md5.h"
#include <inttypes.h>

const char *salt = ")BSz,{,zKj)xXwP0\\ej";
static char *m;

//#define LOG_TAG "AAA"
//
//#define LOGD(...) __android_log_write(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
//#define LOGI(...) __android_log_write(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
//#define LOGE(...) __android_log_write(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
//
//#define LOGDH(...) LOGD(__VA_ARGS__)
//#define LOGIH(...) LOGI(__VA_ARGS__)
//#define LOGEH(...) LOGE(__VA_ARGS__)

JNIEXPORT jstring JNICALL Java_vn_lovemoney_app_z_T_perform(
        JNIEnv *env, jobject clazz, jobject context, jlong src) {
    checkSignature(env, context);
    u_int64_t origin = (u_int64_t) src;
    u_int64_t b1 = fbc(origin);
    u_int64_t minify = origin * b1 + 64 - b1;
    const int n = snprintf(NULL, 0, "%" PRIu64 "", minify);
    char cstr[n + 1];
    int c = snprintf(cstr, (size_t) (n+1), "%" PRIu64 "", minify);

    size_t strLen = strlen(cstr);
    size_t saltLen = strlen(salt);
    char originalStr[256];
    strlcpy(originalStr, cstr, strLen + 1);
    strlcat(originalStr, salt, strLen + saltLen + 1);
    size_t len = strLen + saltLen;
    struct md5_ctx ctx;
    const char digest[16];
    md5_init(&ctx);
    ctx.size = len;
    strlcpy((char *) ctx.buf, originalStr, len + 1);
    md5_update(&ctx);
    md5_final((unsigned char *) digest, &ctx);
    char md5string[33];
    int i = 0;
    for (i = 0; i < 16; ++i)
        sprintf(&md5string[i * 2], "%02x", (unsigned char) digest[i]);
    return (*env)->NewStringUTF(env, md5string);
}

const char *loadSignature(JNIEnv *env, jobject obj) {
    jclass cls = (*env)->FindClass(env, "android/content/Context");
    jmethodID mid = (*env)->GetMethodID(env, cls, "getPackageManager",
                                        "()Landroid/content/pm/PackageManager;");
    if (mid == NULL) {
        return "";
    }

    jobject pm = (*env)->CallObjectMethod(env, obj, mid);
    if (pm == NULL) {
        return "";
    }

    mid = (*env)->GetMethodID(env, cls, "getPackageName", "()Ljava/lang/String;");
    if (mid == NULL) {
        return "";
    }
    jstring packageName = (jstring) (*env)->CallObjectMethod(env, obj, mid);
    cls = (*env)->GetObjectClass(env, pm);
    mid = (*env)->GetMethodID(env, cls, "getPackageInfo",
                              "(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");

    jobject packageInfo = (*env)->CallObjectMethod(env, pm, mid, packageName,
                                                   0x40); //GET_SIGNATURES = 64;

    cls = (*env)->GetObjectClass(env, packageInfo);
    jfieldID fid = (*env)->GetFieldID(env, cls, "signatures", "[Landroid/content/pm/Signature;");
    jobjectArray signatures = (jobjectArray) (*env)->GetObjectField(env, packageInfo, fid);
    jobject sign = (*env)->GetObjectArrayElement(env, signatures, 0);

    cls = (*env)->GetObjectClass(env, sign);
    mid = (*env)->GetMethodID(env, cls, "toCharsString", "()Ljava/lang/String;");
    if (mid == NULL) {
        return "";
    }

    jstring signString = (jstring) (*env)->CallObjectMethod(env, sign, mid);

    return (*env)->GetStringUTFChars(env, signString, JNI_FALSE);
}

char *getSignatureMd5(JNIEnv *env, jobject obj) {
    const char *sign = loadSignature(env, obj);
    struct md5_ctx context;
    unsigned char dest[16];
    md5_init(&context);
    size_t len = strlen(sign);
    context.size = len;
    strlcpy((char *) context.buf, sign, len + 1);
    md5_update(&context);
    md5_final(dest, &context);

    static char destination[33];
    int i;
    for (i = 0; i < 16; ++i)
        sprintf(&destination[i * 2], "%02x", (unsigned char) dest[i]);
    return destination;
}

void checkSignature(JNIEnv *env, jobject obj) {
    if (m == NULL) {
        m = getSignatureMd5(env, obj);
    }
    const char *vd = "d002359c2fec2269969a84de9cd382d6";
    const char *vr = "533cec6554de1573d44efe0898b1c78c";
    if (strcmp(m, vd) != 0 &&
        strcmp(m, vr) != 0) {
        exit(1);
    }
}

u_int64_t fbc(u_int64_t data) {
    u_int64_t temp = data;
    temp = (temp & 0x5555555555555555) + ((temp >> 1) & 0x5555555555555555);
    temp = (temp & 0x3333333333333333) + ((temp >> 2) & 0x3333333333333333);
    temp = (temp & 0x0F0F0F0F0F0F0F0F) + ((temp >> 4) & 0x0F0F0F0F0F0F0F0F);
    temp = (temp & 0x00FF00FF00FF00FF) + ((temp >> 8) & 0x00FF00FF00FF00FF);
    temp = (temp & 0x0000FFFF0000FFFF) + ((temp >> 16) & 0x0000FFFF0000FFFF);
    temp = (temp & 0x00000000FFFFFFFF) + ((temp >> 32) & 0x00000000FFFFFFFF);
    return temp;
}
