//
// Created by Tuan Nguyen on 1/25/16.
//

#ifndef ENCRYPTION_H
#define ENCRYPTION_H
#include <jni.h>
#include <stdio.h>
JNIEXPORT jstring JNICALL Java_vn_lovemoney_app_z_T_perform(JNIEnv *env, jobject clazz, jobject context, jlong src);
void checkSignature(JNIEnv* env, jobject obj);

uint64_t fbc(uint64_t data);
#endif //ENCRYPTION_H
