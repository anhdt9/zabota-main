package vn.lovemoney.app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import vn.lovemoney.app.util.AlarmServiceUtil;

/**
 * Created by tuannguyen on 4/14/16.
 */
public class BootReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if(action.equals(Intent.ACTION_BOOT_COMPLETED)){
			AlarmServiceUtil.getInstance(context).startScheduleLocalPush();
		}
	}
}
