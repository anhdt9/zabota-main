package vn.lovemoney.app.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateUtils;
import android.util.Log;

import java.util.Calendar;

import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.SplashActivity;
import vn.lovemoney.app.model.other.RemoteConfig;
import vn.lovemoney.app.util.PreferenceManager;

/**
 * Created by tuannguyen on 4/14/16.
 */
public class LocalPushReceiver extends BroadcastReceiver {

    private static final int PUSH_ID = 0x1229;

    @Override
    public void onReceive(Context context, Intent intent) {
        String remindMessage = context.getString(R.string.msg_remind);
        RemoteConfig config = PreferenceManager.getInstance(context).getRemoteConfig();
        if (config != null) {
            remindMessage = config.getRemindMessage();
        }
        Calendar lastAttendance = PreferenceManager.getInstance(context).getLastLuckySubmit();
        if (lastAttendance == null || !DateUtils.isToday(lastAttendance.getTimeInMillis())) {
            Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_onesignal_large_icon_default);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_stat_onesignal_default)
                    .setLargeIcon(bm)
                    .setColor(ContextCompat.getColor(context,R.color.primary))
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(remindMessage)
                    .setAutoCancel(true);
            Intent resultIntent = new Intent(context, SplashActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(PUSH_ID, builder.build());
            PreferenceManager.getInstance(context).saveLastRemind(Calendar.getInstance());
        }
    }
}
