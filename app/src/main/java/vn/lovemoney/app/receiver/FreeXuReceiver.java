package vn.lovemoney.app.receiver;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import vn.lovemoney.app.model.AdditionData;
import vn.lovemoney.app.model.Notification;
import vn.lovemoney.app.model.NotificationData;
import vn.lovemoney.app.network.GsonSingleton;
import vn.lovemoney.app.service.OfferManagerService;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by tuannguyen on 12/14/15.
 */
public class FreeXuReceiver extends WakefulBroadcastReceiver {

	private static final String TAG = FreeXuReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (Intent.ACTION_PACKAGE_ADDED.equals(action)) {
			Intent service = new Intent(context, OfferManagerService.class);
			Uri data = intent.getData();
			String packageName = data.getEncodedSchemeSpecificPart();
			service.putExtra(ArgumentKeys.PACKAGE_NAME, packageName);
			startWakefulService(context, service);
		} else if(Intent.ACTION_PACKAGE_REMOVED.equals(action)){

		} else if(action.equals("com.onesignal.BackgroundBroadcast.RECEIVE")){
			PreferenceManager.getInstance(context).increaseUnreadNotificationCount();
			Bundle data = intent.getBundleExtra("data");
			if(data != null) {
				String json = data.getString("custom", "");
				if(!TextUtils.isEmpty(json)) {
					AdditionData additionData = GsonSingleton.getInstance().fromJson(json, AdditionData.class);
					if(additionData != null) {
						NotificationData notificationData = additionData.getNotificationData();
						if (notificationData != null) {
							int actionCode = notificationData.getAction();
							if (actionCode == Notification.ACTION_REFERRAL) {
								Singleton.getInstance().setShouldUpdateProfile(true);
								EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
							}
						}
					}
				}
			}
		}
	}
}
