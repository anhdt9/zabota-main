package vn.lovemoney.app.model;

import android.text.format.DateUtils;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by tuannguyen on 1/8/16.
 */
public class Notification {

	private static final SimpleDateFormat TIME_ONLY_FORMATTER = new SimpleDateFormat("hh:mm a");
	private static final SimpleDateFormat FULL_DATE_FORMATTER = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
	public static final int ACTION_REFERRAL = 1;
	public static final int ACTION_UPGRADE_VERSION = 2;

	private int id;
	private String content;
	@SerializedName("created_at")
	private Calendar createdDate;

	public int getId() {
		return id;
	}

	public String getContent() {
		return content;
	}

	public Calendar getCreatedDate() {
		return createdDate;
	}

	public String getCreatedDateString(){
		Calendar cal = createdDate;
		Date date = cal.getTime();
		if(DateUtils.isToday(cal.getTimeInMillis())){
			return TIME_ONLY_FORMATTER.format(date);
		} else{
			return FULL_DATE_FORMATTER.format(date);
		}
	}
}
