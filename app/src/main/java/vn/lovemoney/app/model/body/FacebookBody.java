package vn.lovemoney.app.model.body;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public class FacebookBody {
    private String zabotaFacebookId;
    private String campaignId;

    FacebookBody(String zabotaFacebookId, String campaignId) {
        this.zabotaFacebookId = zabotaFacebookId;
        this.campaignId = campaignId;
    }
}
