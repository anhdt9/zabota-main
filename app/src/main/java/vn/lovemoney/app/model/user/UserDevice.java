package vn.lovemoney.app.model.user;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 8470p on 4/1/2017.
 */

public class UserDevice {
    private String token;
    private String name;
    @SerializedName("avatar_url")
    private String avatarUrl;
    private int balance;
    @SerializedName("referral_code")
    private String referralCode;

    public String getToken() {
        return token;
    }

    public String getName() {
        return name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public int getBalance() {
        return balance;
    }

    public String getReferralCode(){
        return referralCode;
    }
}
