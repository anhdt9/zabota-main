package vn.lovemoney.app.model.body;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public class AppBody {
    private int appId;
    private int campaignId;

    public AppBody(int appId, int campaignId) {
        this.appId = appId;
        this.campaignId = campaignId;
    }
}
