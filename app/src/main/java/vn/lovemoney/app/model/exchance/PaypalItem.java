package vn.lovemoney.app.model.exchance;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 8470p on 5/16/2017.
 */

public class PaypalItem implements Parcelable {
    private int id;
    private int credit;
    private int exchangePrice;
    private String providerName;
    private String logoUrl;
    private String currency;

    public int getId() {
        return id;
    }

    public int getCredit() {
        return credit;
    }

    public int getExchangePrice() {
        return exchangePrice;
    }

    public String getProviderName() {
        return providerName;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public String getCurrency() {
        return currency;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    protected PaypalItem(Parcel in) {
        id = in.readInt();
        credit = in.readInt();
        exchangePrice = in.readInt();
        providerName = in.readString();
        logoUrl = in.readString();
        currency = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(credit);
        dest.writeInt(exchangePrice);
        dest.writeString(providerName);
        dest.writeString(logoUrl);
        dest.writeString(currency);
    }

    public static final Creator<PaypalItem> CREATOR = new Creator<PaypalItem>() {
        @Override
        public PaypalItem createFromParcel(Parcel in) {
            return new PaypalItem(in);
        }

        @Override
        public PaypalItem[] newArray(int size) {
            return new PaypalItem[size];
        }
    };
}
