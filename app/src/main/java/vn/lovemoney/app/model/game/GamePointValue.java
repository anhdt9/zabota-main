package vn.lovemoney.app.model.game;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Laptop TCC on 5/16/2017.
 */

public class GamePointValue implements Parcelable {
    private String label;
    private int value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    protected GamePointValue(Parcel in) {
        label = in.readString();
        value = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeInt(value);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GamePointValue> CREATOR = new Parcelable.Creator<GamePointValue>() {
        @Override
        public GamePointValue createFromParcel(Parcel in) {
            return new GamePointValue(in);
        }

        @Override
        public GamePointValue[] newArray(int size) {
            return new GamePointValue[size];
        }
    };
}
