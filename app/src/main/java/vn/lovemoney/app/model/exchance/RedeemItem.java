package vn.lovemoney.app.model.exchance;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public class RedeemItem implements Parcelable {
    private int id;
    private String providerName;
    private int credit;
    private int exchangePrice;
    private String logoUrl;
    private String currency;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getExchangePrice() {
        return exchangePrice;
    }

    public void setExchangePrice(int exchangePrice) {
        this.exchangePrice = exchangePrice;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected RedeemItem(Parcel in) {
        id = in.readInt();
        providerName = in.readString();
        credit = in.readInt();
        exchangePrice = in.readInt();
        logoUrl = in.readString();
        currency = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(providerName);
        dest.writeInt(credit);
        dest.writeInt(exchangePrice);
        dest.writeString(logoUrl);
        dest.writeString(currency);
    }

    public static final Creator<RedeemItem> CREATOR = new Creator<RedeemItem>() {
        @Override
        public RedeemItem createFromParcel(Parcel in) {
            return new RedeemItem(in);
        }

        @Override
        public RedeemItem[] newArray(int size) {
            return new RedeemItem[size];
        }
    };
}
