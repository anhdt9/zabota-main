package vn.lovemoney.app.model.android.app;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 8470p on 4/1/2017.
 */

public class NewAppOffer implements Parcelable {

    public static final int STATUS_NOT_REQUESTED = -1;
    public static final int STATUS_REQUESTED = 0;
    public static final int STATUS_INSTALLED = 1;
    public static final int STATUS_VERIFIED = 2;

    private int appId;
    private String name;
    private int price;
    private int campaignId;
    private String iconUrl;
    private String packageBundle;
    private int status;
    private boolean reviewed;
    private int bonus;
    private String guide;

    public int getAppId() {
        return appId;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getPackageBundle() {
        return packageBundle;
    }

    public int getStatus() {
        return status;
    }

    public boolean isReviewed() {
        return reviewed;
    }

    public int getBonus() {
        return bonus;
    }

    public String getGuide() {
        return guide;
    }

    public NewAppOffer() {

    }

    protected NewAppOffer(Parcel in) {
        appId = in.readInt();
        name = in.readString();
        price = in.readInt();
        campaignId = in.readInt();
        iconUrl = in.readString();
        packageBundle = in.readString();
        status = in.readInt();
        reviewed = in.readByte() != 0x00;
        bonus = in.readInt();
        guide = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(appId);
        dest.writeString(name);
        dest.writeInt(price);
        dest.writeInt(campaignId);
        dest.writeString(iconUrl);
        dest.writeString(packageBundle);
        dest.writeInt(status);
        dest.writeByte((byte) (reviewed ? 0x01 : 0x00));
        dest.writeInt(bonus);
        dest.writeString(guide);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<NewAppOffer> CREATOR = new Parcelable.Creator<NewAppOffer>() {
        @Override
        public NewAppOffer createFromParcel(Parcel in) {
            return new NewAppOffer(in);
        }

        @Override
        public NewAppOffer[] newArray(int size) {
            return new NewAppOffer[size];
        }
    };

    @Override
    public String toString() {
        return "NewAppOffer{" +
                "appId=" + appId +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", campaignId=" + campaignId +
                ", iconUrl='" + iconUrl + '\'' +
                ", packageBundle='" + packageBundle + '\'' +
                ", status=" + status +
                ", reviewed=" + reviewed +
                ", bonus=" + bonus +
                ", guide='" + guide + '\'' +
                '}';
    }
}
