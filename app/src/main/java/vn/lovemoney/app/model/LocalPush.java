package vn.lovemoney.app.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tuannguyen on 4/15/16.
 */
public class LocalPush implements Parcelable {
	private int id;
	private String message;
	private int start;
	private int end;

	public int getId() {
		return id;
	}

	public String getMessage() {
		return message;
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	protected LocalPush(Parcel in) {
		id = in.readInt();
		message = in.readString();
		start = in.readInt();
		end = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(message);
		dest.writeInt(start);
		dest.writeInt(end);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<LocalPush> CREATOR = new Parcelable.Creator<LocalPush>() {
		@Override
		public LocalPush createFromParcel(Parcel in) {
			return new LocalPush(in);
		}

		@Override
		public LocalPush[] newArray(int size) {
			return new LocalPush[size];
		}
	};
}