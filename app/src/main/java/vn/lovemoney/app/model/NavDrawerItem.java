package vn.lovemoney.app.model;

import android.graphics.drawable.Drawable;

/**
 * Created by Ravi on 29/07/15.
 */
public class NavDrawerItem {
    private boolean showNotify;
    private String title;
    private Drawable img;


    public NavDrawerItem() {

    }

    public String getTitle() {
        return title;
    }
    public Drawable getImg() {
        return img;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public void setImg(Drawable img) {
        this.img = img;
    }

}
