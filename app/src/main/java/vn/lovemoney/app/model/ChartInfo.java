package vn.lovemoney.app.model;

/**
 * Created by tuandigital on 8/25/16.
 */
public class ChartInfo {
    private String chartName;
    private String displayName;

    public String getChartName() {
        return chartName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setChartName(String chartName) {
        this.chartName = chartName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
