package vn.lovemoney.app.model.navigationdrawer.mail.detail;

/**
 * Created by DTA on 16/03/2017.
 */

public class Message {
    private String mail;
    private String date;
    private String hour;
    private boolean isClient;

    public Message(String mail, String date, String hour, boolean isClient) {
        this.mail = mail;
        this.date = date;
        this.hour = hour;
        this.isClient = isClient;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public boolean isClient() {
        return isClient;
    }

    public void setClient(boolean client) {
        isClient = client;
    }
}
