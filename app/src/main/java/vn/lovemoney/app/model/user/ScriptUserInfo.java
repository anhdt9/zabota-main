package vn.lovemoney.app.model.user;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 8470p on 4/1/2017.
 */

public class ScriptUserInfo {
    private boolean me;
    private String name;
    private String avatarUrl;
    @SerializedName("earning")
    private int totalEarning;
    private int rank;

    public String getName() {
        return name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public int getTotalEarning() {
        return totalEarning;
    }

    public boolean isMe() {
        return me;
    }

    public int getRank() {
        return rank;
    }
}
