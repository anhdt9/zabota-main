package vn.lovemoney.app.model.bonus.coin.other;

import android.graphics.drawable.Drawable;

/**
 * Created by DTA on 15/03/2017.
 */

public class ShareItem {
    private Drawable ic;
    private String name;
    private boolean isCheck;

    public ShareItem(Drawable ic, String name, boolean isCheck) {
        this.ic = ic;
        this.name = name;
        this.isCheck = isCheck;
    }

    public Drawable getIc() {
        return ic;
    }

    public void setIc(Drawable ic) {
        this.ic = ic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }
}
