package vn.lovemoney.app.model.exchance;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public class ExchangeItem implements Parcelable {

    public static final int TYPE_MOBILE_CARD = 0;
    public static final int TYPE_SERVICE_CARD = 1;
    public static final int TYPE_MONEY = 2;

    private String name;
    private String pin;
    private String serial;
    private int type;
    private Calendar expiredAt;

    public String getName() {
        return name;
    }

    public String getPin() {
        return pin;
    }

    public String getSerial() {
        return serial;
    }

    public Calendar getExpiredAt(){
        return expiredAt;
    }

    public int getType() {
        return type;
    }

    protected ExchangeItem(Parcel in) {
        name = in.readString();
        pin = in.readString();
        serial = in.readString();
        type = in.readInt();
        expiredAt = (Calendar) in.readValue(Calendar.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(pin);
        dest.writeString(serial);
        dest.writeInt(type);
        dest.writeValue(expiredAt);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ExchangeItem> CREATOR = new Parcelable.Creator<ExchangeItem>() {
        @Override
        public ExchangeItem createFromParcel(Parcel in) {
            return new ExchangeItem(in);
        }

        @Override
        public ExchangeItem[] newArray(int size) {
            return new ExchangeItem[size];
        }
    };
}