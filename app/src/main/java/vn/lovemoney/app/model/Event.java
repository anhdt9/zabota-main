package vn.lovemoney.app.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;

import com.google.gson.annotations.SerializedName;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by tuannguyen on 3/3/16.
 */
public class Event implements Parcelable {

	public static final int TYPE_NONE = 0;
	public static final int TYPE_ANSWER = 1;
	public static final int TYPE_SHARE_FACEBOOK = 2;
	public static final int TYPE_SHARE_APP = 3;
	public static final int TYPE_SHARE_LOVEMONEY = 4;
	public static final int TYPE_APP_INVITE = 5;
	public static final int TYPE_LIKE_FANPAGE = 6;
	public static final int TYPE_LOTTERY = 7;
	public static final int TYPE_PROMOTION = 8;
	public static final int TYPE_ADD_GOOGLE_PLUS = 10;
	public static final int TYPE_QUESTION_GAME = 11;
	public static final int TYPE_GET_FREE_COIN = 12;
	public static final int TYPE_PLUS_GOOGLE = 13;

	@IntDef({TYPE_NONE, TYPE_ANSWER, TYPE_SHARE_FACEBOOK, TYPE_SHARE_APP, TYPE_SHARE_LOVEMONEY, TYPE_APP_INVITE, TYPE_LIKE_FANPAGE, TYPE_LOTTERY, TYPE_PROMOTION})
	@Retention(RetentionPolicy.SOURCE)
	@interface EventTypes {
	}

	private int id;
	@SerializedName("icon_url")
	private String iconUrl;
	private String title;
	private String guide;
	private String description;
	private String data;
	private int bonus = 0;
	private String uri;
	private String action;
	@EventTypes
	private int type;
	private String clipboard;

	public int getId() {
		return id;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public String getTitle() {
		return title;
	}

	public String getGuide() {
		return guide;
	}

	public String getDescription() {
		return description;
	}

	public String getData() {
		return data;
	}

	public int getBonus() {
		return bonus;
	}

	public String getUri() {
		return uri;
	}

	public String getAction() {
		return action;
	}

	public String getClipboard() {
		return clipboard;
	}

	@EventTypes
	public int getType() {
		return type;
	}

	protected Event(Parcel in) {
		id = in.readInt();
		iconUrl = in.readString();
		title = in.readString();
		guide = in.readString();
		description = in.readString();
		data = in.readString();
		bonus = in.readInt();
		uri = in.readString();
		action = in.readString();
		type = in.readInt();
		clipboard = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(iconUrl);
		dest.writeString(title);
		dest.writeString(guide);
		dest.writeString(description);
		dest.writeString(data);
		dest.writeInt(bonus);
		dest.writeString(uri);
		dest.writeString(action);
		dest.writeInt(type);
		dest.writeString(clipboard);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
		@Override
		public Event createFromParcel(Parcel in) {
			return new Event(in);
		}

		@Override
		public Event[] newArray(int size) {
			return new Event[size];
		}
	};
}