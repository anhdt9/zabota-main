package vn.lovemoney.app.model;

/**
 * Created by tuannguyen on 12/14/15.
 */
public class BaseObjectResponse<T> extends BaseResponse {

	private T result;

	public T getResult() {
		return result;
	}
}
