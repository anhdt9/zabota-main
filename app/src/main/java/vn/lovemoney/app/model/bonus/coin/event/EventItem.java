package vn.lovemoney.app.model.bonus.coin.event;

/**
 * Created by DTA on 14/03/2017.
 */

public class EventItem {

    private int id;
    private String title;
    private String guide;
    private String iconUrl;
    private String endAt;
    private int bonus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public int getBonus() {
        return bonus;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getEndAt() {
        return endAt;
    }

    public void setEndAt(String endAt) {
        this.endAt = endAt;
    }
}
