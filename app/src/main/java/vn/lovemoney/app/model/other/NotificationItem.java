package vn.lovemoney.app.model.other;

/**
 * Created by 8470p on 5/16/2017.
 */

public class NotificationItem {

    private int id;
    private String message;
    private String uri;
    private String createdAt;
    private int seen;

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getUri() {
        return uri;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public int getSeen() {
        return seen;
    }

    public void setSeen(int seen) {
        this.seen = seen;
    }

}
