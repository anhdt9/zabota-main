package vn.lovemoney.app.model;

/**
 * Created by tuannguyen on 1/26/16.
 */
public class NotificationData {

	private int action;

	private int latestVersion;

	public int getAction() {
		return action;
	}

	public int getLatestVersion() {
		return latestVersion;
	}
}
