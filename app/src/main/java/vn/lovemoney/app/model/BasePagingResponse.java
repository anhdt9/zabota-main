package vn.lovemoney.app.model;

import java.util.List;

/**
 * Created by tuannguyen on 12/15/15.
 */
public class BasePagingResponse<T> extends BaseResponse {

    private int pageSize;
    private int pageIndex;
    private int itemCount;
    private List<T> result;
    private int totalUnseenCount;

    public int getPageSize() {
        return pageSize;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public int getItemCount() {
        return itemCount;
    }

    public List<T> getResult() {
        return result;
    }

    public int getTotalUnseenCount() {
        return totalUnseenCount;
    }
}
