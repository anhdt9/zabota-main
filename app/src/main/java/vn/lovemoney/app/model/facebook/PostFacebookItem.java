package vn.lovemoney.app.model.facebook;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public class PostFacebookItem {
    private String content;
    private int bonus;

    public String getContent() {
        return content;
    }

    public int getBonus() {
        return bonus;
    }
}
