package vn.lovemoney.app.model.game;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Laptop TCC on 5/6/2017.
 */

public class GameItem implements Parcelable {
    private String label;
    private int value;
    private GamePointValue[] data;

    public GameItem(String label, int value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public GamePointValue[] getData() {
        return data;
    }

    public String[] getLabels(){
        String[] labels = new String[data.length];
        for (int i = 0; i < data.length; i++) {
            labels[i] = data[i].getLabel();
        }
        return labels;
    }

    public int getResultIndex() {
        for (int i = 0; i < data.length; i++) {
            if (value == data[i].getValue()) {
                return i;
            }
        }
        return 0;
    }

    protected GameItem(Parcel in) {
        label = in.readString();
        value = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeInt(value);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GameItem> CREATOR = new Parcelable.Creator<GameItem>() {
        @Override
        public GameItem createFromParcel(Parcel in) {
            return new GameItem(in);
        }

        @Override
        public GameItem[] newArray(int size) {
            return new GameItem[size];
        }
    };
}
