package vn.lovemoney.app.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tuannguyen on 12/15/15.
 */
public class BasePagingRequest {
	private int pageIndex;
	private int pageSize;

	public BasePagingRequest(int pageIndex, int pageSize) {
		this.pageIndex = pageIndex;
		this.pageSize = pageSize;
	}

	public void reset(){
		pageIndex = 0;
	}

	public void nextPage(){
		pageIndex++;
	}

	public Map<String, String> toQueryMap(){
		HashMap<String, String> ret = new HashMap<>();
		ret.put("pageSize", String.valueOf(pageSize));
		ret.put("pageIndex", String.valueOf(pageIndex));
		return ret;
	}

}
