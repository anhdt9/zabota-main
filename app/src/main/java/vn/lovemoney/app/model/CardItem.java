package vn.lovemoney.app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuannguyen on 12/17/15.
 */
class CardItem {
	private int id;
	private int value;
	@SerializedName("coin_price")
	private int coinPrice;

	public int getId() {
		return id;
	}

	public int getValue() {
		return value;
	}

	public int getCoinPrice() {
		return coinPrice;
	}
}
