package vn.lovemoney.app.model.facebook;

/**
 * Created by DTA on 21/04/2017.
 */

public class PutFacebookBody {
    private int campaignId;
    private String fbTransactionId;

    public PutFacebookBody(int campaignId, String fbTransactionId) {
        this.campaignId = campaignId;
        this.fbTransactionId = fbTransactionId;
    }
}
