package vn.lovemoney.app.model.user;

/**
 * Created by 8470p on 4/1/2017.
 */

public class ChangeDeviceRqBody {
    private String phone;
    private String token;

    public ChangeDeviceRqBody(String phone, String token) {
        this.phone = phone;
        this.token = token;
    }
}
