package vn.lovemoney.app.model;

/**
 * Created by tuannguyen on 12/16/15.
 */
public class CoinHistory {
	private String message;
	private int amount;
	private int prevBalance;
	private int newBalance;
	private int type;
	private String createdAt;
	private int status;

	public String getMessage() {
		return message;
	}

	public int getAmount() {
		return amount;
	}

	public int getPrevBalance() {
		return prevBalance;
	}

	public int getNewBalance() {
		return newBalance;
	}

	public int getType() {
		return type;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public int getStatus() {
		return status;
	}
}
