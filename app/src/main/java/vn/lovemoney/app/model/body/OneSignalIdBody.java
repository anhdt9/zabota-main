package vn.lovemoney.app.model.body;

/**
 * Created by Laptop TCC on 4/4/2017.
 */

public class OneSignalIdBody {
    String oneSignalId;

    public OneSignalIdBody(String oneSignalId) {
        this.oneSignalId = oneSignalId;
    }
}
