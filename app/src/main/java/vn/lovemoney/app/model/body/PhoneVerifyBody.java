package vn.lovemoney.app.model.body;

/**
 * Created by Laptop TCC on 4/3/2017.
 */

public class PhoneVerifyBody {
    String phone;
    String code;
    String fbId;
    String fbToken;

    public PhoneVerifyBody(String phone, String code) {
        this.phone = phone;
        this.code = code;
    }
    public PhoneVerifyBody(String fbId, String fbToken, String code) {
        this.fbId = fbId;
        this.fbToken = fbToken;
        this.code = code;
    }
}
