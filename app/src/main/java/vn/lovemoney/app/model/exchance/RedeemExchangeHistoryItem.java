package vn.lovemoney.app.model.exchance;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public class RedeemExchangeHistoryItem {
    private int id;
    private String name;
    private String pin;
    private String serial;
    private String createdAt;
    private String expiredAt;
    private int exchangePrice;
    private String iconUrl;
    private int status;
    private int type;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPin() {
        return pin;
    }

    public String getSerial() {
        return serial;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getExpiredAt() {
        return expiredAt;
    }

    public int getExchangePrice() {
        return exchangePrice;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public int getStatus() {
        return status;
    }

    public int getType() {
        return type;
    }
}
