package vn.lovemoney.app.model;

import java.util.List;

/**
 * Created by tuandigital on 8/24/16.
 */
public class FacebookAppRequest {
    private String requestId;
    private String[] recipients ;

    public FacebookAppRequest(String requestId, List<String> recipients) {
        this.requestId = requestId;
        this.recipients = new String[recipients.size()];
        recipients.toArray(this.recipients);
    }
}
