package vn.lovemoney.app.model.support;

/**
 * Created by dta on 30/04/2017.
 */

public class PostTicket {
    private int ticketId;

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }
}
