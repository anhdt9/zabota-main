package vn.lovemoney.app.model;

/**
 * Created by tuannguyen on 12/14/15.
 */
public class BaseResponse {

    private int error;
    private String message;
    private String displayMessage;

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getDisplayMessage() {
        return displayMessage;
    }

}
