package vn.lovemoney.app.model.user;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 8470p on 4/1/2017.
 */

public class ReOpenApp implements Parcelable {
    private int appId;
    private int campaignId;
    private String packageBundle;
    private String installationLink;
    private String name;
    private int remainHours;
    private String iconUrl;
    private int openedCount;
    private int[] openAppBonuses;
    private int currentBonus;

    public int getAppId() {
        return appId;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public String getPackageBundle() {
        return packageBundle;
    }

    public String getInstallationLink() {
        return installationLink;
    }

    public String getName() {
        return name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public int getOpenedCount() {
        return openedCount;
    }

    public int[] getOpenAppBonuses() {
        return openAppBonuses;
    }

    public int getRemainHour() {
        return remainHours;
    }

    public int getCurrentBonus() {
        return currentBonus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected ReOpenApp(Parcel in) {
        appId = in.readInt();
        campaignId = in.readInt();
        packageBundle = in.readString();
        installationLink = in.readString();
        name = in.readString();
        remainHours = in.readInt();
        iconUrl = in.readString();
        openedCount = in.readInt();
        openAppBonuses = in.createIntArray();
        currentBonus = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(appId);
        dest.writeInt(campaignId);
        dest.writeString(packageBundle);
        dest.writeString(installationLink);
        dest.writeString(name);
        dest.writeInt(remainHours);
        dest.writeString(iconUrl);
        dest.writeInt(openedCount);
        dest.writeIntArray(openAppBonuses);
        dest.writeInt(currentBonus);
    }


    public static final Creator<ReOpenApp> CREATOR = new Creator<ReOpenApp>() {
        @Override
        public ReOpenApp createFromParcel(Parcel in) {
            return new ReOpenApp(in);
        }

        @Override
        public ReOpenApp[] newArray(int size) {
            return new ReOpenApp[size];
        }
    };
}
