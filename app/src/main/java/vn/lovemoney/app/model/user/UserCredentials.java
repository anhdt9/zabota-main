package vn.lovemoney.app.model.user;

import static vn.lovemoney.app.R.string.rank;

/**
 * Created by 8470p on 3/31/2017.
 */

public class UserCredentials {
    private String id;
    private String token;
    private String name;
    private String avatarUrl;
    private int balance;
    private int earning;
    private String phone;
    private String referralCode;
    private boolean isRegister;
    private int verified;
    private boolean enteredReferral;
    private int rank;

    public boolean isRegister() {
        return isRegister;
    }

    public int getVerified() {
        return verified;
    }

    public boolean isVerified() {
        return verified == 1;
    }

    public String getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public String getName() {
        return name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public int getBalance() {
        return balance;
    }

    public int getEarning() {
        return earning;
    }

    public String getPhone() {
        return phone;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public boolean isEnteredReferral() {
        return enteredReferral;
    }

    public int getRank() {
        return rank;
    }
}
