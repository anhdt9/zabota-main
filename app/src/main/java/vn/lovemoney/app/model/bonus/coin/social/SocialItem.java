package vn.lovemoney.app.model.bonus.coin.social;

import android.graphics.drawable.Drawable;

/**
 * Created by 8470p on 3/12/2017.
 */

public class SocialItem {

    private Drawable icon;
    private Drawable nextIcon;
    private String title;

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public Drawable getNextIcon() {
        return nextIcon;
    }

    public void setNextIcon(Drawable nextIcon) {
        this.nextIcon = nextIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

