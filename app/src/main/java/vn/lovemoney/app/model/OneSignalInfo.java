package vn.lovemoney.app.model;

/**
 * Created by tuannguyen on 1/11/16.
 */
public class OneSignalInfo {
	private String oneSignalId;
	private String registrationId;

	public OneSignalInfo(String oneSignalId, String registrationId) {
		this.oneSignalId = oneSignalId;
		this.registrationId = registrationId;
	}

	public String getOneSignalId() {
		return oneSignalId;
	}
}
