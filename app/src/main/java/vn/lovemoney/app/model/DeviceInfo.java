package vn.lovemoney.app.model;

import java.util.List;

/**
 * Created by tuannguyen on 12/25/15.
 */
public class DeviceInfo {
	private String model;
	private String product;
	private String osVersion;
	private String device;
	private String deviceId;
	private String imei;
	private int sdkVersion;
	private String manufacture;
	private String phoneNumber;
	private String email;
	private List<String> emails;

	public void setModel(String model) {
		this.model = model;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public void setSdkVersion(int sdkVersion) {
		this.sdkVersion = sdkVersion;
	}

	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmails(List<String> emails){
		this.emails = emails;
	}
}
