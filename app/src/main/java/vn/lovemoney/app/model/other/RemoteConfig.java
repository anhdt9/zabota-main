package vn.lovemoney.app.model.other;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuandigital on 6/12/17.
 */

public class RemoteConfig {
    private int latestVersion;
    private String remindMessage;

    public int getLatestVersion() {
        return latestVersion;
    }

    public String getRemindMessage() {
        return remindMessage;
    }
}
