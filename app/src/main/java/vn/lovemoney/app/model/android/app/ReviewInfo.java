package vn.lovemoney.app.model.android.app;

/**
 * Created by 8470p on 4/16/2017.
 */

public class ReviewInfo {
    private String reviewUrl;
    private String content;

    public String getReviewUrl() {
        return reviewUrl;
    }

    public String getContent() {
        return content;
    }
}
