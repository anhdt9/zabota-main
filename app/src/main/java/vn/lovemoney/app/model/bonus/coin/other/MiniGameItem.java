package vn.lovemoney.app.model.bonus.coin.other;

/**
 * Created by DTA on 15/03/2017.
 */

public class MiniGameItem {
    private int iconResId;
    private String name;

    public MiniGameItem(int iconResId, String name) {
        this.iconResId = iconResId;
        this.name = name;
    }

    public int getIconResId() {
        return iconResId;
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
