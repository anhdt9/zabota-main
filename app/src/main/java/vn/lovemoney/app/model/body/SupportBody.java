package vn.lovemoney.app.model.body;

/**
 * Created by dta on 30/04/2017.
 */

public class SupportBody {
    private int threadId;
    private String userFacebookId;
    private String content;

    public SupportBody(int threadId, String userFacebookId) {
        this.threadId = threadId;
        this.userFacebookId = userFacebookId;
    }

    public SupportBody(String content) {
        this.content = content;
    }

    public SupportBody(String userFacebookId, String content) {
        this.userFacebookId = userFacebookId;
        this.content = content;
    }

    public SupportBody(int threadId, String userFacebookId, String content) {
        this.threadId = threadId;
        this.userFacebookId = userFacebookId;
        this.content = content;
    }
}
