package vn.lovemoney.app.model.facebook;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public class FacebookItem implements Parcelable {


    public static final int ACTION_LIKE = 1;
    public static final int ACTION_COMMENT = 2;
    public static final int ACTION_SHARE = 3;
    public static final int ACTION_REVIEW = 4;

    public static final int TYPE_POST = 1;
    public static final int TYPE_PAGE = 2;
    public static final int TYPE_GROUP = 3;

    private int campaignId;
    private String name;
    private int type;
    private String guide;
    private String url;
    private String facebookObjectId;
    private int bonus;
    private String iconUrl;

    public int getCampaignId() {
        return campaignId;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public String getFacebookObjectId() {
        return facebookObjectId;
    }

    public int getBonus() {
        return bonus;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public FacebookItem(Parcel in) {
        campaignId = in.readInt();
        name = in.readString();
        type = in.readInt();
        facebookObjectId = in.readString();
        bonus = in.readInt();
        iconUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(campaignId);
        dest.writeString(name);
        dest.writeInt(type);
        dest.writeString(facebookObjectId);
        dest.writeInt(bonus);
        dest.writeString(iconUrl);

    }

    public static final Parcelable.Creator<FacebookItem> CREATOR = new Parcelable.Creator<FacebookItem>() {
        @Override
        public FacebookItem createFromParcel(Parcel in) {
            return new FacebookItem(in);
        }

        @Override
        public FacebookItem[] newArray(int size) {
            return new FacebookItem[size];
        }
    };

}
