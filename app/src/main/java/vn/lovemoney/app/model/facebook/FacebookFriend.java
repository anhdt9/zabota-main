package vn.lovemoney.app.model.facebook;

/**
 * Created by DTA on 22/04/2017.
 */

public class FacebookFriend {
    private String name;
    private String pictureUrl;
    private boolean isCheck;
    private String id;

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
