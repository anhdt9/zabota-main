package vn.lovemoney.app.model.support;

/**
 * Created by dta on 30/04/2017.
 */

public class GetTicket {
    private int ticketId;
    private String subject;
    private int status;
    private String createdAt;
    private int unseenCount;

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String[] getTime() {
        String[] time = new String[2];
        int indexSpace = -1;
        for (int i = 0; i < createdAt.length(); i++) {
            if (String.valueOf(createdAt.charAt(i)).equalsIgnoreCase(" ")) {
                indexSpace = i;
                break;
            }
        }
        time[0] = createdAt.substring(indexSpace + 1, createdAt.length());
        time[1] = createdAt.substring(0, indexSpace);
        return time;
    }

    public int getUnseenCount() {
        return unseenCount;
    }

    public void setUnseenCount(int unseenCount) {
        this.unseenCount = unseenCount;
    }
}
