package vn.lovemoney.app.model.bonus;

/**
 * Created by DTA on 24/05/2017.
 */

public class EventAnswerBody {
    private int id;
    private String answer;

    public EventAnswerBody(int id, String answer) {
        this.id = id;
        this.answer = answer;
    }
}
