package vn.lovemoney.app.model.exchance;

import java.util.List;

import vn.lovemoney.app.model.BaseObjectResponse;

/**
 * Created by 8470p on 5/17/2017.
 */

public class ShowPaypalListResponse extends BaseObjectResponse<List<PaypalItem>> {
}
