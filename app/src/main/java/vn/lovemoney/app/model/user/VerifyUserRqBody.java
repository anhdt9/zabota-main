package vn.lovemoney.app.model.user;

/**
 * Created by 8470p on 4/1/2017.
 */

public class VerifyUserRqBody {
    private String phone;
    private String code;

    public VerifyUserRqBody(String phone, String code) {
        this.phone = phone;
        this.code = code;
    }
}
