package vn.lovemoney.app.model.exchance;

import java.util.List;

import vn.lovemoney.app.model.BaseObjectResponse;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public class ShowRedeemListResponse extends BaseObjectResponse<List<RedeemItem>> {
}
