package vn.lovemoney.app.model.bonus.coin.social;

import android.graphics.drawable.Drawable;

/**
 * Created by DTA on 13/03/2017.
 */

public class FanPageItem {
    private Drawable icon;
    private String name;
    private String coin;

    public FanPageItem(Drawable icon, String name, String coin) {
        this.icon = icon;
        this.name = name;
        this.coin = coin;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }
}
