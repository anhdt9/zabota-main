package vn.lovemoney.app.model.support;

/**
 * Created by dta on 30/04/2017.
 */

public class GetSupportThead {
    private int threadId;
    private String subject;
    private boolean isSelected;

    public int getThreadId() {
        return threadId;
    }

    public void setThreadId(int threadId) {
        this.threadId = threadId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
