package vn.lovemoney.app.model.user;

/**
 * Created by 8470p on 3/31/2017.
 */

public class LoginRqBody {
    private String fbId;
    private String fbToken;

    public LoginRqBody(String fbId, String fbToken) {
        this.fbId = fbId;
        this.fbToken = fbToken;
    }
}
