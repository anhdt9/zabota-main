package vn.lovemoney.app.model.user;

/**
 * Created by 8470p on 4/1/2017.
 */

public class UserInfo {
    private String name;
    private String email;
    private String phone;
    private String avatarUrl;
    private int balance;
    private int earning;
    private boolean verified;
    private String referralCode;
    private boolean enteredReferral;
    private int rank;
    private int unseenCount;

    public UserInfo(UserCredentials userCredentials) {
        name = userCredentials.getName();
        avatarUrl = userCredentials.getAvatarUrl();
        phone = userCredentials.getPhone();
        verified = userCredentials.isVerified();
        balance = userCredentials.getBalance();
        earning = userCredentials.getEarning();
        referralCode = userCredentials.getReferralCode();
        enteredReferral = userCredentials.isEnteredReferral();
        rank = userCredentials.getRank();
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public int getBalance() {
        return balance;
    }

    public int getEarning() {
        return earning;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public boolean isVerified() {
        return verified;
    }

    public boolean isEnteredReferral() {
        return enteredReferral;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getUnseenCount() {
        return unseenCount;
    }

    public void setUnseenCount(int unseenCount) {
        this.unseenCount = unseenCount;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", balance=" + balance +
                ", earning=" + earning +
                ", verified=" + verified +
                ", referralCode='" + referralCode + '\'' +
                ", enteredReferral=" + enteredReferral +
                ", rank=" + rank +
                ", unseenCount=" + unseenCount +
                '}';
    }
}
