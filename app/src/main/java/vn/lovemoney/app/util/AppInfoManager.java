package vn.lovemoney.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

import vn.lovemoney.app.model.android.app.NewAppOffer;
import vn.lovemoney.app.network.GsonSingleton;

/**
 * Created by tuannguyen on 12/16/15.
 */
public class AppInfoManager {
	private static final String APP_TOKEN_PREF = "app_token_pref";

	private static volatile AppInfoManager sInstance;

	private SharedPreferences mPref;
	private Map<String, NewAppOffer> mCacheOffer;

	private AppInfoManager(Context context) {
		mPref = context.getSharedPreferences(APP_TOKEN_PREF, Context.MODE_PRIVATE);
		mCacheOffer = new HashMap<>();
	}

	public static AppInfoManager getInstance(Context context){
		if(sInstance == null){
			synchronized (AppInfoManager.class){
				if(sInstance == null){
					sInstance = new AppInfoManager(context.getApplicationContext());
				}
			}
		}
		return sInstance;
	}

	public NewAppOffer getAppOffer(String packageBundle) {
		NewAppOffer appOffer = null;
		if (mCacheOffer.containsKey(packageBundle)) {
			appOffer = mCacheOffer.get(packageBundle);
		} else {
			String json = mPref.getString(packageBundle, null);
			if (!TextUtils.isEmpty(json)) {
				appOffer = GsonSingleton.getInstance().fromJson(json, NewAppOffer.class);
			}
		}
		return appOffer;
	}

	public void saveAppOffer(String packageBundle, NewAppOffer body) {
		mCacheOffer.put(packageBundle, body);
		mPref.edit().putString(packageBundle, GsonSingleton.getInstance().toJson(body)).apply();
	}

	public void clear(){
		mPref.edit().clear().apply();
	}

}
