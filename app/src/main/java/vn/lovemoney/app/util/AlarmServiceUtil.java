package vn.lovemoney.app.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.SparseArray;

import java.util.Calendar;
import java.util.Random;

import vn.lovemoney.app.model.LocalPush;
import vn.lovemoney.app.receiver.LocalPushReceiver;
import vn.lovemoney.app.service.AuditRunningService;
import vn.lovemoney.app.service.RunAppManagerService;

/**
 * Created by tuannguyen on 4/12/16.
 */
public class AlarmServiceUtil {
	private static volatile AlarmServiceUtil sInstance;
	private static final int RC_CHECK_APP_OPENED = 12;
	private static final int RC_CHECK_APP_REOPENED = 13;
	private final AlarmManager mAlarm;
	private PendingIntent mCurrentIntent;
	private PendingIntent mLocalPushIntent;
	private Context mContext;
	private SparseArray<PendingIntent> mLocalPushIntents;

	public static AlarmServiceUtil getInstance(Context context) {
		if (sInstance == null) {
			synchronized (AlarmServiceUtil.class) {
				if (sInstance == null) {
					sInstance = new AlarmServiceUtil(context);
				}
			}
		}
		return sInstance;
	}

	private AlarmServiceUtil(Context context) {
		mContext = context.getApplicationContext();
		mAlarm = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
		mLocalPushIntents = new SparseArray<>();
	}

	public void startCheckingAppOpen(String packageName, int campaignId, int count) {
		startService(AuditRunningService.class, RC_CHECK_APP_OPENED, packageName, campaignId, count);
	}

	public void startCheckingReopenApp(String packageName, int campaignId, int count) {
		startService(RunAppManagerService.class, RC_CHECK_APP_REOPENED, packageName, campaignId, count);
	}

	private void startService(Class<? extends Service> clazz, int requestCode, String packageName, int campaignId, int count) {
		cancelAlarm();
		Context appContext = mContext.getApplicationContext();
		Intent intent = new Intent(appContext, clazz);
		intent.putExtra(ArgumentKeys.PACKAGE_NAME, packageName);
		intent.putExtra(ArgumentKeys.CAMPAIGN_ID, campaignId);
		intent.putExtra(ArgumentKeys.COUNT, count);
		mCurrentIntent = PendingIntent.getService(appContext, requestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		int delay = count == 0 ? 50000 : 10000;
		mAlarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delay, mCurrentIntent);
	}

	public void startScheduleLocalPush() {
		if(PreferenceManager.getInstance(mContext).isScheduledLocalPushToday()){
			return;
		}
		Calendar lastReminded = PreferenceManager.getInstance(mContext).getLastRemind();
		if(lastReminded.getTimeInMillis() == 0 || !DateUtils.isToday(lastReminded.getTimeInMillis())) {
			cancelScheduleLocalPush();
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			Random rand = new Random();
			calendar.set(Calendar.HOUR_OF_DAY, 7 + rand.nextInt(16));
			calendar.set(Calendar.MINUTE, rand.nextInt(60));
			if (Calendar.getInstance().after(calendar)) {
				calendar.add(Calendar.DATE, 1);
			}
			Intent intent = new Intent(mContext, LocalPushReceiver.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 1122, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
				mAlarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
			} else {
				mAlarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
			}
			PreferenceManager.getInstance(mContext).saveScheduledLocalPush();
		}
	}

	public void cancelAlarm() {
		if (mCurrentIntent != null) {
			mAlarm.cancel(mCurrentIntent);
			mCurrentIntent = null;
		}
	}

	private PendingIntent getScheduleLocalPushIntent() {
		if (mLocalPushIntent == null) {
			Intent intent = new Intent(mContext, LocalPushReceiver.class);
			mLocalPushIntent = PendingIntent.getBroadcast(mContext, 99999, intent, 0);
		}
		return mLocalPushIntent;
	}

	private void cancelScheduleLocalPush() {
		mAlarm.cancel(getScheduleLocalPushIntent());
	}

}
