package vn.lovemoney.app.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Locale;

/**
 * Created by Laptop TCC on 3/11/2017.
 */

public class LanguageUtils {

    public static final int LANGUAGE_VI = 0;
    public static final int LANGUAGE_EN = 1;

    public static void setCurrentLanguage(int type, Context context) {
        switch (type) {
            case 0:
                setCurrentLanguage("en", context);
                break;
            case 1:
                setCurrentLanguage("vi", context);
                break;
            default:
                setCurrentLanguage("en", context);
                break;
        }
    }

    public static String getCurrentLanguage(Context context) {
        return PreferenceManager.getInstance(context).getLanguage();
    }

    private static void setCurrentLanguage(String language, Context context) {
        PreferenceManager.getInstance(context).setLanguage(language);
    }

    public static Locale getLocale(Context context){
        String lang = getCurrentLanguage(context);
        String country = lang.equals("vi") ? "VN" : "US";
        return new Locale(lang, country);
    }

}
