package vn.lovemoney.app.util;

/**
 * Created by tuannguyen on 12/10/15.
 */
public class ArgumentKeys {
    public static final String SELECTED_TAB_POSITION = "selected_tab_position";
    public static final String REOPEN_APP = "reopen_app";
    public static final String APP_OFFER = "app_offer";
    public static final String REDEEM_ITEM = "redeem_item";
    public static final String EXCHANGE_ITEM = "exchange_item";
    public static final String PAYPAL_ITEM = "paypal_item";
    public static final String IS_INSTALL = "is_install";
    public static final String IS_REVIEW = "is_review";
    public static final String IS_CONFIRM = "is_confirm";
    public static final String FREEXU_ACTION = "freexu_action";
    public static final String PACKAGE_NAME = "package_name";
    public static final String CARD = "card";
    public static final String IS_REGISTER = "is_register";
    public static final String FROM_LOGIN = "from_login";
    public static final String SHARE_ITEMS = "share_items";
    public static final String BONUS = "bonus";
    public static final String NOTIFICATION = "notification";
    public static final String LIKE_COUNT = "like_count";
    public static final String URL = "url";
    public static final String PAGE_ID = "page_id";
    public static final String EVENT = "event";
    public static final String SHARE_EVENT = "share_event";
    public static final String QUESTION_LEVELS = "question_levels";
    public static final String TITLE = "title";
    public static final String GUIDE = "guide";
    public static final String QUESTION = "question";
    public static final String ANSWER = "answer";
    public static final String COUNT = "count";
    public static final String LOCAL_PUSH = "local_push";
    public static final String PAGE_URL = "page_url";
    public static final String CAMPAIGN_ID = "campaign_id";
    public static final String FACEBOOK_ITEM = "facebook_item";
    public static final String KIDE_GUILINE = "kind_gui";
    public static final String RESULT_INDEX = "result_index";
    public static final String MINI_GAME_DATA = "mini_game_data";
    public static final String PHONE = "phone";
}
