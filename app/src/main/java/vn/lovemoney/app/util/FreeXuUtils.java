package vn.lovemoney.app.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.jaredrummler.android.processes.AndroidProcesses;
import com.jaredrummler.android.processes.models.AndroidAppProcess;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.DeviceInfo;
import vn.lovemoney.app.network.GsonSingleton;

/**
 * Created by HN on 12/8/2015.
 */
public class FreeXuUtils {

    public static final String GOOGLE_PLAY_PACKAGE_NAME = "com.android.vending";
    public static final int LANGUAGE_VN = 0;
    public static final int LANGUAGE_EN = 1;
    public static int mCurLanguage = 0;

    public static String getDeviceId(Context context) {
        String imei = getImei(context);

        if (imei != null) {
            return imei;
        }
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);


    }

    public static String getFormattedDate(Calendar cal) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(cal.getTime());
    }

    public static String getFormattedTime(Calendar cal) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(cal.getTime());
    }

    public static boolean isAppInstallFromGooglePlay(Context context, String packageName) {
        if (context != null) {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo applicationInfo = null;
            try {
                applicationInfo = pm.getApplicationInfo(packageName, 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (applicationInfo != null) {
                String installerPackageName = pm.getInstallerPackageName(packageName);

                if (!TextUtils.isEmpty(installerPackageName) && installerPackageName.equals(GOOGLE_PLAY_PACKAGE_NAME)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isAppInstalled(Context context, String packageName) {
        if (context != null) {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo applicationInfo;
            try {
                applicationInfo = pm.getApplicationInfo(packageName, 0);
                if (applicationInfo != null) {
                    return true;
                }
            } catch (PackageManager.NameNotFoundException e) {
//			e.printStackTrace();
            }
        }
        return false;
    }

    public static Calendar getAppInstalledTime(Context context, String packageName) {
        if (context != null) {
            PackageManager pm = context.getPackageManager();
            PackageInfo packageInfo = null;
            try {
                packageInfo = pm.getPackageInfo(packageName, 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (packageInfo != null) {
                long timestamp = packageInfo.firstInstallTime;
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(timestamp);
                return cal;
            }
        }
        return null;
    }

    public static boolean isGooglePlayStoreInstalled(Context context) {
        ApplicationInfo applicationInfo = null;
        if (context != null) {
            try {
                PackageManager pm = context.getPackageManager();
                applicationInfo = pm.getApplicationInfo(GOOGLE_PLAY_PACKAGE_NAME, 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return applicationInfo != null;
    }

    public static int getDipsFromPixel(Context context, float pixels) {
        // Get the screen's density scale
        float scale = context.getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    public static boolean isAppRunning(Context context, String packageName) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return true;
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            List<AndroidAppProcess> processes = AndroidProcesses.getRunningAppProcesses();
            for (AndroidAppProcess process : processes) {
                if (packageName.equals(process.getPackageName())) {
                    return true;
                }
            }
        } else {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
            for (int i = 0; i < procInfos.size(); i++) {
                if (procInfos.get(i).processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getPrimaryAccount(Context context) {
        if (context != null) {
            Pattern emailPattern = Patterns.EMAIL_ADDRESS;
            Account[] accounts = AccountManager.get(context).getAccountsByType("com.google");
            for (Account account : accounts) {
                if (emailPattern.matcher(account.name).matches()) {
                    return account.name;
                }
            }
        }
        return null;
    }

    public static List<String> getEmails(Context context) {
        List<String> ret = new ArrayList<>();
        if (context != null) {
            Pattern emailPattern = Patterns.EMAIL_ADDRESS;
            Account[] accounts = AccountManager.get(context).getAccountsByType("com.google");
            for (Account account : accounts) {
                if (emailPattern.matcher(account.name).matches()) {
                    ret.add(account.name);
                }
            }
        }
        return ret;
    }

    public static String getImei(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                return telephonyManager.getDeviceId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getPhoneNumber(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                String phone = telephonyManager.getLine1Number();
                if (!TextUtils.isEmpty(phone)) {
                    phone = phone.replaceAll("[\\D]", "");
                    phone = phone.replace("^84", "0");
                    return phone;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDeviceInfo(Context context) {
        if (context == null) {
            return "";
        }
        DeviceInfo info = new DeviceInfo();
        info.setDevice(Build.DEVICE);
        info.setDeviceId(getDeviceId(context));
        info.setModel(Build.MODEL);
        info.setManufacture(Build.MANUFACTURER);
        info.setOsVersion(System.getProperty("os.version"));
        info.setProduct(Build.PRODUCT);
        info.setSdkVersion(Build.VERSION.SDK_INT);
        info.setImei(getImei(context));
        info.setPhoneNumber(getPhoneNumber(context));
        info.setEmail(getPrimaryAccount(context));
        info.setEmails(getEmails(context));
        return GsonSingleton.getInstance().toJson(info);
    }

    public static boolean isRunningOnEmulator(Context context) {
        try {
            SensorManager manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
            List<Sensor> sensors = manager.getSensorList(Sensor.TYPE_ALL);
            if (sensors.isEmpty()) {
                return true;
            }
            for (Sensor sensor : sensors) {
                Class clazz = sensor.getClass();
                Field field = clazz.getDeclaredField("mVendor");
                field.setAccessible(true);
                String vendorName = ((String) field.get(sensor)).toLowerCase();
                String sensorName = sensor.getName().toLowerCase();
                if (vendorName.contains("bluestacks") || vendorName.contains("greatfruit") || vendorName.equals("nox") || sensorName.contains("droid4x")) {
                    return true;
                }
            }
            boolean result =//
                    Build.FINGERPRINT.startsWith("generic")//
                            || Build.FINGERPRINT.startsWith("unknown")//
                            || Build.MODEL.contains("google_sdk")//
                            || Build.MODEL.contains("AMIDuOS")
                            || Build.MODEL.contains("Emulator")//
                            || Build.MODEL.contains("Android SDK built for x86");
            if (result)
                return true;
            File test = new File("/data/Bluestacks.prop");
            result = test.exists();
            result |= Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic");
            if (result)
                return true;
            result = "google_sdk".equals(Build.PRODUCT);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public static void saveToClipboard(Context context, String content) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("text", content);
        clipboard.setPrimaryClip(clip);
    }

    public static void customTab(Context context, FragmentStatePagerAdapter adapter, TabLayout tabLayout, String tabTitles[], boolean showCounter[]) {
        LayoutInflater inflater = LayoutInflater.from(context);
        for (int i = 0; i < adapter.getCount(); i++) {
            View v = inflater.inflate(R.layout.fragment_custom_tab, null);
            TextView tabTitle = (TextView) v.findViewById(R.id.tab_title);

            TextView tabCounter = (TextView) v.findViewById(R.id.tab_counter);
            tabCounter.setVisibility((showCounter[i]) ? View.VISIBLE : View.GONE);

            tabTitle.setTextColor(context.getResources().getColor(R.color.semiWhite));
            tabTitle.setTypeface(null, Typeface.BOLD);
            tabTitle.setText(tabTitles[i]);
            tabTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_size_small));
            Typeface fontPath = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
            tabTitle.setTypeface(fontPath);

            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(v);
            }
        }
    }
}
