package vn.lovemoney.app.util;

/**
 * Created by tuannguyen on 12/23/15.
 */
public class Singleton {
	private static volatile Singleton sInstance;
	private boolean mShouldUpdateProfile;
	private boolean mShouldRefreshOfferList;
	private boolean mShouldUpdateReOpenAppList;

	public static Singleton getInstance(){
		if(sInstance == null){
			synchronized (Singleton.class) {
				if(sInstance == null) {
					sInstance = new Singleton();
				}
			}
		}
		return sInstance;
	}

	private Singleton(){}

	public void setShouldUpdateProfile(boolean value){
		mShouldUpdateProfile = value;
	}

	public void setShouldRefreshOfferList(boolean value){
		mShouldRefreshOfferList = value;
	}

	public boolean shouldUpdateProfile(){
		return mShouldUpdateProfile;
	}

	public boolean shouldRefreshOfferList(){
		return mShouldRefreshOfferList;
	}

	public boolean shouldUpdateReOpenAppList(){
		return mShouldUpdateReOpenAppList;
	}

	public void setShouldUpdateReOpenAppList(boolean value){
		mShouldUpdateReOpenAppList = value;
	}
}
