package vn.lovemoney.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import vn.lovemoney.app.model.OneSignalInfo;
import vn.lovemoney.app.model.other.RemoteConfig;
import vn.lovemoney.app.model.user.UserCredentials;
import vn.lovemoney.app.model.user.UserInfo;
import vn.lovemoney.app.network.GsonSingleton;

/**
 * Created by tuannguyen on 12/9/15.
 */
public class PreferenceManager {

    private static final String TAG = "PreferenceManager";
    private static final String PRIVATE_PREF = "dingo_pref";
    private static final String IS_LOGGED_IN = "is_logged_in";
    private static final String TOKEN = "token";
    private static final String ID = "id";
    public static final String USER_INFO = "user_info";
    public static final String UNREAD_NOTIFICATION_COUNT = "unread_notification_count";
    public static final String SAVED_NOTIFICATION_INFO = "saved_notification_info";
    private static final String ONE_SIGNAL_INFO = "one_signal_info";
    private static final String REPORTED = "red";
    private static final String LAST_LUCKY_SUBMIT = "last_lucky_submit";
    private static final String LAST_PUSH = "last_push";
    private static final String LAST_INVITED = "last_invited";
    private static final String TODAY_INVITED_COUNT = "today_invited_count";
    private static final String LANGUAGUE_APP = "language";
    private static final String TICKET_PROCESSING = "ticket_processing";
    private static final String TICKET_SUPPORTING_LOGIN_ERROR = "ticket_supporting_login_error";

    public static final String GUIDE_SUPPORT_LOGIN_CLICK = "guide_support_login_click";
    public static final String GUIDE_SUPPORT_LOGIN_SMS = "guide_support_login_sms";
    public static final String GUIDE_ENTER_REFERRAL_LAYOUT = "guide_enter_referral_layout";
    public static final String GUIDE_INSTALL_DOWNLOAD = "guide_install_download";
    public static final String GUIDE_INSTALL_DIALOG = "guide_install_dialog";
    public static final String GUIDE_REDEEM_TOPUP = "guide_redeem_topup";
    public static final String GUIDE_REDEEM_CONFIRM_CHANGE = "guide_redeem_confirm_change";
    public static final String GUIDE_SHARE_CODE = "guide_share_code";
    private static final String REMOTE_CONFIG = "remote_config";
    private static final String LAST_REMIND = "last_remind";
    private static final String RATED = "rated";

    private static final HashMap<String, Object> mPreferenceMap = new HashMap<>();

    private static volatile PreferenceManager sInstance;

    private SharedPreferences mPref;
    private UserInfo mUserInfo;
    private UserCredentials mUserCredentials;
    public static final String USER_CREDENTIAL = "user_credential";
    private Integer mUnreadNotificationCount;
    private Boolean mSavedNotificationInfo;
    private OneSignalInfo mOneSignalInfo;
    private Boolean mScheduledLocalPush;

    private PreferenceManager(Context context) {
        mPref = context.getSharedPreferences(PRIVATE_PREF, Context.MODE_PRIVATE);
    }

    public static PreferenceManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (PreferenceManager.class) {
                if (sInstance == null) {
                    sInstance = new PreferenceManager(context.getApplicationContext());
                }
            }
        }
        return sInstance;
    }

    public void registerListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mPref.registerOnSharedPreferenceChangeListener(listener);
    }

    public void unregisterListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        mPref.unregisterOnSharedPreferenceChangeListener(listener);
    }

    public boolean isLoggedIn() {
        return mPref.getBoolean(IS_LOGGED_IN, false);
    }

    public void saveUserLogin(UserCredentials nUserCredentials) {
        mUserCredentials = nUserCredentials;
        UserInfo userInfo = new UserInfo(mUserCredentials);
        String userCredentials = GsonSingleton.getInstance().toJson(mUserCredentials);
        String userInfoJson = GsonSingleton.getInstance().toJson(userInfo);
        SharedPreferences.Editor editor = mPref.edit();
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.putString(TOKEN, mUserCredentials.getToken());
        editor.putString(ID, mUserCredentials.getId());
        Log.i(TAG, "mUserCredentials=" + userCredentials);
        editor.putString(USER_CREDENTIAL, userCredentials);
        editor.putString(USER_INFO, userInfoJson);
        editor.apply();
    }

    public UserCredentials getUserCredential() {
        if (mUserCredentials == null) {
            String json = mPref.getString(USER_CREDENTIAL, null);
            if (!TextUtils.isEmpty(json)) {
                mUserCredentials = GsonSingleton.getInstance().fromJson(json, UserCredentials.class);
            }
        }
        return mUserCredentials;
    }

    public void logout() {
        mUserInfo = null;
        SharedPreferences.Editor editor = mPref.edit();
        editor.putBoolean(IS_LOGGED_IN, false);
        editor.remove(TOKEN);
        editor.remove(ID);
        editor.remove(USER_INFO);
        editor.apply();
    }

    public String getToken() {
        return mPref.getString(TOKEN, null);
    }

    public String getId() {
        return mPref.getString(ID, null);
    }

    public UserInfo getUserInfo() {
        if (mUserInfo == null) {
            String json = mPref.getString(USER_INFO, null);
            if (!TextUtils.isEmpty(json)) {
                mUserInfo = GsonSingleton.getInstance().fromJson(json, UserInfo.class);
            }
        }
        return mUserInfo;
    }

    public void updateBalance(Integer newBalance) {
        UserInfo userInfo = getUserInfo();
        userInfo.setBalance(newBalance);
        mUserInfo = userInfo;
        mPref.edit().putString(USER_INFO, GsonSingleton.getInstance().toJson(mUserInfo)).apply();
    }

    public void updateUserInfo(UserInfo userInfo) {
        mUserInfo = userInfo;
        mPref.edit().putString(USER_INFO, GsonSingleton.getInstance().toJson(mUserInfo))
                .putInt(UNREAD_NOTIFICATION_COUNT, userInfo.getUnseenCount())
                .apply();
    }

    public void increaseUnreadNotificationCount() {
        int unreadNotificationCount = getUnreadNotificationCount();
        unreadNotificationCount++;
        mUnreadNotificationCount = unreadNotificationCount;
        mPref.edit().putInt(UNREAD_NOTIFICATION_COUNT, unreadNotificationCount).apply();
    }

    public int getUnreadNotificationCount() {
        if (mUnreadNotificationCount == null) {
            mUnreadNotificationCount = mPref.getInt(UNREAD_NOTIFICATION_COUNT, 0);
        }
        return mUnreadNotificationCount;
    }

    public void clearUnreadNotificationCount() {
        mUnreadNotificationCount = 0;
        mPref.edit().remove(UNREAD_NOTIFICATION_COUNT).apply();
    }

    public boolean isSavedOneSignalInfo() {
        if (mSavedNotificationInfo == null) {
            mSavedNotificationInfo = mPref.getBoolean(SAVED_NOTIFICATION_INFO, false);
        }
        return mSavedNotificationInfo;
    }

    public void setSavedOneSignalInfo(boolean value) {
        mSavedNotificationInfo = value;
        mPref.edit().putBoolean(SAVED_NOTIFICATION_INFO, value).apply();
    }

    public void saveOneSignalInfo(OneSignalInfo info) {
        mOneSignalInfo = info;
        mPref.edit().putString(ONE_SIGNAL_INFO, GsonSingleton.getInstance().toJson(info)).apply();
    }

    public OneSignalInfo getOneSignalInfo() {
        if (mOneSignalInfo == null) {
            String json = mPref.getString(ONE_SIGNAL_INFO, null);
            if (!TextUtils.isEmpty(json)) {
                mOneSignalInfo = GsonSingleton.getInstance().fromJson(json, OneSignalInfo.class);
            }
        }
        return mOneSignalInfo;
    }

    public void saveLastLuckySubmit(Calendar cal) {
        mPref.edit().putLong(LAST_LUCKY_SUBMIT, cal.getTimeInMillis()).apply();
    }

    public Calendar getLastLuckySubmit() {
        Calendar cal = Calendar.getInstance();
        long millis = mPref.getLong(LAST_LUCKY_SUBMIT, 0);
        cal.setTimeInMillis(millis);
        return cal;
    }

    public boolean isReported() {
        return mPref.getBoolean(REPORTED, false);
    }

    public void markAsReported() {
        mPref.edit().putBoolean(REPORTED, true).apply();
    }

    public void saveScheduledLocalPush() {
        mPref.edit().putLong(LAST_PUSH, Calendar.getInstance().getTimeInMillis()).apply();
    }

    public boolean isScheduledLocalPushToday() {
        long millis = mPref.getLong(LAST_PUSH, 0);
        return DateUtils.isToday(millis);
    }

    public void setLanguage(String language) {
        if (mPref != null)
            mPref.edit().putString(LANGUAGUE_APP, language).apply();
    }

    public String getLanguage() {
        Locale currentLocale = Locale.getDefault();
        return mPref.getString(LANGUAGUE_APP, currentLocale.getLanguage());
    }

    public boolean shouldInviteFriends() {
        return false;
    }

    public void saveInvitedFriends() {
        long lastInvited = mPref.getLong(LAST_INVITED, 0);
        int todayCount = mPref.getInt(TODAY_INVITED_COUNT, 0);
        mPref.edit()
                .putLong(LAST_INVITED, Calendar.getInstance().getTimeInMillis())
                .putInt(TODAY_INVITED_COUNT, DateUtils.isToday(lastInvited) ? todayCount + 1 : 1)
                .apply();
    }

    public void setSupportTicketProcessing(int ticket) {
        if (mPref != null) {
            mPref.edit().putInt(TICKET_SUPPORTING_LOGIN_ERROR, ticket).apply();
        }
    }

    public int getSupportTicketProcessing() {
        if (mPref == null)
            return -1;
        return mPref.getInt(TICKET_SUPPORTING_LOGIN_ERROR, -1);
    }

    public void setTicketProcessing(int ticket) {
        if (mPref != null) {
            mPref.edit().putInt(TICKET_PROCESSING, ticket).apply();
        }
    }

    public int getTicketProcessing() {
        if (mPref == null)
            return -1;
        return mPref.getInt(TICKET_PROCESSING, -1);
    }

    public Calendar getLastRemind() {
        long timestamp = mPref.getLong(LAST_REMIND, 0);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        return cal;
    }

    public void saveLastRemind(Calendar cal) {
        long timestamp = cal.getTimeInMillis();
        mPref.edit().putLong(LAST_REMIND, timestamp).apply();
    }

    public void saveRated(){
        mPref.edit().putBoolean(RATED, true).apply();
    }

    public boolean isRated(){
        return mPref.getBoolean(RATED, false);
    }

    public static void saveState(Context context, String keyString, int value) {
        try {
            SharedPreferences pref = context.getSharedPreferences(PRIVATE_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.remove(keyString);
            editor.putInt(keyString, value);
            mPreferenceMap.put(keyString, value);
            editor.apply();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public static void saveState(Context context, String keyString, String value) {
        try {
            SharedPreferences pref = context.getSharedPreferences(PRIVATE_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.remove(keyString);
            editor.putString(keyString, value);
            mPreferenceMap.put(keyString, value);
            editor.apply();
        } catch (Exception e) {
            android.util.Log.e(TAG, e.toString());
        }
    }

    public static void saveState(Context context, String keyString, boolean value) {
        SharedPreferences pref = context.getSharedPreferences(PRIVATE_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(keyString);
        editor.putBoolean(keyString, value);
        mPreferenceMap.put(keyString, value);
        editor.apply();
    }

    public static int loadIntKey(Context context, String keyString, int defValue) {
        int key;
        try {
            Integer value = (Integer) (mPreferenceMap.get(keyString));
            if (value != null) {
                return value;
            }
            SharedPreferences pref = context.getSharedPreferences(PRIVATE_PREF, Context.MODE_PRIVATE);
            key = pref.getInt(keyString, defValue);
            mPreferenceMap.put(keyString, key);
        } catch (Exception e) {
            key = defValue;
            android.util.Log.e(TAG, e.toString());
        }
        return key;
    }

    public static String loadStringKey(Context context, String keyString) {
        String key = "";
        try {
            String value = (String) mPreferenceMap.get(keyString);
            if (value != null) {
                return value;
            }
            SharedPreferences pref = context.getSharedPreferences(PRIVATE_PREF, Context.MODE_PRIVATE);
            key = pref.getString(keyString, "");
            mPreferenceMap.put(keyString, key);
        } catch (Exception e) {
            android.util.Log.e(TAG, e.toString());
        }
        return key;
    }

    public static boolean loadBooleanKey(Context context, String keyString, boolean defValue) {
        try {
            Boolean value = (Boolean) mPreferenceMap.get(keyString);
            if (value != null) {
                return value;
            }
            SharedPreferences pref = context.getSharedPreferences(PRIVATE_PREF, Context.MODE_PRIVATE);
            defValue = pref.getBoolean(keyString, defValue);
            mPreferenceMap.put(keyString, defValue);
        } catch (Exception e) {
            android.util.Log.e(TAG, e.toString());
            return defValue;
        }
        return defValue;
    }

    public void setRemoteConfig(RemoteConfig remoteConfig) {
        mPreferenceMap.put(REMOTE_CONFIG, remoteConfig);
        mPref.edit().putString(REMOTE_CONFIG, GsonSingleton.getInstance().toJson(remoteConfig)).apply();
    }

    public RemoteConfig getRemoteConfig() {
        if (mPreferenceMap.containsKey(REMOTE_CONFIG)) {
            return (RemoteConfig) mPreferenceMap.get(REMOTE_CONFIG);
        } else {
            String remoteConfigJson = mPref.getString(REMOTE_CONFIG, null);
            if (remoteConfigJson != null) {
                RemoteConfig remoteConfig = GsonSingleton.getInstance().getGson().fromJson(remoteConfigJson, RemoteConfig.class);
                mPreferenceMap.put(REMOTE_CONFIG, remoteConfig);
                return remoteConfig;
            }
            return null;
        }
    }
}
