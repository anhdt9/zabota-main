package vn.lovemoney.app.util;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import vn.lovemoney.app.R;

/**
 * Created by DTA on 30/05/2017.
 */

public class GuideUtils {

    public static final int GUIDE_SUPPORT_LOGIN_CLICK = 1;
    public static final int GUIDE_SUPPORT_LOGIN_SMS = 2;
    public static final int GUIDE_ENTER_REFERRAL_LAYOUT = 3;
    //    public static final int GUIDE_ENTER_REFERRAL_CONFIRM = 4;
//    public static final int GUIDE_ENTER_REFERRAL_CLOSE = 5;
    public static final int GUIDE_INSTALL_DOWNLOAD = 6;
    public static final int GUIDE_INSTALL_DIALOG = 7;
    //    public static final int GUIDE_INSTALL_STEP_1_2 = 8;
//    public static final int GUIDE_INSTALL_STEP_3 = 9;
//    public static final int GUIDE_INSTALL_STEP_4 = 10;
    public static final int GUIDE_REDEEM_TOPUP = 11;
    //    public static final int GUIDE_REDEEM_ITEM = 12;
    public static final int GUIDE_REDEEM_CONFIRM_CHANGE = 13;
    public static final int GUIDE_SHARE_CODE = 14;

    private static final SparseArray<String> mMap = new SparseArray<>();

    static {
        mMap.put(GUIDE_SUPPORT_LOGIN_CLICK, PreferenceManager.GUIDE_SUPPORT_LOGIN_CLICK);
        mMap.put(GUIDE_SUPPORT_LOGIN_SMS, PreferenceManager.GUIDE_SUPPORT_LOGIN_SMS);
        mMap.put(GUIDE_ENTER_REFERRAL_LAYOUT, PreferenceManager.GUIDE_ENTER_REFERRAL_LAYOUT);
        mMap.put(GUIDE_INSTALL_DOWNLOAD, PreferenceManager.GUIDE_INSTALL_DOWNLOAD);
        mMap.put(GUIDE_INSTALL_DIALOG, PreferenceManager.GUIDE_INSTALL_DIALOG);
        mMap.put(GUIDE_REDEEM_TOPUP, PreferenceManager.GUIDE_REDEEM_TOPUP);
        mMap.put(GUIDE_REDEEM_CONFIRM_CHANGE, PreferenceManager.GUIDE_REDEEM_CONFIRM_CHANGE);
        mMap.put(GUIDE_SHARE_CODE, PreferenceManager.GUIDE_SHARE_CODE);
    }

    public static void requestShowToolTip(int id, Context context, ViewGroup rootView) {
        if (Utils.isTestGuide)
            return;
        if (PreferenceManager.getInstance(context).loadBooleanKey(context, mMap.get(id), false)) {
            return;
        }
        ArrayList<View> guides = getGuideViews(id, context);
        showToolTip(id, context, rootView, guides);
    }

    private static ArrayList<View> getGuideViews(int id, Context context) {
        ArrayList<View> arr = new ArrayList<>();
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (id) {
            case GUIDE_SUPPORT_LOGIN_CLICK:
                arr.add(vi.inflate(R.layout.guide_support_login_click, null));
                break;
            case GUIDE_SUPPORT_LOGIN_SMS:
                arr.add(vi.inflate(R.layout.guide_support_login_sms, null));
                break;
            case GUIDE_INSTALL_DOWNLOAD:
                arr.add(vi.inflate(R.layout.guide_install_download, null));
                break;
            case GUIDE_ENTER_REFERRAL_LAYOUT:
                arr.add(vi.inflate(R.layout.guide_enter_referral_layout, null));
                arr.add(vi.inflate(R.layout.guide_enter_referral_confirm, null));
                arr.add(vi.inflate(R.layout.guide_enter_referral_close, null));
                break;
            case GUIDE_INSTALL_DIALOG:
                arr.add(vi.inflate(R.layout.guide_install_dialog, null));
                arr.add(vi.inflate(R.layout.guide_install_step_1_2, null));
                arr.add(vi.inflate(R.layout.guide_install_step_3, null));
                arr.add(vi.inflate(R.layout.guide_install_step_4, null));
                break;
            case GUIDE_REDEEM_TOPUP:
                arr.add(vi.inflate(R.layout.guide_redeem_topup, null));
                arr.add(vi.inflate(R.layout.guide_redeem_item, null));
                break;
            case GUIDE_REDEEM_CONFIRM_CHANGE:
                arr.add(vi.inflate(R.layout.guide_redeem_confirm_change, null));
                break;
            case GUIDE_SHARE_CODE:
                arr.add(vi.inflate(R.layout.guide_share_code, null));
                break;
        }
        return arr;
    }

    private static boolean showed = false;
    private static int nextIndex = 0;

    private static void reset() {
        showed = false;
        nextIndex = 0;
    }

    private static void showToolTip(final int id, final Context context, final ViewGroup rootView, final ArrayList<View> guides) {

        if (showed || nextIndex == guides.size()) {
            if (nextIndex == guides.size()) {
                PreferenceManager.getInstance(context).saveState(context, mMap.get(id), true);
            }
            reset();
            return;
        }
        if (nextIndex > 0) {
            rootView.removeView(guides.get(nextIndex - 1));
        }

        View v = guides.get(nextIndex);
        rootView.addView(v);
        rootView.bringChildToFront(v);
        View btnNext = v.findViewById(R.id.tv_next);
        View childRootView = v.findViewById(R.id.childRootView);

        childRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showed = true;
                PreferenceManager.getInstance(context).saveState(context, mMap.get(id), true);
                rootView.removeView(guides.get(nextIndex));
                reset();
            }
        });

        if (btnNext != null) {
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nextIndex++;
                    showToolTip(id, context, rootView, guides);
                }
            });
        }
    }
}
