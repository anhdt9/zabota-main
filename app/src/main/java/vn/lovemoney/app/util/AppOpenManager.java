package vn.lovemoney.app.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tuannguyen on 1/26/16.
 */
public class AppOpenManager {
	private static final String APP_OPEN_PREF = "app_open_pref";

	private static volatile AppOpenManager sInstance;

	private SharedPreferences mPref;
	private Map<String, Boolean> mCache;

	private AppOpenManager(Context context){
		mPref = context.getSharedPreferences(APP_OPEN_PREF, Context.MODE_PRIVATE);
		mCache = new HashMap<>();
	}

	public static AppOpenManager getInstance(Context context){
		if(sInstance == null){
			synchronized (AppOpenManager.class){
				if(sInstance == null){
					sInstance = new AppOpenManager(context.getApplicationContext());
				}
			}
		}
		return sInstance;
	}

	public boolean isAppOpened(String packageName){
		boolean ret = false;
		if(mCache.containsKey(packageName)){
			ret = mCache.get(packageName);
		} else{
			ret = mPref.getBoolean(packageName, false);
			mCache.put(packageName, ret);
		}
		return ret;
	}

	public void markAppOpened(String packageName){
		mCache.put(packageName, true);
		String key = String.format("%s_count", packageName);
		mPref.edit().putBoolean(packageName, true)
				.putInt(key, 0)
				.apply();
	}

	public void increaseOpening(String packageName){
		String key = String.format("%s_count", packageName);
		int openedNumber = mPref.getInt(key, 0);
		openedNumber++;
		mPref.edit().putInt(key, openedNumber).apply();
	}

	public void resetOpening(String packageName){
		String key = String.format("%s_count", packageName);
		mPref.edit().putInt(key, 0).apply();
	}

	public int getOpenedCount(String packageName){
		return mPref.getInt(packageName + "_count", 0);
	}
}
