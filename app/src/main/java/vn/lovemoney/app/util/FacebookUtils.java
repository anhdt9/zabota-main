package vn.lovemoney.app.util;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.internal.ServerProtocol;
import com.facebook.internal.Utility;
import com.facebook.share.internal.ShareConstants;

import java.util.List;
import java.util.Locale;

/**
 * Created by tuandigital on 8/26/16.
 */
public class FacebookUtils {
    static final String REDIRECT_URI = "fbconnect://success";
    static final String CANCEL_URI = "fbconnect://cancel";
    private static final String DISPLAY_TOUCH = "touch";

    public static boolean isLoggedIn(){
        return AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired() ;
    }

    public static boolean isLoggedInExpired(){
        return AccessToken.getCurrentAccessToken() != null && AccessToken.getCurrentAccessToken().isExpired() ;
    }

    public static String buildAppRequestUrl(String title, String message, List<String> recipients) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken == null){
            return null;
        }
        Bundle params = new Bundle();
        Utility.putCommaSeparatedStringList(
                params,
                ShareConstants.WEB_DIALOG_PARAM_TO,
                recipients);
        Utility.putNonEmptyString(
                params,
                ShareConstants.WEB_DIALOG_PARAM_MESSAGE,
                message);
        Utility.putNonEmptyString(
                params,
                ShareConstants.WEB_DIALOG_PARAM_TITLE,
                title);
        params.putString(
                ServerProtocol.DIALOG_PARAM_SDK_VERSION,
                String.format(Locale.ROOT, "android-%s", FacebookSdk.getSdkVersion()));
        params.putString(ServerProtocol.DIALOG_PARAM_APP_ID, FacebookSdk.getApplicationId());
        params.putString(ServerProtocol.DIALOG_PARAM_ACCESS_TOKEN, accessToken.getToken());
        params.putString(ServerProtocol.DIALOG_PARAM_REDIRECT_URI, REDIRECT_URI);
        params.putString(ServerProtocol.DIALOG_PARAM_DISPLAY, DISPLAY_TOUCH);
        return Utility.buildUri(ServerProtocol.getDialogAuthority(), ServerProtocol.getDefaultAPIVersion() + "/" + ServerProtocol.DIALOG_PATH + "apprequests", params).toString();
    }
}
