package vn.lovemoney.app.util.event;

import org.json.JSONObject;

/**
 * Created by DTA on 27/05/2017.
 */

public class MessageEvent {

    private int mEvent = -1;
    private JSONObject data;
    private String message;
    private int myToolTipId;

    public static final int SHOW_NOTIFICATION = 0;
    public static final int UPDATE_OFFER_LIST = 1;
    public static final int UPDATE_PROFILE = 2;
    public static final int UPDATE_DRAWER = 3;
    public static final int UPDATE_PROFILE_SUCCESS = 4;
    public static final int UPDATE_REOPEN_LIST = 5;
    public static final int UPDATE_UNCOUNT_NOTIFICATION = 6;
    public static final int SHOW_TOOLTIP = 7;


    public MessageEvent(int event) {
        mEvent = event;
    }

    public MessageEvent(int mEvent, int myToolTipId) {
        this.mEvent = mEvent;
        this.myToolTipId = myToolTipId;
    }

    public MessageEvent(int mEvent, JSONObject data, String message) {
        this.mEvent = mEvent;
        this.data = data;
        this.message = message;
    }

    public JSONObject getData() {
        return data;
    }

    public int getEvent() {
        return mEvent;
    }

    public String getMessage() {
        return message;
    }

    public int getMyToolTipId() {
        return myToolTipId;
    }
}
