package vn.lovemoney.app.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;

import vn.lovemoney.app.R;

/**
 * Created by ki on 11/10/2015.
 */
public class LoadMoreListView extends ListView implements View.OnClickListener, AbsListView.OnScrollListener {

	private static final String TAG = "LoadMoreListView";
	private static final int NOT_START = 0;
	private static final int FAILED = 1;
	private static final int LOADING = 2;
	private static final int CONTINUE = 3;
	private static final int FINISH = 4;

	private OnScrollListener onScrollListener;
	private LayoutInflater inflater;
	// footer view
	private View footerView;
	// private TextView mLabLoadMore;
	private ProgressWheel progressBarLoadMore;
	private TextView txtNoData;
	private TextView txtLoadFailed;

	// Listener to process load more items when user reaches the end of the list
	private OnLoadMoreListener onLoadMoreListener;
	// To know if the list is loading more items
	private int loadStatus;
	private int currentScrollState;


	public LoadMoreListView(Context context) {
		super(context);
		initialize();
	}

	public LoadMoreListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}

	public LoadMoreListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initialize();
	}

	private void initialize() {

		inflater = LayoutInflater.from(getContext());

		// footer
		footerView = inflater.inflate(R.layout.view_footer_load_more, this, false);
			/*
         * mLabLoadMore = (TextView) footerView
		 * .findViewById(R.id.load_more_lab_view);
		 */
		progressBarLoadMore = (ProgressWheel) footerView.findViewById(R.id.progress_bar);
		txtNoData = (TextView) footerView.findViewById(R.id.no_data);
		txtLoadFailed = (TextView) footerView.findViewById(R.id.load_failed);
		txtLoadFailed.setOnClickListener(this);

		addFooterView(footerView, null, false);

		super.setOnScrollListener(this);
	}

	@Override
	public void setOnScrollListener(OnScrollListener l) {
		onScrollListener = l;
	}

	/**
	 * Register a callback to be invoked when this list reaches the end (last
	 * item be visible)
	 *
	 * @param onLoadMoreListener The callback to run.
	 */

	public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
		this.onLoadMoreListener = onLoadMoreListener;
	}

	private void onLoadMore() {
		if (onLoadMoreListener != null) {
			loadStatus = LOADING;
			updateFooter();
			onLoadMoreListener.onLoadMore();
		}
	}

	/**
	 * Notify the loading more operation has finished
	 */
	public void onLoadMoreComplete() {
		loadStatus = CONTINUE;
		updateFooter();
	}

	public void onFinishAll() {
		loadStatus = FINISH;
		updateFooter();
	}

	public void onLoadFailed() {
		loadStatus = FAILED;
		updateFooter();
	}

	private void updateFooter() {
		progressBarLoadMore.setVisibility(loadStatus == LOADING ? VISIBLE : GONE);
		txtNoData.setVisibility(loadStatus == FINISH && getCount() == getHeaderViewsCount() + 1 ? VISIBLE : GONE);
		txtLoadFailed.setVisibility(loadStatus == FAILED ? VISIBLE : GONE);
	}

	@Override
	public void onClick(View view) {
		onLoadMore();
	}

	@Override
	public void onScrollStateChanged(AbsListView absListView, int scrollState) {
		if (scrollState == SCROLL_STATE_IDLE) {
			absListView.invalidateViews();
		}

		currentScrollState = scrollState;

		if (onScrollListener != null) {
			onScrollListener.onScrollStateChanged(absListView, scrollState);
		}
	}

	@Override
	public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		if (onScrollListener != null) {
			onScrollListener.onScroll(absListView, firstVisibleItem,
					visibleItemCount, totalItemCount);
		}

		if (onLoadMoreListener != null) {

			if (visibleItemCount == totalItemCount && loadStatus != NOT_START) {
				updateFooter();
				// mLabLoadMore.setVisibility(View.GONE);
				return;
			}

			boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;

			if ((loadStatus == NOT_START || loadStatus == CONTINUE) && loadMore && (currentScrollState != SCROLL_STATE_IDLE || loadStatus == NOT_START)) {
				// mLabLoadMore.setVisibility(View.VISIBLE);
				onLoadMore();
			}

		}
	}

	public interface OnLoadMoreListener {
		void onLoadMore();
	}

}

