package vn.lovemoney.app.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import vn.lovemoney.app.R;
import vn.lovemoney.app.fragment.navigationdrawer.FAQsFragment;
import vn.lovemoney.app.fragment.navigationdrawer.InboxFragment;
import vn.lovemoney.app.fragment.navigationdrawer.SendMessageFragment;

/**
 * Created by 8470p on 3/14/2017.
 */

public class CustomerSupportPagerAdapter extends FragmentStatePagerAdapter {

    public String tabTitles[];

    public CustomerSupportPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        tabTitles = context.getResources().getStringArray(R.array.nav_customer_support);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return InboxFragment.newInstance();
            case 1:
                return FAQsFragment.newInstance();
            case 2:
                return SendMessageFragment.newInstance();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabTitles[position];
    }

}
