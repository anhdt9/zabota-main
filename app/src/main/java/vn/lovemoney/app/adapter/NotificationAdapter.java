package vn.lovemoney.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.other.NotificationItem;

/**
 * Created by tuannguyen on 1/8/16.
 */
public class NotificationAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private List<NotificationItem> mNotifications;

	public NotificationAdapter (Context context, List<NotificationItem> notifications) {
		mInflater = LayoutInflater.from(context);
		mNotifications = notifications;
	}

	@Override
	public int getCount() {
		return mNotifications == null ? 0 : mNotifications.size();
	}

	@Override
	public NotificationItem getItem(int position) {
		return mNotifications.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.listitem_notification, parent, false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else{
			holder = (ViewHolder) convertView.getTag();
		}
		NotificationItem notification = getItem(position);
		holder.txtContent.setText(notification.getMessage());
		String[] createdTime = notification.getCreatedAt().split(" ");
		holder.txtTime.setText(createdTime[0] + "    " + createdTime[1]);

//		Animation animation = null;
//		animation = AnimationUtils.loadAnimation(mContext, R.anim.left_in);
//		animation.setDuration(500);
//		convertView.startAnimation(animation);

		return convertView;
	}

	private class ViewHolder {
		TextView txtContent;
		TextView txtTime;

		ViewHolder(View view) {
			this.txtContent = (TextView) view.findViewById(R.id.content);
			this.txtTime = (TextView) view.findViewById(R.id.time);
		}
	}
}
