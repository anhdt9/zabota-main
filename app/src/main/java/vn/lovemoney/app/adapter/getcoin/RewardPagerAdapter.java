package vn.lovemoney.app.adapter.getcoin;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import vn.lovemoney.app.R;
import vn.lovemoney.app.fragment.getcoin.PaypalRedeemFragment;
import vn.lovemoney.app.fragment.getcoin.TopupFragment;

public class RewardPagerAdapter extends FragmentStatePagerAdapter {


    private static final int FRAGMENT_TOPUP = 0;
    private static final int FRAGMENT_PAYPAL = 1;
    private static final int FRAGMENT_COUNT = 2;

    public String[] getTabTitles() {
        return tabTitles;
    }

    private String tabTitles[];

    public RewardPagerAdapter(FragmentManager manager, Context context) {
        super(manager);
        tabTitles = context.getResources().getStringArray(R.array.nav_get_coin);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case FRAGMENT_TOPUP:
                return TopupFragment.newInstance();
            case FRAGMENT_PAYPAL:
                return PaypalRedeemFragment.newIntance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}