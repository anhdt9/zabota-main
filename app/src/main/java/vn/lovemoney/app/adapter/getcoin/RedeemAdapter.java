package vn.lovemoney.app.adapter.getcoin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.exchance.RedeemItem;

/**
 * Created by 8470p on 3/12/2017.
 */

public class RedeemAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<RedeemItem> mItems;

    public RedeemAdapter(Context context, List<RedeemItem> items) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public RedeemItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RedeemItem redeemItem = getItem(position);

        RedeemAdapter.AppViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_topup, parent, false);
            holder = new RedeemAdapter.AppViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (RedeemAdapter.AppViewHolder) convertView.getTag();
        }

        String creditStr = mContext.getString(R.string.card_title, redeemItem.getProviderName(), redeemItem.getCredit(), redeemItem.getCurrency());
        holder.txtCredit.setText(creditStr);
        String priceStr = mContext.getResources().getString(R.string.price, redeemItem.getExchangePrice());
        holder.txtExchangePrice.setText(priceStr);

        Picasso.with(mContext)
                .load(redeemItem.getLogoUrl())
                .fit()
                .placeholder(R.drawable.ic_image_empty)
                .error(R.drawable.ic_image_empty)
                .into(holder.imageView);

        return convertView;
    }

    private class AppViewHolder {
        private TextView txtCredit;
        private TextView txtExchangePrice;
        private ImageView imageView;

        private AppViewHolder(View v) {
            txtCredit = (TextView) v.findViewById(R.id.credit);
            txtExchangePrice = (TextView) v.findViewById(R.id.price);
            imageView = (ImageView) v.findViewById(R.id.icon);
        }
    }
}
