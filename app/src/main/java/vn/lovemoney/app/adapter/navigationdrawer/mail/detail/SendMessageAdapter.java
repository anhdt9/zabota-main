package vn.lovemoney.app.adapter.navigationdrawer.mail.detail;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.support.GetSupportThead;

/**
 * Created by 8470p on 3/17/2017.
 */

public class SendMessageAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<GetSupportThead> mQuestionItems;

    public SendMessageAdapter(Context context, List<GetSupportThead> navDrawerItems) {
        this.mContext = context;
        this.mQuestionItems = navDrawerItems;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mQuestionItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mQuestionItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GetSupportThead thread = mQuestionItems.get(position);

        if (convertView == null)
            convertView = mInflater.inflate(R.layout.listitem_question, null);

        TextView question = (TextView) convertView.findViewById(R.id.tv_question);
        question.setText(thread.getSubject());
        ImageView hand = (ImageView) convertView.findViewById(R.id.hand);

        if (thread.isSelected()) {
            hand.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_hand_click));
            question.setTextColor(ContextCompat.getColor(mContext, R.color.primary));
        } else {
            hand.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_hand_normal));
            question.setTextColor(ContextCompat.getColor(mContext, R.color.mainBlack));
        }

        return convertView;
    }
}
