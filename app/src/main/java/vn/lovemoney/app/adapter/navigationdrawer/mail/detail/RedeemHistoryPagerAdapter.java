package vn.lovemoney.app.adapter.navigationdrawer.mail.detail;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import vn.lovemoney.app.R;
import vn.lovemoney.app.fragment.navigationdrawer.TransactionHistoryAllFragment;
import vn.lovemoney.app.fragment.navigationdrawer.RedeemHistoryGetRewardFragment;

/**
 * Created by 8470p on 3/14/2017.
 */

public class RedeemHistoryPagerAdapter extends FragmentStatePagerAdapter {

    public String[] getTabTitles() {
        return tabTitles;
    }

    private String tabTitles[];

    public RedeemHistoryPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        tabTitles = context.getResources().getStringArray(R.array.nav_coin_history);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return TransactionHistoryAllFragment.newInstance();
            case 1:
                return RedeemHistoryGetRewardFragment.newInstance();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabTitles[position];
    }
}
