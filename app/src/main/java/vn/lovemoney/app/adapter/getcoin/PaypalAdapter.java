package vn.lovemoney.app.adapter.getcoin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.exchance.PaypalItem;

/**
 * Created by 8470p on 5/17/2017.
 */

public class PaypalAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<PaypalItem> mItems;

    public PaypalAdapter(Context context, List<PaypalItem> items) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public PaypalItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PaypalItem paypalItem = getItem(position);

        PaypalAdapter.AppViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_paypal, parent, false);
            holder = new PaypalAdapter.AppViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (PaypalAdapter.AppViewHolder) convertView.getTag();
        }

        String currency = paypalItem.getCurrency();

        String creditStr = mContext.getString(R.string.change_paypal_card, paypalItem.getCredit(), paypalItem.getCurrency());
        holder.txtCredit.setText(creditStr);
        String priceStr = mContext.getString(R.string.earn_coin, paypalItem.getExchangePrice());
        holder.txtExchangePrice.setText(priceStr);

        return convertView;
    }

    private class AppViewHolder {
        private TextView txtCredit;
        private TextView txtExchangePrice;

        private AppViewHolder(View v) {
            txtCredit = (TextView) v.findViewById(R.id.credit);
            txtExchangePrice = (TextView) v.findViewById(R.id.price);
        }
    }
}
