package vn.lovemoney.app.adapter;

/*
 * Created by Ravi on 29/07/15.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.NavDrawerItem;

public class NavigationDrawerAdapter extends BaseAdapter{

    private static final int TYPE_COUNT = 2;
    private static final int TYPE_DRAWER = 0;
    private static final int TYPE_DRAWER_SECTION = 1;

    private List<NavDrawerItem> mItems;
    private LayoutInflater mInflater;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> items){
        mItems = items;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        NavDrawerItem item = mItems.get(position);
        if(item.getImg() == null){
            return TYPE_DRAWER_SECTION;
        }
        return TYPE_DRAWER;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) == TYPE_DRAWER;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        NavDrawerItem item = (NavDrawerItem) getItem(position);
        if(type == TYPE_DRAWER){
            ViewHolder holder = null;
            if(convertView == null){
                convertView = mInflater.inflate(R.layout.listitem_drawer, parent, false);
                holder = new ViewHolder();
                holder.ivIcon = (ImageView) convertView.findViewById(R.id.icon);
                holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
                convertView.setTag(holder);
            } else{
                holder = (ViewHolder) convertView.getTag();
            }
            holder.ivIcon.setImageDrawable(item.getImg());
            holder.txtTitle.setText(item.getTitle());
        } else {
            TextView txtTitle = null;
            if(convertView == null){
                convertView = mInflater.inflate(R.layout.listitem_drawer_section, parent, false);
                txtTitle = (TextView) convertView.findViewById(R.id.title);
                convertView.setTag(txtTitle);
            } else{
                txtTitle = (TextView) convertView.getTag();
            }
            txtTitle.setText(item.getTitle());
        }
        return convertView;
    }

    private class ViewHolder{
        public ImageView ivIcon;
        public TextView txtTitle;
    }
}
