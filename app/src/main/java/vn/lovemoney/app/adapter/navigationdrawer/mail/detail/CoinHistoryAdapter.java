package vn.lovemoney.app.adapter.navigationdrawer.mail.detail;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.CoinHistory;

public class CoinHistoryAdapter extends ArrayAdapter<CoinHistory> {
	private LayoutInflater mInflater;
	private Context mContext;

	public CoinHistoryAdapter(Context context, List<CoinHistory> objects) {
		super(context, R.layout.listitem_app_offer, objects);
		mContext = context;
		mInflater = LayoutInflater.from(context);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.listitem_activity_history, parent, false);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		CoinHistory current = getItem(position);

		String createAt = current.getCreatedAt();
		String[] dateTime = createAt.split(" ");
		int status = current.getStatus();

		holder.txtTime.setText(dateTime[1] + "\n" + dateTime[0]);
		String description = mContext.getString(R.string.redeem_description_script, current.getMessage(), current.getStatus() == 1 ? mContext.getString(R.string.confirm_done) : mContext.getString(R.string.wait_confirm));
		holder.txtDescription.setText(Html.fromHtml(description));
		String coin = mContext.getString(R.string.earn_coin, current.getAmount());
		holder.txtPrice.setText(coin);
		return convertView;
	}

	private class ViewHolder {
		public TextView txtTime;
		public TextView txtDescription;
		public TextView txtPrice;

		public ViewHolder(View view) {
			txtTime = (TextView) view.findViewById(R.id.time);
			txtDescription = (TextView) view.findViewById(R.id.description);
			txtPrice = (TextView) view.findViewById(R.id.price);
		}

	}

}
