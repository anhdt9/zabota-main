package vn.lovemoney.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.bonus.coin.event.EventItem;

/**
 * Created by DTA on 14/03/2017.
 */

public class EventAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<EventItem> mItems;
    private Context mContext;

    public EventAdapter(Context context, List<EventItem> mItems) {
        mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mItems = mItems;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        EventItem eventItem = (EventItem) getItem(position);

        EventAdapter.AppViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_event, parent, false);
            holder = new EventAdapter.AppViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (EventAdapter.AppViewHolder) convertView.getTag();
        }

        holder.name.setText(eventItem.getTitle());
        Picasso.with(mContext)
                .load(eventItem.getIconUrl())
                .fit()
                .centerInside()
                .placeholder(R.drawable.ic_image_empty)
                .error(R.drawable.ic_image_empty)
                .into(holder.icon);
        holder.coin.setText(String.format("+%d", eventItem.getBonus()));

        return convertView;
    }

    public class AppViewHolder {
        public ImageView icon;
        public TextView name;
        public TextView coin;

        public AppViewHolder(View v) {
            icon = (ImageView) v.findViewById(R.id.icon);
            name = (TextView) v.findViewById(R.id.tv_name_event);
            coin = (TextView) v.findViewById(R.id.tv_coin_event);
        }
    }
}
