package vn.lovemoney.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.user.ScriptUserInfo;


public class TopChartAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<ScriptUserInfo> mTopUsers;

    public TopChartAdapter(Context context, List<ScriptUserInfo> topUsers) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mTopUsers = topUsers;
    }

    @Override
    public int getCount() {
        return mTopUsers == null ? 0 : mTopUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return mTopUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_top_chart_new, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        ScriptUserInfo user = (ScriptUserInfo) getItem(position);
        String rank = String.valueOf(user.getRank());
        setImageView(holder.iconRank, position);
        setTextView(holder.txtRank, position, rank);

        Picasso.with(mContext)
                .load(user.getAvatarUrl())
                .centerCrop()
                .fit()
                .placeholder(R.drawable.ic_image_empty)
                .error(R.drawable.ic_image_empty)
                .into(holder.ivAvatar);
        holder.txtName.setSelected(user.isMe());
        holder.txtName.setText(user.getName());
        String earn = String.format("%d", user.getTotalEarning());
        holder.txtTotalEarn.setText(earn);
        return convertView;
    }

    private class Holder {
        public TextView txtRank;
        public ImageView ivAvatar;
        public TextView txtName;
        public TextView txtTotalEarn;
        public ImageView iconRank;

        public Holder(View view) {
            txtRank = (TextView) view.findViewById(R.id.rank_text);
            ivAvatar = (ImageView) view.findViewById(R.id.avatar);
            txtName = (TextView) view.findViewById(R.id.name);
            txtTotalEarn = (TextView) view.findViewById(R.id.total_earn);
            iconRank = (ImageView) view.findViewById(R.id.rank_icon);
        }
    }

    private void setImageView(ImageView imageView, int position) {

        if (position >= 5) {
            imageView.setVisibility(View.GONE);
            return;
        } else {
            imageView.setVisibility(View.VISIBLE);
        }
        switch (position) {
            case 0:
                imageView.setImageResource(R.drawable.icon_rank_01);
                break;
            case 1:
                imageView.setImageResource(R.drawable.icon_rank_02);
                break;
            case 2:
                imageView.setImageResource(R.drawable.icon_rank_03);
                break;
            case 3:
                imageView.setImageResource(R.drawable.icon_rank_04);
                break;
            case 4:
                imageView.setImageResource(R.drawable.icon_rank_05);
                break;
            default:
                imageView.setImageResource(R.drawable.icon_rank_coin);
                break;
        }
    }

    private void setTextView(TextView textView, int position, String rank) {

        if (position < 5) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(rank);
        }
    }
}