package vn.lovemoney.app.adapter.makecoin;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import vn.lovemoney.app.R;
import vn.lovemoney.app.fragment.makecoin.InstallAppFragment;
import vn.lovemoney.app.fragment.makecoin.OpenAppFragment;

public class MakeCoinPagerAdapter extends FragmentStatePagerAdapter {

    private static final int FRAGMENT_INSTALL_APP = 0;
    private static final int FRAGMENT_OPEN_APP = 1;
    private static final int FRAGMENT_COUNT = 2;

    private SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public String[] getTabTitles() {
        return tabTitles;
    }

    private String tabTitles[];

    public MakeCoinPagerAdapter(FragmentManager manager, Context context) {
        super(manager);
        tabTitles = context.getResources().getStringArray(R.array.nav_make_coin);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case FRAGMENT_INSTALL_APP:
                return InstallAppFragment.newInstance();
            case FRAGMENT_OPEN_APP:
                return OpenAppFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}