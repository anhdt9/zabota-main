package vn.lovemoney.app.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import vn.lovemoney.app.R;
import vn.lovemoney.app.fragment.bonuscoin.EventListFragment;
import vn.lovemoney.app.fragment.bonuscoin.OtherBonusFragment;
import vn.lovemoney.app.fragment.bonuscoin.SocialListFragment;

public class BonusCoinPagerAdapter extends FragmentStatePagerAdapter {


    private static final int FRAGMENT_BONUS_SOCIAL = 0;
    private static final int FRAGMENT_BONUS_EVENTS = 1;
    private static final int FRAGMENT_BONUS_OTHER = 2;
    private static final int FRAGMENT_COUNT = 3;

    public String[] getTabTitles() {
        return tabTitles;
    }

    private String tabTitles[];

    public BonusCoinPagerAdapter(FragmentManager manager, Context context) {
        super(manager);
        tabTitles = context.getResources().getStringArray(R.array.nav_bonus_coin);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case FRAGMENT_BONUS_SOCIAL:
                return new SocialListFragment();
            case FRAGMENT_BONUS_EVENTS:
                return EventListFragment.newInstance();
            case FRAGMENT_BONUS_OTHER:
                return OtherBonusFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}