package vn.lovemoney.app.adapter.bonus.social;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.bonus.coin.social.SocialItem;

/**
 * Created by 8470p on 3/12/2017.
 */

public class SocialAdapter extends BaseAdapter{
    private LayoutInflater mInflater;
    private List<SocialItem> mItems;

    public SocialAdapter(Context context, List<SocialItem> items) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SocialItem socialItem = (SocialItem) getItem(position);

        SocialAdapter.AppViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_social, parent, false);
            holder = new SocialAdapter.AppViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (SocialAdapter.AppViewHolder) convertView.getTag();
        }

        holder.txtTitle.setText(socialItem.getTitle());
        holder.icon.setImageDrawable(socialItem.getIcon());

        return convertView;
    }

    private class AppViewHolder {
        public TextView txtTitle;
        public ImageView icon;

        public AppViewHolder(View v) {
            txtTitle = (TextView) v.findViewById(R.id.tv_title);
            icon = (ImageView) v.findViewById(R.id.icon);
        }
    }
}
