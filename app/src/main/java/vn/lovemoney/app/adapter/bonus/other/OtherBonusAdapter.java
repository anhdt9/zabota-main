package vn.lovemoney.app.adapter.bonus.other;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.bonus.coin.other.OtherItem;

/**
 * Created by 8470p on 3/12/2017.
 */

public class OtherBonusAdapter extends BaseAdapter{
    private LayoutInflater mInflater;
    private List<OtherItem> mItems;

    public OtherBonusAdapter(Context context, List<OtherItem> items) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OtherItem otherItem = (OtherItem) getItem(position);

        OtherBonusAdapter.AppViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_bonus_other, parent, false);
            holder = new OtherBonusAdapter.AppViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (OtherBonusAdapter.AppViewHolder) convertView.getTag();
        }

        holder.txtTitle.setText(otherItem.getTitle());
        holder.icon.setImageDrawable(otherItem.getIcon());

        return convertView;
    }

    private class AppViewHolder {
        public TextView txtTitle;
        public ImageView icon;

        public AppViewHolder(View v) {
            txtTitle = (TextView) v.findViewById(R.id.tv_title);
            icon = (ImageView) v.findViewById(R.id.icon);
        }
    }
}
