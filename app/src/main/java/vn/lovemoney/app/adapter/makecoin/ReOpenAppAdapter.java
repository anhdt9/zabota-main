package vn.lovemoney.app.adapter.makecoin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.user.ReOpenApp;

/**
 * Created by vuong_000 on 1/5/2016.
 */
public class ReOpenAppAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<ReOpenApp> mItems;

    public ReOpenAppAdapter(Context context, List<ReOpenApp> items) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_re_open_app, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ReOpenApp offer = (ReOpenApp) getItem(position);
        Picasso.with(mContext)
                .load(offer.getIconUrl())
                .fit()
                .placeholder(R.drawable.ic_image_empty)
                .error(R.drawable.ic_image_empty)
                .into(holder.ivIcon);

        holder.txtName.setText(offer.getName());
        int remainHours = offer.getRemainHour();
        String waitingTimeText = remainHours > 0 ? mContext.getString(R.string.remain_times, offer.getRemainHour()) : mContext.getString(R.string.open_now);
        if (remainHours > 0) {
            holder.timeState.setImageResource(R.drawable.icon_timing_pending);
            holder.layoutPrice.setBackgroundResource(R.drawable.background_icon_xu_pending_custom);
        } else {
            holder.timeState.setImageResource(R.drawable.icon_timing_active);
            holder.layoutPrice.setBackgroundResource(R.drawable.background_icon_xu_active_custom);
        }
        holder.txtWaitingTime.setText(waitingTimeText);
        String price = String.format("%d", offer.getCurrentBonus());
        holder.txtPrice.setText(price);
        return convertView;
    }

//    private String confixUrl(String url) {
//        url = url.replace("\\/\\/", "");
//        //Log.i("duy.pq", "url=" + url);
//        url = "http:" + url;
//        return url;
//    }

    private class ViewHolder {
        public ImageView ivIcon;
        public ImageView timeState;
        public TextView txtName;
        public TextView txtWaitingTime;
        public TextView txtPrice;
        public LinearLayout layoutPrice;

        public ViewHolder(View v) {
            ivIcon = (ImageView) v.findViewById(R.id.icon);
            txtName = (TextView) v.findViewById(R.id.name);
            txtWaitingTime = (TextView) v.findViewById(R.id.waiting_time);
            txtPrice = (TextView) v.findViewById(R.id.price);
            timeState = (ImageView) v.findViewById(R.id.state);
            layoutPrice = (LinearLayout) v.findViewById(R.id.layout_price_status);
        }
    }
}
