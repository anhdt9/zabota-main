package vn.lovemoney.app.adapter.navigationdrawer.mail.detail;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.support.GetMessage;

/**
 * Created by DTA on 16/03/2017.
 */

public class MessageItemAdapter extends BaseAdapter {
    private Context context;
    private List<GetMessage> messagesItems;

    public MessageItemAdapter(Context context, List<GetMessage> navDrawerItems) {
        this.context = context;
        this.messagesItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return messagesItems.size();
    }

    @Override
    public GetMessage getItem(int position) {
        return messagesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        GetMessage m = messagesItems.get(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (messagesItems.get(position).isAdmin()) {
            convertView = mInflater.inflate(R.layout.list_item_message_left, null);
        } else {
            convertView = mInflater.inflate(R.layout.list_item_message_right, null);
        }

        TextView txtMsg = (TextView) convertView.findViewById(R.id.txtMsg);
        txtMsg.setText(m.getContent());

        return convertView;
    }
}
