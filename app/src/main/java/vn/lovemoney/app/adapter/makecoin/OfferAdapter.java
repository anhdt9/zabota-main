package vn.lovemoney.app.adapter.makecoin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.android.app.NewAppOffer;

/**
 * Created by vuong_000 on 11/27/2015.
 */
public class OfferAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<NewAppOffer> mItems;

    public OfferAdapter(Context context, List<NewAppOffer> items) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//		int type = getItemViewType(position);
        NewAppOffer appOffer = (NewAppOffer) getItem(position);
        AppViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_app_offer_new, parent, false);
            holder = new AppViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (AppViewHolder) convertView.getTag();
        }
        Picasso.with(mContext)
                .load(appOffer.getIconUrl())
                .fit()
                .placeholder(R.drawable.ic_image_empty)
                .error(R.drawable.ic_image_empty)
                .into(holder.ivIcon);

        holder.txtName.setText(appOffer.getName());
        int state = appOffer.getStatus();
        holder.ivState.setSelected(state == NewAppOffer.STATUS_INSTALLED || state == NewAppOffer.STATUS_VERIFIED);
        holder.txtStateAlert.setText(state == NewAppOffer.STATUS_INSTALLED ? R.string.confirm_now : (state == NewAppOffer.STATUS_VERIFIED ? R.string.review_now : R.string.install_now));
        holder.txtPrice.setText(String.valueOf(appOffer.getBonus()));

        return convertView;
    }

    private class AppViewHolder {
        private ImageView ivIcon;
        private TextView txtName;
        private ImageView ivState;
        private TextView txtStateAlert;
        private TextView txtPrice;

        public AppViewHolder(View v) {
            ivIcon = (ImageView) v.findViewById(R.id.icon);
            txtName = (TextView) v.findViewById(R.id.name);
            ivState = (ImageView) v.findViewById(R.id.state);
            txtStateAlert = (TextView) v.findViewById(R.id.state_alert);
            txtPrice = (TextView) v.findViewById(R.id.price);
        }
    }

}
