package vn.lovemoney.app.adapter.navigationdrawer.mail.detail;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.exchance.ExchangeItem;
import vn.lovemoney.app.model.exchance.RedeemExchangeHistoryItem;
import vn.lovemoney.app.util.FreeXuUtils;

/**
 * Created by vuong_000 on 12/15/2015.
 */
public class RedeemHistoryAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<RedeemExchangeHistoryItem> mRedeems;

    public RedeemHistoryAdapter(Context context, List<RedeemExchangeHistoryItem> redeems) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mRedeems = redeems;
    }

    @Override
    public int getCount() {
        return mRedeems == null ? 0 : mRedeems.size();
    }

    @Override
    public Object getItem(int position) {
        return mRedeems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_redeem_history_full, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final RedeemExchangeHistoryItem redeem = (RedeemExchangeHistoryItem) getItem(position);
        String formattedContent = null;

        String createAt = redeem.getCreatedAt();
        String[] dateTime = createAt.split(" ");

        int type = redeem.getType();
        if (type == ExchangeItem.TYPE_MOBILE_CARD || type == ExchangeItem.TYPE_SERVICE_CARD) {
            formattedContent = mContext.getString(R.string.redeem_description, redeem.getName(), redeem.getSerial(), redeem.getPin(), dateTime[0],
                    redeem.getStatus() == 1 ? mContext.getString(R.string.confirm_done) : mContext.getString(R.string.wait_confirm));
            holder.layoutUseCopy.setVisibility(View.VISIBLE);
        } else {
            formattedContent = mContext.getString(R.string.redeem_description_script_card, redeem.getName(), redeem.getStatus() == 1 ? mContext.getString(R.string.confirm_done) : mContext.getString(R.string.wait_confirm));
            holder.layoutUseCopy.setVisibility(View.GONE);
        }
        Picasso.with(mContext)
                .load(redeem.getIconUrl())
                .fit()
                .placeholder(R.drawable.ic_image_empty)
                .error(R.drawable.ic_image_empty)
                .into(holder.imgCard);

        holder.btnUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder builder = new StringBuilder("tel:*100*");
                builder.append(redeem.getPin().replaceAll("[^0-9+]", ""));
                builder.append(Uri.encode("#"));
                String uri = builder.toString();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(uri));
                mContext.startActivity(intent);
            }
        });

        holder.btnCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cardInfo = mContext.getString(R.string.card_information_script, redeem.getName(), redeem.getSerial(), redeem.getPin());
                FreeXuUtils.saveToClipboard(mContext, cardInfo);
                Toast.makeText(mContext, mContext.getString(R.string.copy_success), Toast.LENGTH_LONG).show();
            }
        });

        holder.txtDate.setText(dateTime[1] + "\n" + dateTime[0]);
        holder.txtContent.setText(Html.fromHtml(formattedContent));

        return convertView;
    }

    private class ViewHolder {
        private TextView txtDate;
        private TextView txtContent;
        private ImageView imgCard;
        private Button btnUse, btnCopy;
        private LinearLayout layoutUseCopy;

        private ViewHolder(View view) {
            txtDate = (TextView) view.findViewById(R.id.date);
            txtContent = (TextView) view.findViewById(R.id.content);
            imgCard = (ImageView) view.findViewById(R.id.card);
            btnCopy = (Button) view.findViewById(R.id.btn_copy);
            btnUse = (Button) view.findViewById(R.id.btn_use);
            layoutUseCopy = (LinearLayout) view.findViewById(R.id.button_container);
        }
    }
}
