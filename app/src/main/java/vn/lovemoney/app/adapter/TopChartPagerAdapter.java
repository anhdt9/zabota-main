package vn.lovemoney.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import vn.lovemoney.app.fragment.TopChartFragment;
import vn.lovemoney.app.model.ChartInfo;

/**
 * Created by tuandigital on 8/25/16.
 */
public class TopChartPagerAdapter extends FragmentStatePagerAdapter {

    public static List<ChartInfo> mItems;

    public TopChartPagerAdapter(FragmentManager fm, List<ChartInfo> items) {
        super(fm);
        mItems = items;
    }

    @Override
    public Fragment getItem(int position) {
        ChartInfo chartInfo = mItems.get(position);
        return TopChartFragment.newInstance(chartInfo.getChartName());
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        ChartInfo info = mItems.get(position);
        return info.getDisplayName();
    }
}
