package vn.lovemoney.app.adapter.bonus.other;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.bonus.coin.other.MiniGameItem;

/**
 * Created by DTA on 13/03/2017.
 */

public class MiniGameAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<MiniGameItem> mItems;

    public MiniGameAdapter(Context mContext, List<MiniGameItem> mItems) {
        this.mItems = mItems;
        this.mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        MiniGameItem miniGameItem = (MiniGameItem) getItem(position);

        ViewHolder holder = null;
        if (v == null) {
            v = mInflater.inflate(R.layout.listitem_minigame, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.icon.setImageResource(miniGameItem.getIconResId());
        holder.name.setText(miniGameItem.getName());

        return v;
    }

    private class ViewHolder {
        public ImageView icon;
        public TextView name;

        public ViewHolder(View v) {
            icon = (ImageView) v.findViewById(R.id.icon);
            name = (TextView) v.findViewById(R.id.name);
        }
    }
}
