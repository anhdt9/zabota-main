package vn.lovemoney.app.adapter.bonus.social;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.facebook.FacebookItem;

/**
 * Created by DTA on 13/03/2017.
 */

public class FacebookAdapter extends BaseAdapter {

    private static final int POST = 1;

    private Context mContext;
    private LayoutInflater mInflater;
    private List<FacebookItem> mItems;

    public FacebookAdapter(Context mContext, List<FacebookItem> mItems) {
        this.mContext = mContext;
        this.mItems = mItems;
        this.mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        FacebookItem item = (FacebookItem) getItem(position);

        FacebookViewHolder holder;
        if (v == null) {
            v = mInflater.inflate(R.layout.listitem_like_fanpage, parent, false);
            holder = new FacebookViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (FacebookViewHolder) v.getTag();
        }
        Picasso.with(mContext)
                .load(item.getIconUrl())
                .centerCrop()
                .fit()
                .placeholder(R.drawable.ic_image_empty)
                .into(holder.icon);
        holder.type.setImageDrawable((item.getType() == POST) ? ContextCompat.getDrawable(mContext, R.drawable.icon_post) : ContextCompat.getDrawable(mContext, R.drawable.icon_page));
        holder.name.setText(item.getName());
        holder.coin.setText("+ " + item.getBonus());

        return v;
    }

    private class FacebookViewHolder {
        public ImageView icon;
        public ImageView type;
        public TextView name;
        public TextView coin;

        public FacebookViewHolder(View v) {
            icon = (ImageView) v.findViewById(R.id.icon);
            type = (ImageView) v.findViewById(R.id.icon_type);
            name = (TextView) v.findViewById(R.id.tv_name_page);
            coin = (TextView) v.findViewById(R.id.tv_coin);
        }
    }
}
