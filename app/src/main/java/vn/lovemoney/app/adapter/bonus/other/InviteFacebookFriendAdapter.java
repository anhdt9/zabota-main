package vn.lovemoney.app.adapter.bonus.other;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.facebook.FacebookFriend;

/**
 * Created by DTA on 15/03/2017.
 */

public class InviteFacebookFriendAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<FacebookFriend> mItems;

    public InviteFacebookFriendAdapter(Context context, List<FacebookFriend> items) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FacebookFriend friend = (FacebookFriend) getItem(position);

        InviteFacebookFriendAdapter.AppViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_invite_fb_friend, parent, false);
            holder = new InviteFacebookFriendAdapter.AppViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (InviteFacebookFriendAdapter.AppViewHolder) convertView.getTag();
        }

        Picasso.with(mContext)
                .load(friend.getPictureUrl())
                .centerCrop()
                .fit()
                .placeholder(R.drawable.ic_image_empty)
                .into(holder.ic);
        holder.name.setText(friend.getName());
        holder.cb.setChecked(friend.isCheck());

        return convertView;
    }

    private class AppViewHolder {
        public ImageView ic;
        public TextView name;
        public CheckBox cb;

        public AppViewHolder(View v) {
            ic = (ImageView) v.findViewById(R.id.item_icon);
            name = (TextView) v.findViewById(R.id.item_name);
            cb = (CheckBox) v.findViewById(R.id.item_cb);
        }
    }
    public void addData(List<FacebookFriend> list) {
        mItems.clear();
        mItems.addAll(list);
        notifyDataSetChanged();
    }
}
