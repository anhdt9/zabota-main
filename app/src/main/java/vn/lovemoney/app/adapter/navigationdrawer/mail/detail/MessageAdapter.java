package vn.lovemoney.app.adapter.navigationdrawer.mail.detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.support.GetTicket;

/**
 * Created by 8470p on 3/16/2017.
 */
public class MessageAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<GetTicket> mMailboxes;
    private static final int PROCESSING = 1;

    public MessageAdapter(Context context, List<GetTicket> redeems) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mMailboxes = redeems;
    }

    @Override
    public int getCount() {
        return mMailboxes == null ? 0 : mMailboxes.size();
    }

    @Override
    public Object getItem(int position) {
        return mMailboxes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageAdapter.ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_mailbox, parent, false);

            holder = new MessageAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (MessageAdapter.ViewHolder) convertView.getTag();
        }

        GetTicket ticket = (GetTicket) getItem(position);
        String time[] = ticket.getTime();

        holder.tvHour.setText(time[0]);
        holder.tvDate.setText(time[1]);
        holder.tvId.setText("#" + String.format("%02d", ticket.getTicketId()) + " : ");
        holder.tvTitle.setText(ticket.getSubject());
        holder.tvStatus.setText((ticket.getStatus() == PROCESSING) ? mContext.getString(R.string.processing) : mContext.getString(R.string.confirm_done));
        holder.tvStatus.setTextColor((ticket.getStatus() == PROCESSING) ? mContext.getResources().getColor(R.color.red) : mContext.getResources().getColor(R.color.mainGray));
        holder.tvId.setTextColor((ticket.getStatus() == PROCESSING) ? mContext.getResources().getColor(R.color.mainGreen) : mContext.getResources().getColor(R.color.mainGray));
        holder.tvTitle.setTextColor((ticket.getStatus() == PROCESSING) ? mContext.getResources().getColor(R.color.mainGreen) : mContext.getResources().getColor(R.color.mainGray));
        holder.process.setVisibility((ticket.getStatus() == PROCESSING) ? View.VISIBLE : View.GONE);
        holder.imgStatus.setVisibility((ticket.getStatus() == PROCESSING) ? View.VISIBLE : View.GONE);

        return convertView;
    }

    private class ViewHolder {
        private TextView tvHour;
        private TextView tvDate;
        private TextView tvId;
        private TextView tvTitle;
        private TextView tvStatus;
        private TextView process;
        private ImageView imgStatus;

        private ViewHolder(View view) {
            tvHour = (TextView) view.findViewById(R.id.hour);
            tvDate = (TextView) view.findViewById(R.id.date);
            tvId = (TextView) view.findViewById(R.id.tv_id);
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvStatus = (TextView) view.findViewById(R.id.tv_status);
            process = (TextView) view.findViewById(R.id.tv_status_processing);
            imgStatus = (ImageView) view.findViewById(R.id.img_status_processing);
        }

    }
}

