package vn.lovemoney.app.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import vn.lovemoney.app.R;
import vn.lovemoney.app.fragment.WebOnlyFragment;

/**
 * Created by 8470p on 3/15/2017.
 */

public class OtherInfoPagerAdapter extends FragmentStatePagerAdapter {

    private static final int PAGE_COUNT = 3;


    public String[] getTabTitles() {
        return tabTitles;
    }

    private String tabTitles[];

    public OtherInfoPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        tabTitles = context.getResources().getStringArray(R.array.nav_other_info);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return WebOnlyFragment.newInstance("https://imoads.net/zabota/app_static/about");
            case 1:
                return WebOnlyFragment.newInstance("https://imoads.net/zabota/app_static/contact");
            case 2:
                return WebOnlyFragment.newInstance("https://imoads.net/zabota/app_static/policy");
        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return tabTitles[position];
    }
}
