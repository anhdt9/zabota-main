package vn.lovemoney.app.adapter.navigationdrawer.mail.detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.navigationdrawer.mail.detail.FAQs;

/**
 * Created by 8470p on 3/17/2017.
 */

public class FaqsAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<FAQs> mFAQs;

    public FaqsAdapter(Context context, List<FAQs> faqs) {
        mInflater = LayoutInflater.from(context);
        mFAQs = faqs;
    }

    @Override
    public int getCount() {
        return mFAQs == null ? 0 : mFAQs.size();
    }

    @Override
    public Object getItem(int position) {
        return mFAQs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FaqsAdapter.ViewHolder holder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_fags, parent, false);

            holder = new FaqsAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (FaqsAdapter.ViewHolder) convertView.getTag();
        }
        FAQs redeem = (FAQs) getItem(position);

        holder.txtStt.setText(String.valueOf(redeem.getId()));
        holder.txtContent.setText(redeem.getQuestion());

        return convertView;
    }

    private class ViewHolder {
        private TextView txtStt;
        private TextView txtContent;

        private ViewHolder(View view) {
            txtStt = (TextView) view.findViewById(R.id.stt);
            txtContent = (TextView) view.findViewById(R.id.content);
        }

    }
}
