package vn.lovemoney.app.activity.bonus.other.share;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.share.model.GameRequestContent;
import com.facebook.share.widget.GameRequestDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.BaseActivity;
import vn.lovemoney.app.adapter.bonus.other.InviteFacebookFriendAdapter;
import vn.lovemoney.app.model.FacebookAppRequest;
import vn.lovemoney.app.model.IntegerResponse;
import vn.lovemoney.app.model.facebook.FacebookFriend;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by DTA on 15/03/2017.
 */

public class InviteFacebookFriendActivity extends BaseActivity {

    private static final Object mShareFacebookLock = new Object();

    private Context mContext;
    private InviteFacebookFriendAdapter adapter;
    private TextView tv_counter;
    private GameRequestDialog mGameRequestDialog;
    private CallbackManager mCallbackManager;
    private List<FacebookFriend> mFriends;
    private CheckBox cb_all;
    private int cnt;
    private TextView load_failed;
    private static final int LIMIT_INVITABLE_FRIENDS = 45;
    private static final int LIMIT_FRIENDS = 5;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setTitle(R.string.item_share_app);
        ListView lv = (ListView) findViewById(R.id.list);
        Button btn_send = (Button) findViewById(R.id.btn_send);
        cb_all = (CheckBox) findViewById(R.id.cb_all);
        tv_counter = (TextView) findViewById(R.id.tv_counter);
        load_failed = (TextView) findViewById(R.id.load_failed);
        load_failed.setVisibility(View.GONE);
        cnt = 0;
        mFriends = new ArrayList<>();
        adapter = new InviteFacebookFriendAdapter(this, mFriends);
        lv.setAdapter(adapter);
        lv.invalidate();

        mCallbackManager = CallbackManager.Factory.create();
        initializeGameRequestDialog();
        loadFriends();

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cnt == 0) {
                    Toast.makeText(mContext, mContext.getString(R.string.select_friend_notification), Toast.LENGTH_SHORT).show();
                } else {
                    GameRequestContent content = new GameRequestContent.Builder()
                            .setMessage(mContext.getResources().getString(R.string.app_name))
                            .setTitle(mContext.getResources().getString(R.string.share_app_title))
                            .setRecipients(getItemChecked())
                            .build();
                    mGameRequestDialog.show(content);
                }
            }
        });

        cb_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkAll(cb_all.isChecked());
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FacebookFriend friend = mFriends.get(position);
                boolean checkState = friend.isCheck();
                checkState = !checkState;
                friend.setCheck(checkState);
                if (checkState) {
                    cnt++;
                } else {
                    cnt--;
                }
                cb_all.setChecked(cnt == mFriends.size());
                tv_counter.setText(mContext.getResources().getString(R.string.counter, cnt, mFriends.size()));
                adapter.notifyDataSetChanged();
            }
        });
        load_failed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFriends();
            }
        });
        ImageButton ibBack = (ImageButton) findViewById(android.R.id.home);
        ibBack.setVisibility(shouldEnableBack() ? View.VISIBLE : View.GONE);
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHomeClicked();
            }
        });
    }

    private void checkAll(boolean isChecked) {
        synchronized (mShareFacebookLock) {
            for (int i = 0; i < mFriends.size(); i++) {
                FacebookFriend item = mFriends.get(i);
                item.setCheck(isChecked);
            }
        }
        if (isChecked) {
            cnt = mFriends.size();
        } else {
            cnt = 0;
        }
        tv_counter.setText(mContext.getResources().getString(R.string.counter, cnt, mFriends.size()));
        adapter.notifyDataSetChanged();
    }

    private List<String> getItemChecked() {
        List<String> selectedFriends = new ArrayList<>();
        for (int i = 0; i < mFriends.size(); i++) {
            FacebookFriend item = mFriends.get(i);
            if (item.isCheck()) {
                selectedFriends.add(item.getId());
            }
        }
        return selectedFriends;
    }

    private void loadFriends() {
        new AsyncTask<Void, Void, List<FacebookFriend>>() {
            @Override
            protected List<FacebookFriend> doInBackground(Void... voids) {
                Bundle params = new Bundle();
                params.putInt("limit", 100);
                params.putString("fields", "id,name,picture.type(normal)");

                Stack<FacebookFriend> appFriends = new Stack<>();

                GraphRequest friendRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/friends", params, HttpMethod.GET);

                GraphResponse friendResponse = friendRequest.executeAndWait();
                if(friendResponse.getError() != null){
                    return null;
                }
                JSONObject result = friendResponse.getJSONObject();
                try {
                    if (result != null && result.has("data")) {
                        JSONArray data = result.getJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject friend = data.getJSONObject(i);

                            FacebookFriend fr = new FacebookFriend();
                            fr.setName(friend.getString("name"));
                            fr.setPictureUrl(friend.getJSONObject("picture").getJSONObject("data").getString("url"));
                            fr.setId(friend.getString("id"));
                            appFriends.push(fr);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Stack<FacebookFriend> invitableFriends = new Stack<>();

                GraphRequest invitableFriendRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/invitable_friends", params, HttpMethod.GET);
                GraphResponse invitableFriendResponse = invitableFriendRequest.executeAndWait();
                if(invitableFriendResponse.getError() != null){
                    return null;
                }
                if (invitableFriendResponse.getError() == null) {
                    JSONObject result2 = invitableFriendResponse.getJSONObject();
                    try {
                        if (result2 != null && result2.has("data")) {
                            JSONArray data = result2.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject friend = data.getJSONObject(i);

                                FacebookFriend fr = new FacebookFriend();
                                fr.setName(friend.getString("name"));
                                fr.setPictureUrl(friend.getJSONObject("picture").getJSONObject("data").getString("url"));
                                fr.setId(friend.getString("id"));
                                invitableFriends.push(fr);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                final List<FacebookFriend> ret = new ArrayList<>();
                int friendAddedCount = 0;
                int invitableFriendAddedCount = 0;
                while (ret.size() < 50 && (!appFriends.isEmpty() || !invitableFriends.isEmpty())) {
                    if (!invitableFriends.isEmpty() && (invitableFriendAddedCount < LIMIT_INVITABLE_FRIENDS || appFriends.isEmpty())) {
                        ret.add(invitableFriends.pop());
                        invitableFriendAddedCount++;
                    } else if (!appFriends.isEmpty() && (friendAddedCount < LIMIT_FRIENDS || invitableFriends.isEmpty())) {
                        friendAddedCount++;
                        ret.add(appFriends.pop());
                    }

                }
                return ret;
            }

            @Override
            protected void onPostExecute(List<FacebookFriend> friendIds) {
                if (friendIds != null) {
                    load_failed.setVisibility(View.GONE);
                    mFriends = friendIds;
                    tv_counter.setText(mContext.getResources().getString(R.string.counter, cnt, mFriends.size()));
                    dismissProgressDialog();
                    adapter.addData(mFriends);
                } else{
                    load_failed.setVisibility(View.VISIBLE);
                }
            }

            @Override
            protected void onCancelled() {
                dismissProgressDialog();
            }
        }.execute();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public int getView() {
        return R.layout.activity_invite_fb_friend;
    }

    private void initializeGameRequestDialog() {
        mGameRequestDialog = new GameRequestDialog(this);
        mGameRequestDialog.registerCallback(mCallbackManager,
                new FacebookCallback<GameRequestDialog.Result>() {
                    public void onSuccess(GameRequestDialog.Result result) {
                        if (result.getRequestRecipients().size() > 0) {
                            Call<IntegerResponse> call = RestClient.getInstance(mContext).getFacebookApi().submitFacebookAppRequest(new FacebookAppRequest(result.getRequestId(), result.getRequestRecipients()));
                            call.enqueue(new RestCallback<IntegerResponse>() {
                                @Override
                                public void failure(RestError error) {
                                    if (error.getCode() != 0) {
                                        Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void success(IntegerResponse response) {
                                    Singleton.getInstance().setShouldUpdateProfile(true);
                                    EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
                                    Toast.makeText(mContext, response.getDisplayMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }

                    public void onCancel() {
                    }

                    public void onError(FacebookException error) {
                    }
                }
        );
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if (e.getEvent() == MessageEvent.SHOW_NOTIFICATION) {
            showNotification(this, e);
        }
    }
}
