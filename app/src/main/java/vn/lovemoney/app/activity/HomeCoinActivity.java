
package vn.lovemoney.app.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAdOptions;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyReward;
import com.adcolony.sdk.AdColonyRewardListener;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.internal.Utility;
import com.facebook.login.LoginManager;
import com.facebook.share.internal.ShareConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.dialog.EnterReferralCodeDialog;
import vn.lovemoney.app.dialog.RateConfirmationDialog;
import vn.lovemoney.app.fragment.BonusCoinFragment;
import vn.lovemoney.app.fragment.DrawerFragment;
import vn.lovemoney.app.fragment.MakeCoinFragment;
import vn.lovemoney.app.fragment.NotificationListFragment;
import vn.lovemoney.app.fragment.RedeemFragment;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.Event;
import vn.lovemoney.app.model.FacebookAppRequest;
import vn.lovemoney.app.model.IntegerResponse;
import vn.lovemoney.app.model.OneSignalInfo;
import vn.lovemoney.app.model.body.OneSignalIdBody;
import vn.lovemoney.app.model.user.ProfileResponse;
import vn.lovemoney.app.model.user.UserInfo;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.ActivityNavigator;
import vn.lovemoney.app.util.AlarmServiceUtil;
import vn.lovemoney.app.util.AppInfoManager;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.FacebookUtils;
import vn.lovemoney.app.util.FreeXuUtils;
import vn.lovemoney.app.util.GuideUtils;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;


public class HomeCoinActivity extends BaseActivity implements DrawerFragment.FragmentDrawerListener, SharedPreferences.OnSharedPreferenceChangeListener, View.OnClickListener, AdColonyRewardListener {

    private static final int TOP_CHART = 0;
    private static final int VERIFy_OR_ENTER_REFERRAL_CODE = 1;
    private static final int TRANSACTION_HISTORY = 2;
    private static final int SUPPORT = 3;
    private static final int OTHER_INFO = 4;
    private static final int LOG_OUT = 5;
    private static final String TAG = HomeCoinActivity.class.getSimpleName();
    private static final String ADCOLONY_APP_ID = "app4561015687984038b5";
    public static final String ZONE_ID = "vz49ec85fcafa3491b9c";

    private static final int TAB_BONUS = 0;
    private static final int TAB_MAKE_COIN = 1;
    private static final int TAB_REDEEM = 2;

    //private TabLayout mTabLayout;
    private DrawerFragment mDrawerFragment;
    private NotificationListFragment mDrawerFragmentRight;
    private DrawerLayout mDrawerLayoutRight, mDrawerLayoutLeft;
    private int mSelectedTabPosition = 1;
    private Event mShareEvent;
    private String[] activityTitles;
    public static String CURRENT_TAG = "";
    private Handler mHandler;
    private Context mContext;
    private WebView mWeb;

    private ImageView mNavBonusCoinImg;
    private TextView mNavBonusCoinBadge;
    private TextView mNavBonusCoinTitle;

    private ImageView mNavMakeCoinImg;
    private TextView mNavMakeCoinBadge;
    private TextView mNavMakeCoinTitle;

    private ImageView mNavRewardImg;
    private TextView mNavRewardBadge;
    private TextView mNavRewardTitle;
    private ImageButton mBtnRate;

    private boolean mDoubleBackToExitPressedOnce;
    private AdColonyAdOptions mAdOptions;
    private AdColonyInterstitialListener mAdLisenter;
    private AdColonyInterstitial mAdColonyInterstitial;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        handleRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mSelectedTabPosition = 1;
        if (savedInstanceState != null) {
            mSelectedTabPosition = savedInstanceState.getInt(ArgumentKeys.SELECTED_TAB_POSITION, 0);
            mShareEvent = savedInstanceState.getParcelable(ArgumentKeys.SHARE_EVENT);
        } else {
            boolean fromLogin = getIntent().getBooleanExtra(ArgumentKeys.FROM_LOGIN, false);
            if (!fromLogin) {
                Singleton.getInstance().setShouldUpdateProfile(true);
            }
            AlarmServiceUtil.getInstance(this).startScheduleLocalPush();
        }
        checkPermission();
        mWeb = (WebView) findViewById(R.id.web);
        mBtnRate = (ImageButton) findViewById(R.id.rate);
        mBtnRate.setOnClickListener(this);

        activityTitles = mContext.getResources().getStringArray(R.array.nav_item_activity_titles);
        mHandler = new Handler();
        setUpNavigationView();

        mDrawerFragment = (DrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerLayoutLeft = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerFragment.setUp(mDrawerLayoutLeft, getToolbar());
        mDrawerFragment.setDrawerListener(this);

        mDrawerFragmentRight = (NotificationListFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer_right);
        mDrawerLayoutRight = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerFragmentRight.setUp(R.id.fragment_navigation_drawer_right, mDrawerLayoutRight, getToolbar());
        mDrawerFragmentRight.setDrawerListener(this);

        PreferenceManager pm = PreferenceManager.getInstance(this);
        pm.registerListener(this);
        OneSignalInfo info = pm.getOneSignalInfo();
        if (!pm.isSavedOneSignalInfo() && info != null) {
            saveOneSignalInfo(info);
        }

        loadHomeFragment();
        // mWeb.getSettings().setJavaScriptEnabled(true);
        String customUserId = pm.getId() +
                "-" +
                FreeXuUtils.getDeviceId(this);
        AdColonyAppOptions appOptions = new AdColonyAppOptions()
                .setUserID(customUserId);

/** Pass options with user id set with configure */
        AdColony.configure(this, appOptions, ADCOLONY_APP_ID, ZONE_ID);
        AdColony.setRewardListener(this);
        mAdOptions = new AdColonyAdOptions()
                .enableConfirmationDialog(true)
                .enableResultsDialog(true);
        mAdLisenter = new AdColonyInterstitialListener() {
            @Override
            public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
                mAdColonyInterstitial = adColonyInterstitial;
                Toast.makeText(HomeCoinActivity.this, R.string.msg_rewarded_video_available, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onExpiring(AdColonyInterstitial ad) {
                super.onExpiring(ad);
                requestNewAd();
            }
        };

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateProfile();
        if (Singleton.getInstance().shouldUpdateReOpenAppList()) {
            EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_REOPEN_LIST));
        }
        if (Singleton.getInstance().shouldRefreshOfferList()) {
            EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_OFFER_LIST));
        }
        if (isAllPermissionsGranted() && mRequirePermissionDialog != null) {
            mRequirePermissionDialog.dismiss();
        }
        recordScreenName("Home");
        if (mAdColonyInterstitial == null || mAdColonyInterstitial.isExpired()) {
            requestNewAd();
        }
        mBtnRate.setVisibility(PreferenceManager.getInstance(this).isRated() ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ArgumentKeys.SELECTED_TAB_POSITION, mSelectedTabPosition);
        outState.putParcelable(ArgumentKeys.SHARE_EVENT, mShareEvent);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mSelectedTabPosition = savedInstanceState.getInt(ArgumentKeys.SELECTED_TAB_POSITION, 1);
    }

    @Override
    protected void onDestroy() {
        PreferenceManager.getInstance(this).unregisterListener(this);
        super.onDestroy();
    }

    @Override
    public int getView() {
        return R.layout.activity_home_coin;
    }

    @Override
    protected void onHomeClicked() {
        mDrawerFragment.toggle();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        switch (e.getEvent()) {
            case MessageEvent.UPDATE_PROFILE:
                updateProfile();
                break;
            case MessageEvent.UPDATE_PROFILE_SUCCESS:
                mDrawerFragment.updateHeaderDisplay();
                break;
            case MessageEvent.SHOW_NOTIFICATION:
                showNotification(this, e);
                break;
            case MessageEvent.UPDATE_DRAWER:
                mDrawerFragment.setNeedRefreshDrawer(true);
                break;
            case MessageEvent.UPDATE_UNCOUNT_NOTIFICATION:
                if (mDrawerLayoutRight.isDrawerOpen(GravityCompat.END)) {
                    mDrawerFragmentRight.onRefresh();
                } else {
                    PreferenceManager.getInstance(mContext).increaseUnreadNotificationCount();
                }
                break;
            case MessageEvent.SHOW_TOOLTIP:
                int myId = e.getMyToolTipId();
                if (myId == GuideUtils.GUIDE_INSTALL_DOWNLOAD || myId == GuideUtils.GUIDE_ENTER_REFERRAL_LAYOUT
                        || myId == GuideUtils.GUIDE_INSTALL_DIALOG || myId == GuideUtils.GUIDE_REDEEM_TOPUP
                        || myId == GuideUtils.GUIDE_REDEEM_CONFIRM_CHANGE || myId == GuideUtils.GUIDE_SHARE_CODE) {
                    ViewGroup rootView = (ViewGroup) findViewById(R.id.rootView);
                    GuideUtils.requestShowToolTip(myId, mContext, rootView);
                }
                break;
        }
    }

    public void requestNewAd() {
        AdColony.requestInterstitial(HomeCoinActivity.ZONE_ID, mAdLisenter, mAdOptions);
    }

    public void showRewardedVideo() {
        if (mAdColonyInterstitial != null && !mAdColonyInterstitial.isExpired()) {
            mAdColonyInterstitial.show();
        } else {
            Toast.makeText(this, R.string.msg_reward_not_avaiable, Toast.LENGTH_SHORT).show();
        }

    }

    private void updateProfile() {
        if (Singleton.getInstance().shouldUpdateProfile()) {
            Singleton.getInstance().setShouldUpdateProfile(false);
            Call<ProfileResponse> call = RestClient.getInstance(this).getUserApi().getProfile();
            call.enqueue(new RestCallback<ProfileResponse>() {
                @Override
                public void failure(RestError error) {
                    if (error.getCode() != 0) {
                        Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void success(ProfileResponse response) {
                    PreferenceManager.getInstance(HomeCoinActivity.this).updateUserInfo(response.getResult());
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE_SUCCESS));
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (mDoubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        mDoubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.msg_press_back_to_exit, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mDoubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        Intent myIntent;
        UserInfo userInfo = PreferenceManager.getInstance(mContext).getUserInfo();
        switch (position) {
            case TOP_CHART:
                myIntent = new Intent(this, TopChartActivity.class);
                this.startActivity(myIntent);
                overridePendingTransition(R.anim.push_up, R.anim.hold);
                break;
            case VERIFy_OR_ENTER_REFERRAL_CODE:
                if (userInfo != null && userInfo.isVerified() && userInfo.isEnteredReferral()) {
                    myIntent = new Intent(this, RedeemHistoryActivity.class);
                    this.startActivity(myIntent);
                    overridePendingTransition(R.anim.push_up, R.anim.hold);
                } else if (userInfo != null && userInfo.isVerified()) {
                    EnterReferralCodeDialog dlg = EnterReferralCodeDialog.newInstance();
                    dlg.show(getSupportFragmentManager(), "EnterReferralCode");
                    break;
                } else {
                    myIntent = new Intent(this, ActivateActivity.class);
                    this.startActivity(myIntent);
                    overridePendingTransition(R.anim.push_up, R.anim.hold);
                }
                break;
            case TRANSACTION_HISTORY:
                if (userInfo != null && userInfo.isVerified() && userInfo.isEnteredReferral()) {
                    myIntent = new Intent(this, CustomerSupportActivity.class);
                    this.startActivity(myIntent);
                    overridePendingTransition(R.anim.push_up, R.anim.hold);
                } else {
                    myIntent = new Intent(this, RedeemHistoryActivity.class);
                    this.startActivity(myIntent);
                    overridePendingTransition(R.anim.push_up, R.anim.hold);
                }
                break;
            case SUPPORT:
                if (userInfo != null && userInfo.isVerified() && userInfo.isEnteredReferral()) {
                    myIntent = new Intent(this, OtherInfoActivity.class);
                    this.startActivity(myIntent);
                    overridePendingTransition(R.anim.push_up, R.anim.hold);
                } else {
                    myIntent = new Intent(this, CustomerSupportActivity.class);
                    this.startActivity(myIntent);
                    overridePendingTransition(R.anim.push_up, R.anim.hold);
                }
                break;
            case OTHER_INFO:
                if (userInfo != null && userInfo.isVerified() && userInfo.isEnteredReferral()) {
                    logout();
                } else {
                    myIntent = new Intent(this, OtherInfoActivity.class);
                    this.startActivity(myIntent);
                    overridePendingTransition(R.anim.push_up, R.anim.hold);
                }
                break;
            case LOG_OUT:
                logout();
                break;
            default:
                break;
        }
    }

    private void logout() {
        PreferenceManager.getInstance(HomeCoinActivity.this).setSavedOneSignalInfo(false);
        LoginManager.getInstance().logOut();
        PreferenceManager.getInstance(HomeCoinActivity.this).logout();
        ActivityNavigator.startLoginActivity(HomeCoinActivity.this);
        AppInfoManager.getInstance(HomeCoinActivity.this).clear();
        finish();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(PreferenceManager.UNREAD_NOTIFICATION_COUNT)) {
            updateNotificationState();
        }
    }

    private void saveOneSignalInfo(OneSignalInfo info) {
        String signalId = info.getOneSignalId();
        OneSignalIdBody oneSignalId = new OneSignalIdBody(signalId);
        Call<BaseResponse> call = RestClient.getInstance(mContext).getUserApi().savePushInfo(oneSignalId);
        call.enqueue(new RestCallback<BaseResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
                Log.d(TAG, "Cannot save push notification info");
            }

            @Override
            public void success(BaseResponse response) {
                PreferenceManager.getInstance(HomeCoinActivity.this).setSavedOneSignalInfo(true);
            }
        });
    }

    @Override
    protected void onNotificationsClick() {
        //  ActivityNavigator.openNotificationActivity(this);
        mDrawerFragmentRight.openDrawer();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.rate){
            showRateDialog();
        } else {
            int newTabBarIndex = getTabBarIndex(v.getId());
            if (mSelectedTabPosition != newTabBarIndex) {
                mSelectedTabPosition = newTabBarIndex;
                setCheckMenuItem(mSelectedTabPosition);
                loadHomeFragment();
            }
        }
    }

    private void showRateDialog() {
        RateConfirmationDialog dlg = RateConfirmationDialog.newInstance();
        dlg.show(getSupportFragmentManager(), RateConfirmationDialog.class.getSimpleName());
    }

    private int getTabBarIndex(int id) {
        switch (id) {
            case R.id.nav_item_bonus_coin:
                return 0;
            case R.id.nav_item_make_coin:
                return 1;
            case R.id.nav_item_reward:
                return 2;
            default:
                return 1;
        }
    }

    @Override
    public void onReward(AdColonyReward adColonyReward) {
        if (adColonyReward.success()) {
            EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
            Singleton.getInstance().setShouldUpdateProfile(true);
        }
    }

    private class CustomWebViewClient extends WebViewClient {
        private String[] mScripts;
        private int mCount = 0;
        private boolean mIsAppRequest;

        public CustomWebViewClient(boolean isAppRequest) {
            mIsAppRequest = isAppRequest;
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public void onPageFinished(final WebView view, String url) {
            super.onPageFinished(view, url);
            try {
                if (!mIsAppRequest) {
                    if (mScripts != null && mCount < mScripts.length) {
                        view.loadUrl("javascript:" + mScripts[mCount++]);
                    }
                    if (mScripts == null || mCount == mScripts.length) {
                        mCount++;
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                inviteFacebookFriends();
                            }
                        }, 10000);
                    }
                } else {
                    if (url.startsWith("fbconnect://success")) {
                        Uri u = Uri.parse(url);
                        Bundle b = Utility.parseUrlQueryString(u.getQuery());
                        String requestId = b.getString(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
                        List<String> recipients = new ArrayList<>();
                        while (b.containsKey(String.format(
                                ShareConstants.WEB_DIALOG_RESULT_PARAM_TO_ARRAY_MEMBER, recipients.size()))) {
                            recipients.add(b.getString(String.format(
                                    ShareConstants.WEB_DIALOG_RESULT_PARAM_TO_ARRAY_MEMBER, recipients.size())));
                        }
                        submitAppRequest(requestId, recipients);
                    } else {
                        view.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                view.loadUrl("javascript:document.getElementsByName('__CONFIRM__')[0].click();");
                            }
                        }, 4000);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void submitAppRequest(String requestId, List<String> recipients) {
        Call<IntegerResponse> call = RestClient.getInstance(this).getFacebookApi().submitFacebookAppRequest(new FacebookAppRequest(requestId, recipients));
        call.enqueue(new RestCallback<IntegerResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(IntegerResponse response) {
            }
        });
    }

    private void inviteFacebookFriends() {
        PreferenceManager preferenceManager = PreferenceManager.getInstance(this);
        if (preferenceManager.shouldInviteFriends()) {
            final int invitableCount = 48;
            new AsyncTask<Void, Void, List<String>>() {

                @Override
                protected List<String> doInBackground(Void... voids) {
                    List<String> ret = new ArrayList<>();
                    List<String> appFriends = new ArrayList<>();
                    Bundle params = new Bundle();
                    String order = System.currentTimeMillis() % 2 == 0 ? "reverse_chronological" : "chronological";
                    params.putInt("limit", 100);
                    params.putString("fields", String.format("id.order(%s)", order));
                    GraphRequest friendRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/friends", params, HttpMethod.GET);
                    GraphResponse friendResponse = friendRequest.executeAndWait();
                    if (friendResponse.getError() == null) {
                        JSONObject result = friendResponse.getJSONObject();
                        try {
                            JSONArray data = result.getJSONArray("data");
                            if (data != null) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject friend = data.getJSONObject(i);
                                    appFriends.add(friend.getString("id"));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    GraphRequest invitableFriendRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/invitable_friends", params, HttpMethod.GET);
                    GraphResponse invitableFriendResponse = invitableFriendRequest.executeAndWait();
                    List<String> invitableFriends = new ArrayList<>();
                    if (invitableFriendResponse.getError() == null) {
                        JSONObject result = invitableFriendResponse.getJSONObject();
                        try {
                            JSONArray data = result.getJSONArray("data");
                            if (data != null) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject friend = data.getJSONObject(i);
                                    invitableFriends.add(friend.getString("id"));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Collections.shuffle(appFriends);
                    Collections.shuffle(invitableFriends);
                    int addableAppFriendCount = Math.min(appFriends.size(), 50 - invitableCount);
                    int addableInvitableFriendCount = Math.min(invitableFriends.size(), invitableCount);
                    if (appFriends.size() < 50 - invitableCount && invitableFriends.size() > invitableCount) {
                        addableAppFriendCount = appFriends.size();
                        addableInvitableFriendCount = Math.min(invitableFriends.size(), 50 - addableAppFriendCount);
                    }
                    if (invitableFriends.size() < invitableCount && appFriends.size() > 50 - invitableCount) {
                        addableInvitableFriendCount = invitableFriends.size();
                        addableAppFriendCount = Math.min(appFriends.size(), 50 - addableInvitableFriendCount);
                    }
                    for (int i = 0; i < addableAppFriendCount; i++) {
                        ret.add(appFriends.get(i));
                    }
                    for (int i = 0; i < addableInvitableFriendCount; i++) {
                        ret.add(invitableFriends.get(i));
                    }
                    return ret;
                }

                @Override
                protected void onPostExecute(List<String> friendIds) {
                    String url = FacebookUtils.buildAppRequestUrl(mContext.getResources().getString(R.string.title_app_request), mContext.getResources().getString(R.string.msg_app_request), friendIds);
                    if (!TextUtils.isEmpty(url)) {
                        mWeb.setWebViewClient(new CustomWebViewClient(true));
                        mWeb.loadUrl(url);
                    }
                }

                @Override
                protected void onCancelled() {
                }
            }.execute();

        }
    }

    private void setToolbarTitle() {
        setTitle(activityTitles[mSelectedTabPosition]);
    }

    private void setUpNavigationView() {

        LinearLayout menuItemBonusCoin = (LinearLayout) findViewById(R.id.nav_item_bonus_coin);
        LinearLayout menuItemMakeCoin = (LinearLayout) findViewById(R.id.nav_item_make_coin);
        LinearLayout menuItemReward = (LinearLayout) findViewById(R.id.nav_item_reward);
        menuItemBonusCoin.setOnClickListener(this);
        menuItemMakeCoin.setOnClickListener(this);
        menuItemReward.setOnClickListener(this);

        mNavBonusCoinImg = (ImageView) findViewById(R.id.nav_item_bonus_coin_icon);
        mNavBonusCoinBadge = (TextView) findViewById(R.id.nav_item_bonus_coin_badge);
        mNavBonusCoinTitle = (TextView) findViewById(R.id.nav_item_bonus_coin_title);

        mNavMakeCoinImg = (ImageView) findViewById(R.id.nav_item_make_coin_icon);
        mNavMakeCoinBadge = (TextView) findViewById(R.id.nav_item_make_coin_badge);
        mNavMakeCoinTitle = (TextView) findViewById(R.id.nav_item_make_coin_title);

        mNavRewardImg = (ImageView) findViewById(R.id.nav_item_reward_icon);
        mNavRewardBadge = (TextView) findViewById(R.id.nav_item_reward_badge);
        mNavRewardTitle = (TextView) findViewById(R.id.nav_item_reward_tittle);

        mNavBonusCoinBadge.setVisibility(View.GONE);
        mNavMakeCoinBadge.setVisibility(View.GONE);
        mNavRewardBadge.setVisibility(View.GONE);

        setCheckMenuItem(1);

    }

    private void loadHomeFragment() {
        setToolbarTitle();

        Fragment fragment = null;
        String tag = null;
        FragmentManager fm = getSupportFragmentManager();
        switch (mSelectedTabPosition) {
            case TAB_BONUS:
                tag = BonusCoinFragment.TAG;
                fragment = fm.findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = BonusCoinFragment.newInstance();
                }
                break;
            case TAB_MAKE_COIN:
                tag = MakeCoinFragment.TAG;
                fragment = fm.findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = MakeCoinFragment.newInstance();
                }
                break;
            default:
                tag = RedeemFragment.TAG;
                fragment = fm.findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = RedeemFragment.newInstance();
                }
                break;
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        ft.replace(R.id.frame, fragment, tag);
        ft.commit();
    }

    private void setCheckMenuItem(int pos) {
        switch (pos) {
            case 0:
                mNavBonusCoinImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_bonus_active));
                mNavBonusCoinTitle.setTextColor(mContext.getResources().getColor(R.color.bottom_navigation_view_item_text_color_active));

                mNavMakeCoinImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_earnmoney));
                mNavMakeCoinTitle.setTextColor(mContext.getResources().getColor(R.color.bottom_navigation_view_item_text_color));
                mNavRewardImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_gift));
                mNavRewardTitle.setTextColor(mContext.getResources().getColor(R.color.bottom_navigation_view_item_text_color));
                break;
            case 1:
                mNavBonusCoinImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_bonus));
                mNavBonusCoinTitle.setTextColor(mContext.getResources().getColor(R.color.bottom_navigation_view_item_text_color));

                mNavMakeCoinImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_earnmoney_active));
                mNavMakeCoinTitle.setTextColor(mContext.getResources().getColor(R.color.bottom_navigation_view_item_text_color_active));

                mNavRewardImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_gift));
                mNavRewardTitle.setTextColor(mContext.getResources().getColor(R.color.bottom_navigation_view_item_text_color));

                break;
            case 2:
                mNavBonusCoinImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_bonus));
                mNavBonusCoinTitle.setTextColor(mContext.getResources().getColor(R.color.bottom_navigation_view_item_text_color));

                mNavMakeCoinImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_earnmoney));
                mNavMakeCoinTitle.setTextColor(mContext.getResources().getColor(R.color.bottom_navigation_view_item_text_color));

                mNavRewardImg.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_gift_active));
                mNavRewardTitle.setTextColor(mContext.getResources().getColor(R.color.bottom_navigation_view_item_text_color_active));
                break;

            default:

        }
    }
}