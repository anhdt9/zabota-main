package vn.lovemoney.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.dialog.SuccessActivationDialog;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.body.PhoneBody;
import vn.lovemoney.app.model.body.PhoneVerifyBody;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by 8470p on 3/11/2017.
 */

public class ActivateActivity extends BaseActivity {
    private Button btnConfirm, btnSendCode;
    private EditText txtPhone, txtCode;
    private Call<BaseResponse> mReponse;
    private Context mContext;
    private CountDownTimer timer;
    private static final int TIME_TOTAL = 30000;
    private static final int TIME_STEP_DOWN = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.activate);
        mContext = this;
        initStart();
        initControl();
    }

    private void initStart() {
        btnSendCode = (Button) findViewById(R.id.btn_enter_code);
        btnConfirm = (Button) findViewById(R.id.confirm);
        txtPhone = (EditText) findViewById(R.id.enter_phone_number);
        txtCode = (EditText) findViewById(R.id.enter_code);
    }

    private void initControl() {
        btnSendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txtPhone.getText())) {
                    Toast.makeText(mContext, R.string.msg_phone_missing, Toast.LENGTH_SHORT).show();
                } else {
                    getCodeFromServer();
                    txtCode.setFocusable(true);
                    btnSendCode.setEnabled(false);
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txtCode.getText())) {
                    Toast.makeText(mContext, R.string.msg_phone_missing, Toast.LENGTH_SHORT).show();
                } else {
                    sendVerifyToServer();
                }
            }
        });
    }

    private void countDownTimer() {
        timer = new CountDownTimer(TIME_TOTAL, TIME_STEP_DOWN) {
            @Override
            public void onTick(long millisUntilFinished) {
                btnSendCode.setText(mContext.getResources().getText(R.string.resend) + " (" + millisUntilFinished / 1000 + "s)");
            }

            @Override
            public void onFinish() {
                btnSendCode.setEnabled(true);
                btnSendCode.setText(mContext.getResources().getString(R.string.get_code));
            }
        };
        timer.start();
    }

    private void getCodeFromServer() {
        String phone = txtPhone.getText().toString();
        PhoneBody body = new PhoneBody(phone);
        mReponse = RestClient.getInstance(this).getUserApi().sendOtp(body);
        showProgressDialog();
        btnSendCode.setEnabled(false);
        countDownTimer();
        mReponse.enqueue(new RestCallback<BaseResponse>() {
            @Override
            public void failure(RestError error) {
                dismissProgressDialog();
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    timer.cancel();
                    btnSendCode.setEnabled(true);
                    btnSendCode.setText(R.string.send_code);
                }
            }

            @Override
            public void success(BaseResponse response) {
                dismissProgressDialog();
                Toast.makeText(mContext, response.getDisplayMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendVerifyToServer() {
        final String phone = txtPhone.getText().toString();
        String code = txtCode.getText().toString();
        PhoneVerifyBody body = new PhoneVerifyBody(phone, code);
        mReponse = RestClient.getInstance(this).getUserApi().verify(body);
        showProgressDialog();
        mReponse.enqueue(new RestCallback<BaseResponse>() {
            @Override
            public void failure(RestError error) {
                dismissProgressDialog();
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(BaseResponse response) {
                dismissProgressDialog();
                SuccessActivationDialog dialog = SuccessActivationDialog.newInstance();
                dialog.setListener(new SuccessActivationDialog.ActivateAccountLister() {
                    @Override
                    public void finishActivity() {
                        finish();
                    }
                });
                dialog.show(getSupportFragmentManager(), "SuccessActivationDialog");
                Toast.makeText(mContext, response.getDisplayMessage(), Toast.LENGTH_SHORT).show();
                Singleton.getInstance().setShouldUpdateProfile(true);
                EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
                EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_DRAWER));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        recordScreenName("Activate Account");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int getView() {
        return R.layout.activity_activate;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if(e.getEvent() == MessageEvent.SHOW_NOTIFICATION){
            showNotification(this, e);
        }
    }
}
