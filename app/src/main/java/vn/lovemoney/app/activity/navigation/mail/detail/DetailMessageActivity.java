package vn.lovemoney.app.activity.navigation.mail.detail;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.AccessToken;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.BaseActivity;
import vn.lovemoney.app.adapter.navigationdrawer.mail.detail.MessageItemAdapter;
import vn.lovemoney.app.model.BasePagingRequest;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.body.SupportBody;
import vn.lovemoney.app.model.support.GetMessage;
import vn.lovemoney.app.model.support.GetMessageResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by DTA on 16/03/2017.
 */

public class DetailMessageActivity extends BaseActivity {

    private static final int PAGE_SIZE = 10;

    private MessageItemAdapter adapter;
    private List<GetMessage> mListMessage;
    private int status;
    private int ticketId;
    private Context mContext;
    private ImageView btn_send_sms;
    private EditText enter_sms;
    private LinearLayout layout_send_sms;
    private int mUnseenCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setTitle(R.string.customer_support);
        ListView lv = (ListView) findViewById(R.id.lv_detail_mail);
        btn_send_sms = (ImageView) findViewById(R.id.btn_send_sms);
        enter_sms = (EditText) findViewById(R.id.enter_sms);
        layout_send_sms = (LinearLayout) findViewById(R.id.layout_send_sms);

        status = getIntent().getIntExtra("ticket_status", -1);
        ticketId = getIntent().getIntExtra("ticket_id", -1);
        mUnseenCount = getIntent().getIntExtra("unseen_count", -1);

        reportSeen();

        btn_send_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = enter_sms.getText().toString();
                if (content.isEmpty()) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.enter_sms), Toast.LENGTH_SHORT).show();
                } else {
                    String userFacebookId = null;
                    if (!PreferenceManager.getInstance(mContext).isLoggedIn()) {
                        userFacebookId = AccessToken.getCurrentAccessToken().getUserId();
                    }
                    SupportBody body = new SupportBody(userFacebookId, content);
                    Call<BaseResponse> call = RestClient.getInstance(mContext).getSupportApi().postMessage(ticketId, body);
                    call.enqueue(new RestCallback<BaseResponse>() {
                        @Override
                        public void failure(RestError error) {
                            if (error.getCode() != 0) {
                                Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void success(BaseResponse response) {
                            getData();
                        }
                    });
                    enter_sms.setText("");
                }
            }
        });
        enter_sms.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    btn_send_sms.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_send_click));
                } else {
                    btn_send_sms.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_send_normal));
                }
            }
        });
        mListMessage = new ArrayList<>();
        getData();
        adapter = new MessageItemAdapter(mContext, mListMessage);
        lv.setAdapter(adapter);
    }

    private void reportSeen() {
        if (mUnseenCount <= 0)
            return;

        Call<BaseResponse> call = RestClient.getInstance(mContext).getSupportApi().markSeenMessage(ticketId);
        call.enqueue(new RestCallback<BaseResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() == 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(BaseResponse response) {
                Log.d("anhdt", "reportSeen : " + response.getDisplayMessage());
            }
        });
    }

    private void getData() {

        BasePagingRequest mBasePagingRequest = new BasePagingRequest(0, PAGE_SIZE);
        if (status == 1) {
            layout_send_sms.setVisibility(View.VISIBLE);
        } else {
            layout_send_sms.setVisibility(View.GONE);
        }
        String userFacebookId = null;
        if (!PreferenceManager.getInstance(mContext).isLoggedIn()) {
            userFacebookId = AccessToken.getCurrentAccessToken().getUserId();
        }
        Call<GetMessageResponse> call = RestClient.getInstance(mContext).getSupportApi().getMessage(ticketId, mBasePagingRequest.toQueryMap(), userFacebookId);
        call.enqueue(new RestCallback<GetMessageResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(GetMessageResponse response) {
                mListMessage.clear();

                GetMessage firstSMS = new GetMessage();
                firstSMS.setAdmin(true);
                firstSMS.setContent(mContext.getResources().getString(R.string.admin_hello));
                firstSMS.setCreatedAt("");
                firstSMS.setId(1);
                firstSMS.setSeen(1);
                mListMessage.add(firstSMS);

                List<GetMessage> listMessage = response.getResult();
                for (int i = listMessage.size() - 1; i >= 0; i--) {
                    mListMessage.add(listMessage.get(i));
                }
                adapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getView() {
        return R.layout.activity_detail_mail;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if(e.getEvent() == MessageEvent.SHOW_NOTIFICATION){
            showNotification(this, e);
        }
    }
}
