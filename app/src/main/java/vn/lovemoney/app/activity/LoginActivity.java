package vn.lovemoney.app.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Response;
import vn.lovemoney.app.R;
import vn.lovemoney.app.dialog.SupportLoginDialog;
import vn.lovemoney.app.model.user.LoginResponse;
import vn.lovemoney.app.model.user.LoginRqBody;
import vn.lovemoney.app.model.user.UserCredentials;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.ActivityNavigator;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.FacebookUtils;
import vn.lovemoney.app.util.LanguageUtils;
import vn.lovemoney.app.util.PreferenceManager;

public class LoginActivity extends BaseActivity {
    public static final String TAG = LoginActivity.class.getSimpleName();
    private CallbackManager mCallbackManager;
    private boolean mLoginFacebookStarted;

    private AppCompatSpinner spSelectLanguage;
    private String languageCodes[];
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mCallbackManager = CallbackManager.Factory.create();
        LinearLayout btnLoginFb = (LinearLayout) findViewById(R.id.btn_login_fb);

        spSelectLanguage = (AppCompatSpinner) findViewById(R.id.sp_choose_language);
        languageCodes = this.getResources().getStringArray(R.array.language_codes);

        setDataSpinner();

        btnLoginFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setReadPermissionFB();
            }
        });
        if (savedInstanceState == null) {
            checkPermission();
        }
        ImageView ivMark = (ImageView) findViewById(R.id.mark);
        ivMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spSelectLanguage.performClick();
            }
        });
    }

    private void setReadPermissionFB() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "user_posts", "email"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLoginFb();
        if (isAllPermissionsGranted() && mRequirePermissionDialog != null) {
            mRequirePermissionDialog.dismiss();
        }
        recordScreenName("Login");
    }

    private void checkLoginFb() {
        if (!mLoginFacebookStarted) {
            mLoginFacebookStarted = true;
            //logged
            if (FacebookUtils.isLoggedIn()) {
                onLoginFacebookSuccess(AccessToken.getCurrentAccessToken().getToken(), AccessToken.getCurrentAccessToken().getUserId());
            } else if (FacebookUtils.isLoggedInExpired()) {
                AccessToken.refreshCurrentAccessTokenAsync();
            } else {
                LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        onLoginFacebookSuccess(loginResult.getAccessToken().getToken(), loginResult.getAccessToken().getUserId());
                    }

                    @Override
                    public void onCancel() {
                        Log.i("button login", "onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(LoginActivity.this, R.string.login_facebook_failed, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, error.getMessage());
                    }
                });
            }
        }
    }

    @Override
    public int getView() {
        return R.layout.activity_login;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        handleRequestPermissionResult(requestCode, permissions, grantResults);
    }

    public void onLoginFacebookSuccess(final String fbToken, final String fbId) {

        LoginRqBody body = new LoginRqBody(fbId, fbToken);

        Call<LoginResponse> mLoginRequest = RestClient.getInstance(this).getUserApi().login(body);
        showProgressDialog();
        mLoginRequest.enqueue(new RestCallback<LoginResponse>() {
            @Override
            public void failure(RestError error) {
                dismissProgressDialog();
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (error.getCode() != RestError.NEED_CHANGE_DEVICE) {
                    if ((PreferenceManager.getInstance(mContext).getSupportTicketProcessing() != -1)) {
                        SupportLoginDialog dialog = SupportLoginDialog.newInstance();
                        dialog.show(getFragmentManager(), null);
                    } else {
                        Intent intentSupport = new Intent(mContext, SupportLoginActivity.class);
                        startActivity(intentSupport);
                        overridePendingTransition(R.anim.push_up, R.anim.hold);
                    }
                }
            }

            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful() && response.body().getError() == RestError.NEED_CHANGE_DEVICE) {
                    String phone = response.body().getResult().getPhone();
                    Intent intent = new Intent(mContext, ChangeDeviceActivity.class);
                    intent.putExtra(ArgumentKeys.PHONE, phone);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_up, R.anim.hold);
                    finish();
                } else
                    super.onResponse(call, response);
            }

            @Override
            public void success(LoginResponse response) {
                dismissProgressDialog();
                loginSuccessFaceBook(response);
                PreferenceManager.getInstance(mContext).setSupportTicketProcessing(-1);
            }
        });
    }

    private void loginSuccessFaceBook(LoginResponse response) {
        UserCredentials userCredentials = response.getResult();
        PreferenceManager.getInstance(this).saveUserLogin(userCredentials);
        ActivityNavigator.startHomeActivity(this, true);
        finish();
    }

    private void setDataSpinner() {

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_item_language, R.id.item_language, this.getResources().getStringArray(R.array.languages));
        final String currentLanguage = LanguageUtils.getCurrentLanguage(this);
        spSelectLanguage.setAdapter(adapter);
        spSelectLanguage.setSelection(currentLanguage.equals("vi") ? 1 : 0);
        spSelectLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String newLanguage = languageCodes[position];
                if (!newLanguage.equals(currentLanguage)) {
                    LanguageUtils.setCurrentLanguage(position, mContext);
                    recreate();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
