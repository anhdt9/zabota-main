package vn.lovemoney.app.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;

import retrofit2.Call;
import vn.lovemoney.app.BuildConfig;
import vn.lovemoney.app.R;
import vn.lovemoney.app.model.other.RemoteConfig;
import vn.lovemoney.app.model.other.RemoteConfigResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.ActivityNavigator;
import vn.lovemoney.app.util.FreeXuUtils;
import vn.lovemoney.app.util.PreferenceManager;

/**
 * Created by tuannguyen on 12/10/15.
 */
public class SplashActivity extends BaseActivity {

	private static final String TAG = SplashActivity.class.getSimpleName();

	private static final long TIME_TO_PAUSE = 500;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//TODO anhdt
		if (FreeXuUtils.isRunningOnEmulator(this)) {
			showIncompatibleDeviceDialog();
			return;
		}
		loadRemoteConfig();
		RemoteConfig remoteConfig = PreferenceManager.getInstance(this).getRemoteConfig();
		if (remoteConfig != null) {
			int latestVersion = remoteConfig.getLatestVersion();
			if (latestVersion > BuildConfig.VERSION_CODE) {
				showGoToMarketDialog();
				return;
			}
		}
		if (savedInstanceState == null && checkPlayStoreApp()) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					startNewActivity();
				}
			}, TIME_TO_PAUSE);
		}
	}

	private void showIncompatibleDeviceDialog() {
		AlertDialog dlg = new AlertDialog.Builder(this)
				.setTitle(R.string.title_warning)
				.setMessage(R.string.msg_incompatible_device)
				.setPositiveButton(R.string.quit, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				})
				.setCancelable(false)
				.create();
		dlg.setCanceledOnTouchOutside(false);
		dlg.show();
	}

	@Override
	public int getView() {
		return R.layout.activity_splash;
	}

	private void startNewActivity() {
		if (PreferenceManager.getInstance(SplashActivity.this).isLoggedIn()) {
			ActivityNavigator.startHomeActivity(SplashActivity.this, false);
		} else {
			ActivityNavigator.startLoginActivity(SplashActivity.this);
		}
		finish();
	}

	public boolean checkPlayStoreApp() {
		if (!FreeXuUtils.isGooglePlayStoreInstalled(this)) {
			showGooglePlayInstallDialog();
			return false;
		}
		return true;
	}

	private void showGooglePlayInstallDialog() {
		AlertDialog dlg = new AlertDialog.Builder(this)
				.setTitle(R.string.title_warning)
				.setMessage(R.string.msg_please_install_play_store)
				.setPositiveButton(R.string.install, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String url = "http://www.apkmirror.com/apk/google-inc/google-play-store/";
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
						startActivity(intent);
					}
				})
				.setCancelable(false)
				.create();
		dlg.setCanceledOnTouchOutside(false);
		dlg.show();
	}

	private void showInitializeGooglePlayDialog() {
		AlertDialog dlg = new AlertDialog.Builder(this)
				.setTitle(R.string.title_warning)
				.setMessage(R.string.msg_please_initialize_play_store)
				.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								try {
									ActivityNavigator.openApp(SplashActivity.this, FreeXuUtils.GOOGLE_PLAY_PACKAGE_NAME);
								} catch (NullPointerException e) {
									finish();
								}
							}
						}
				)
				.setCancelable(false)
				.create();

		dlg.setCanceledOnTouchOutside(false);
		dlg.show();
	}

	private void showGoToMarketDialog() {
		String updateVersionMessage = getApplicationContext().getResources().getString(R.string.msg_please_upgrade_new_version);
		AlertDialog dlg = new AlertDialog.Builder(this)
				.setTitle(R.string.title_warning)
				.setMessage(updateVersionMessage)
				.setPositiveButton(R.string.upgrade, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						ActivityNavigator.gotoAppMarket(SplashActivity.this);
					}
				})
				.setCancelable(false)
				.create();
		dlg.setCanceledOnTouchOutside(false);
		dlg.show();
	}

	public void loadRemoteConfig(){
		Call<RemoteConfigResponse> call = RestClient.getInstance(this).getOtherApi().getRemoteConfig();
		call.enqueue(new RestCallback<RemoteConfigResponse>() {
			@Override
			public void failure(RestError error) {

			}

			@Override
			public void success(RemoteConfigResponse response) {
				RemoteConfig remoteConfig = response.getResult();
				if(remoteConfig != null){
					PreferenceManager.getInstance(SplashActivity.this).setRemoteConfig(remoteConfig);
				}
			}
		});
	}
}
