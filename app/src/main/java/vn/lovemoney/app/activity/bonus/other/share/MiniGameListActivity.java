package vn.lovemoney.app.activity.bonus.other.share;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.BaseActivity;
import vn.lovemoney.app.activity.LuckyCircleGameActivity;
import vn.lovemoney.app.adapter.bonus.other.MiniGameAdapter;
import vn.lovemoney.app.model.bonus.coin.other.MiniGameItem;
import vn.lovemoney.app.model.game.GameItem;
import vn.lovemoney.app.model.game.GameMiniResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by DTA on 15/03/2017.
 */

public class MiniGameListActivity extends BaseActivity {
    Context mContext;
    ListView lv;
    List<MiniGameItem> list;
    MiniGameAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setTitle(R.string.play_minigame_title);
        lv = (ListView) findViewById(R.id.list);

        list = new ArrayList<>();
        getData();
        adapter = new MiniGameAdapter(this, list);
        lv.setAdapter(adapter);

        ImageButton ibBack = (ImageButton) findViewById(android.R.id.home);
        ibBack.setVisibility(shouldEnableBack() ? View.VISIBLE : View.GONE);
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHomeClicked();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    showProgressDialog();
                    Call<GameMiniResponse> call = RestClient.getInstance(mContext).getGameApi().getGameMiniInfo();
                    call.enqueue(new RestCallback<GameMiniResponse>() {
                        @Override
                        public void failure(RestError error) {
                            if (error.getCode() != 0) {
                                Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            dismissProgressDialog();
                        }

                        @Override
                        public void success(GameMiniResponse response) {
                            dismissProgressDialog();
                            GameItem gameItem = response.getResult();
                            if(gameItem != null) {
                                Intent intent = new Intent(mContext, LuckyCircleGameActivity.class);
                                intent.putExtra(ArgumentKeys.RESULT_INDEX, gameItem.getResultIndex());
                                intent.putExtra(ArgumentKeys.MINI_GAME_DATA, gameItem.getLabels());
                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_left, R.anim.hold);
                            }
                        }
                    });

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        recordScreenName("Minigame List");
    }

    @Override
    public int getView() {
        return R.layout.activity_minigame;
    }

    private void getData() {
        MiniGameItem it0 = new MiniGameItem(R.drawable.icon_lucky_number, mContext.getResources().getString(R.string.game_mini_1));
        list.add(it0);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if(e.getEvent() == MessageEvent.SHOW_NOTIFICATION){
            showNotification(this, e);
        }
    }
}
