package vn.lovemoney.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ViewSwitcher;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.TopChartPagerAdapter;
import vn.lovemoney.app.model.ChartInfo;
import vn.lovemoney.app.util.FreeXuUtils;
import vn.lovemoney.app.util.event.MessageEvent;


public class TopChartActivity extends BaseActivity implements View.OnClickListener {

    private List<ChartInfo> mItems;
    private TopChartPagerAdapter mAdapter;
    private ViewSwitcher mViewSwitcher; // TODO anhdt
    private Context mContext;
    private TabLayout tabLayout;
    private String[] mChartName;
    private String[] mDisplayRankTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.hunter_top);
        mContext = this;
        mItems = new ArrayList<>();
        mAdapter = new TopChartPagerAdapter(getSupportFragmentManager(), mItems);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewSwitcher = (ViewSwitcher) findViewById(R.id.view_switcher);
        ImageButton ibTryAgain = (ImageButton) findViewById(R.id.try_again);
        ibTryAgain.setOnClickListener(this);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(mAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        loadChartInfo();

        String [] titles = new String[mItems.size()];
        for(int i = 0; i < mItems.size(); i++){
            titles[i] = mItems.get(i).getDisplayName();
        }
        boolean showCounter[] = new boolean[titles.length];
        FreeXuUtils.customTab(mContext, mAdapter, tabLayout, titles, showCounter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        recordScreenName("Top Charts");
    }

    @Override
    public int getView() {
        return R.layout.activity_top_chart;
    }

    private void loadChartInfo() {
        mChartName = mContext.getResources().getStringArray(R.array.rank_query_sortby);
        mDisplayRankTab = mContext.getResources().getStringArray(R.array.rank_display_by);
        mItems.clear();
        for (int i = 0; i < mChartName.length; i++) {
            ChartInfo chartInfo = new ChartInfo();
            chartInfo.setChartName(mChartName[i]);
            chartInfo.setDisplayName(mDisplayRankTab[i]);
            mItems.add(chartInfo);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        loadChartInfo();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if(e.getEvent() == MessageEvent.SHOW_NOTIFICATION){
            showNotification(this, e);
        }
    }
}