package vn.lovemoney.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.OtherInfoPagerAdapter;
import vn.lovemoney.app.util.FreeXuUtils;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by 8470p on 3/15/2017.
 */

public class OtherInfoActivity extends BaseActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private OtherInfoPagerAdapter mAdapter;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.other_information);
        mContext = this;

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        mAdapter = new OtherInfoPagerAdapter(getSupportFragmentManager(), this);

        //Set an Apater for the View Pager
        viewPager.setAdapter(mAdapter);

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
                boolean showCounter[] = new boolean[mAdapter.getTabTitles().length];
                FreeXuUtils.customTab(mContext, mAdapter, tabLayout, mAdapter.getTabTitles(), showCounter);
            }
        });
    }

    @Override
    public int getView() {
        return R.layout.activity_other_info;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if(e.getEvent() == MessageEvent.SHOW_NOTIFICATION){
            showNotification(this, e);
        }
    }
}
