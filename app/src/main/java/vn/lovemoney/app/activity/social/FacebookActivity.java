package vn.lovemoney.app.activity.social;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.BaseActivity;
import vn.lovemoney.app.adapter.bonus.social.FacebookAdapter;
import vn.lovemoney.app.dialog.LikeCommentReviewDialog;
import vn.lovemoney.app.dialog.LikeFacebookDialog;
import vn.lovemoney.app.model.BasePagingRequest;
import vn.lovemoney.app.model.facebook.FacebookItem;
import vn.lovemoney.app.model.facebook.GetFacebookOfferResponse;
import vn.lovemoney.app.model.facebook.PutFacebookBody;
import vn.lovemoney.app.model.facebook.PutFacebookOffersResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;
import vn.lovemoney.app.view.widget.LoadMoreListView;

/**
 * Created by DTA on 13/03/2017.
 */

public class FacebookActivity extends BaseActivity implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, LoadMoreListView.OnLoadMoreListener {


    private static final String TAG = FacebookActivity.class.getSimpleName();

    private Context mContext;
    private LoadMoreListView lv;
    private FacebookAdapter adapter;
    private List<FacebookItem> itemList;
    private static final int PAGE_SIZE = 10;
    private BasePagingRequest mRequest;
    private ShareDialog mShareDialog;
    private CallbackManager mCallbackManager;

    private int mAction;
    private FacebookItem mSelectedItem;

    private LikeFacebookDialog likePageDialog;
    private SwipeRefreshLayout mSwipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        String[] titles = mContext.getResources().getStringArray(R.array.fb_titles);
        mAction = getIntent().getIntExtra("facebook_action", -1);
        setTitle(titles[mAction - 1]);

        lv = (LoadMoreListView) findViewById(R.id.list);
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);

        itemList = new ArrayList<>();
        mRequest = new BasePagingRequest(0, PAGE_SIZE);

        getFacebookOffers(mAction);
        adapter = new FacebookAdapter(this, itemList);
        mCallbackManager = CallbackManager.Factory.create();
        initializeShareFacebookDialog();

        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        lv.setOnLoadMoreListener(this);
        mSwipeLayout.setOnRefreshListener(this);

        ImageButton ibBack = (ImageButton) findViewById(android.R.id.home);
        ibBack.setVisibility(shouldEnableBack() ? View.VISIBLE : View.GONE);
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHomeClicked();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ArgumentKeys.FACEBOOK_ITEM, mSelectedItem);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mSelectedItem = savedInstanceState.getParcelable(ArgumentKeys.FACEBOOK_ITEM);
    }

    @Override
    protected void onResume() {
        super.onResume();
        recordScreenName("Social");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && mSelectedItem.getType() == FacebookItem.TYPE_PAGE) {
            Bundle bundle = data.getBundleExtra("com.facebook.platform.protocol.RESULT_ARGS");
            if (bundle != null) {
                boolean isLiked = bundle.getBoolean("object_is_liked", false);
                String completionGesture = bundle.getString("completionGesture");
                if (isLiked && completionGesture.equals("like")) {
                    if (likePageDialog != null) {
                        reportLike();
                        if (likePageDialog.getDialog() != null && likePageDialog.getDialog().isShowing()) {
                            likePageDialog.dismiss();
                        }
                    }
                }
            }
        }
    }

    @Override
    public int getView() {
        return R.layout.activity_like_fanpage;
    }

    private void getFacebookOffers(int action) {
        Call<GetFacebookOfferResponse> call = RestClient.getInstance(mContext).getFacebookApi().getFacebookOffers(action, mRequest.toQueryMap());
        call.enqueue(new RestCallback<GetFacebookOfferResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
                lv.onLoadFailed();
                mSwipeLayout.setRefreshing(false);
            }

            @Override
            public void success(GetFacebookOfferResponse response) {
                itemList.clear();
                for (int i = 0; i < response.getResult().size(); i++) {
                    FacebookItem item = response.getResult().get(i);
                    if (filterOk(item.getType())) {
                        itemList.add(item);
                    }
                }
                mRequest.nextPage();
                lv.onFinishAll();
                adapter.notifyDataSetChanged();
                mSwipeLayout.setRefreshing(false);
            }
        });
    }

    private boolean filterOk(int type) {
        if (type == FacebookItem.TYPE_POST) {
            if (mAction != FacebookItem.ACTION_REVIEW)
                return true;
        } else if (type == FacebookItem.TYPE_PAGE) {
            if (mAction != FacebookItem.ACTION_COMMENT)
                return true;
        } else if (type == FacebookItem.TYPE_GROUP) {

        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mSelectedItem = itemList.get(position);
        int type = mSelectedItem.getType();
        switch (mAction) {
            case FacebookItem.ACTION_LIKE:
                if (type == FacebookItem.TYPE_PAGE) {
                    showLikePageDialog(mSelectedItem);
                } else {
                    likeCommentReview(mSelectedItem);
                }
                break;
            case FacebookItem.ACTION_SHARE:
                showShareFacebookDialog(mSelectedItem);
                break;
            case FacebookItem.ACTION_COMMENT:
            case FacebookItem.ACTION_REVIEW:
                likeCommentReview(mSelectedItem);
                break;
        }
    }

    private void likeCommentReview(FacebookItem item) {
        LikeCommentReviewDialog dialog = LikeCommentReviewDialog.newInstance(item, mAction);
        dialog.show(getSupportFragmentManager(), "CommentReview");
    }

    private void openBrowser(String facebookID) {
        String facebookUrl = "https://facebook.com/" + facebookID;
        Uri uri = Uri.parse(facebookUrl);
        startActivity(new Intent(Intent.ACTION_VIEW, uri));
    }

    private void showLikePageDialog(FacebookItem item) {
        likePageDialog = LikeFacebookDialog.newInstance(item);
        likePageDialog.show(getSupportFragmentManager(), null);
    }

    private void showShareFacebookDialog(FacebookItem item) {
        if (AccessToken.getCurrentAccessToken() != null) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(item.getName())
                    .setContentUrl(Uri.parse(item.getUrl()))
                    .build();
            mShareDialog.show(linkContent, ShareDialog.Mode.NATIVE);
        }
    }

    private void initializeShareFacebookDialog() {
        mShareDialog = new ShareDialog(this);
        mShareDialog.registerCallback(mCallbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                //always gets called
                Log.d("Zabota", "Share success");
                reportShare();
            }

            @Override
            public void onCancel() {
                //do something
                Log.d(TAG, "onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                // TODO Auto-generated method stub
                Log.d(TAG, "onError");
                Toast.makeText(FacebookActivity.this, R.string.msg_please_use_native_fb_app, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void reportShare() {
        PutFacebookBody body = new PutFacebookBody(mSelectedItem.getCampaignId(), mSelectedItem.getFacebookObjectId());
        Call<PutFacebookOffersResponse> call = RestClient.getInstance(mContext).getFacebookApi().putFacebookOffers(mAction, body);
        call.enqueue(new RestCallback<PutFacebookOffersResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(PutFacebookOffersResponse response) {
                Singleton.getInstance().setShouldUpdateProfile(true);
                EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
                Toast.makeText(mContext, mContext.getResources().getString(R.string.bonus_shared, mSelectedItem.getBonus()), Toast.LENGTH_SHORT).show();
                onRefresh();
            }
        });
    }

    @Override
    public void onRefresh() {
        refreshData();
    }

    private void refreshData() {
        itemList.clear();
        adapter.notifyDataSetChanged();
        mRequest.reset();
        getFacebookOffers(mAction);
    }

    @Override
    public void onLoadMore() {
        getFacebookOffers(mAction);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if (e.getEvent() == MessageEvent.SHOW_NOTIFICATION) {
            showNotification(this, e);
        }
    }

    private void reportLike() {
        PutFacebookBody body = new PutFacebookBody(mSelectedItem.getCampaignId(), mSelectedItem.getFacebookObjectId());
        Call<PutFacebookOffersResponse> call = RestClient.getInstance(mContext).getFacebookApi().putFacebookOffers(FacebookItem.ACTION_LIKE, body);
        call.enqueue(new RestCallback<PutFacebookOffersResponse>() {
            @Override
            public void failure(RestError error) {
                Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void success(PutFacebookOffersResponse response) {
                Singleton.getInstance().setShouldUpdateProfile(true);
                EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
                Toast.makeText(mContext, response.getDisplayMessage(), Toast.LENGTH_SHORT).show();
                onRefresh();
            }
        });
    }
}