
package vn.lovemoney.app.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.LocaleList;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import vn.lovemoney.app.R;
import vn.lovemoney.app.dialog.MaterialProgressDialog;
import vn.lovemoney.app.util.LanguageUtils;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.ZabotaContextWrapper;
import vn.lovemoney.app.util.event.MessageEvent;

public abstract class BaseActivity extends AppCompatActivity {

	private static final String TAG = BaseActivity.class.getSimpleName();

	private static final int RC_CODE_ASK_PERMISSION = 123;
	private static final int PAGE_SIZE = 10;
	private static MaterialProgressDialog mProgressDialog;

	private Context mContext;
	private Toolbar mToolbar;
	private TextView mTxtTitle;
	private FrameLayout mLayoutNumberNotification;
	private TextView mTvNotifications;
	protected AlertDialog mRequirePermissionDialog;
	protected ImageButton ibBack;
	private static final long TIME_AUTO_HIDE_NOTIFICATION = 6000L;
	private FirebaseAnalytics mFirebaseAnalytics;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(getView());
		getWindow().setWindowAnimations(0);
		mContext = this;
		EventBus.getDefault().register(this);
		//Toolbar
		mToolbar = (Toolbar) findViewById(R.id.toolbar);
		if (mToolbar != null) {
			mTxtTitle = (TextView) findViewById(android.R.id.title);
			ImageView mIvNotifications = (ImageView) findViewById(R.id.notifications);
			mTvNotifications = (TextView) findViewById(R.id.tv_notifications);
			FrameLayout mLayoutNotification = (FrameLayout) findViewById(R.id.layout_notification);
			mLayoutNumberNotification = (FrameLayout) findViewById(R.id.layout_number_notifications);
			setSupportActionBar(mToolbar);
			ibBack = (ImageButton) findViewById(android.R.id.home);
			ibBack.setVisibility(shouldEnableBack() ? View.VISIBLE : View.GONE);
			ibBack.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onHomeClicked();
				}
			});
			if (mIvNotifications != null) {
				mIvNotifications.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						onNotificationsClick();
					}
				});
			}

			if (mLayoutNotification != null) {
				mLayoutNotification.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						onNotificationsClick();
					}
				});
			}
		}
	}

	protected void recordScreenName(String screenName) {
		mFirebaseAnalytics.setCurrentScreen(this, screenName, null);
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		Context localizedContext = ZabotaContextWrapper.wrap(newBase, LanguageUtils.getLocale(newBase));
		Context fontIntegratedContext = CalligraphyContextWrapper.wrap(localizedContext);
		super.attachBaseContext(fontIntegratedContext);
	}

	protected abstract int getView();

	protected void onHomeClicked() {
		onBackPressed();
	}

	protected Toolbar getToolbar() {
		return mToolbar;
	}

	protected boolean shouldEnableBack() {
		return true;
	}

	@Override
	public void setTitle(CharSequence title) {
		if (mTxtTitle != null) {
			mTxtTitle.setText(title);
		} else {
			super.setTitle(title);
		}
	}

	@Override
	public void setTitle(int titleId) {
		if (mTxtTitle != null) {
			mTxtTitle.setText(titleId);
		} else {
			super.setTitle(titleId);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	public void showProgressDialog() {
		showProgressDialog(false);
	}

	private void showProgressDialog(boolean hasProgress) {
		if (mProgressDialog == null) {
			mProgressDialog = new MaterialProgressDialog(this, android.R.style.Theme_Translucent);
		}
		if (!mProgressDialog.isShowing() && !isFinishing()) {
			mProgressDialog.show();
		}
	}

	public void dismissProgressDialog() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onEvent(MessageEvent e) {

	}

	protected void showNotification(Activity activity, MessageEvent data) {
		if (!isRunningInForeGround(activity)) {
			return;
		}

		String iconUrl = null;
		String uriData = null;
		try {
			iconUrl = data.getData().getString("iconUrl");
			uriData = data.getData().getString("uri");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		try {
			ViewGroup rootView = (ViewGroup) activity.findViewById(R.id.rootView);

			// genarate notification view
			final View notificationView = vi.inflate(R.layout.notification_view, null);

			ImageView icon = (ImageView) notificationView.findViewById(R.id.notification_icon);
			TextView detail = (TextView) notificationView.findViewById(R.id.notification_detail);

			if (iconUrl != null) {
				Picasso.with(mContext)
						.load(iconUrl)
						.fit()
						.centerCrop()
						.placeholder(R.drawable.ic_image_empty)
						.error(R.drawable.ic_image_empty)
						.into(icon);
			} else {
				icon.setVisibility(View.INVISIBLE);
			}

			if (uriData != null) {
				final String finalUriData = uriData;
				detail.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Uri uri = Uri.parse(finalUriData);
						Intent intent = new Intent(Intent.ACTION_VIEW, uri);

						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
							intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
									Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
									Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
						} else {
							intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
									Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
									Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
						}
						mContext.startActivity(intent);
						notificationView.setVisibility(View.GONE);
					}
				});
			} else {
				detail.setVisibility(View.INVISIBLE);
			}

			TextView tv = (TextView) notificationView.findViewById(R.id.notification_message);
			tv.setText(data.getMessage());

			Button close = (Button) notificationView.findViewById(R.id.notification_close);
			close.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_UNCOUNT_NOTIFICATION));
					notificationView.animate()
							.translationY(-500)
							.setListener(new AnimatorListenerAdapter() {
								@Override
								public void onAnimationEnd(Animator animation) {
									super.onAnimationEnd(animation);
									notificationView.setVisibility(View.GONE);
								}
							});
				}
			});

			// add params

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP);

			rootView.addView(notificationView, params);
			rootView.bringChildToFront(notificationView);

			startShakingAnimation(notificationView);

			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					if (notificationView.getVisibility() == View.VISIBLE) {
						notificationView.setVisibility(View.GONE);
						EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_UNCOUNT_NOTIFICATION));
					}
				}
			}, TIME_AUTO_HIDE_NOTIFICATION);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean isRunningInForeGround(Activity activity) {
		ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningTaskInfo> runningTaskInfo = am.getRunningTasks(10);
		try {
			if (runningTaskInfo.size() > 0) {
				ActivityManager.RunningTaskInfo taskInfo = runningTaskInfo.get(0);
				String className = taskInfo.topActivity.getClassName();
				if (activity.getClass().getName().equals(className)) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private void startShakingAnimation(View view) {
		Animation shake = AnimationUtils.loadAnimation(mContext, R.anim.shaking);
		view.startAnimation(shake);
	}

	@Override
	protected void onResume() {
		super.onResume();
		String actualLanguage = getResources().getConfiguration().locale.getLanguage();
		String selectedLanguage = LanguageUtils.getCurrentLanguage(this);
		if(!actualLanguage.equals(selectedLanguage)){
			Locale newLocale = LanguageUtils.getLocale(this);
			Resources res = getResources();
			Configuration configuration = res.getConfiguration();

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
				configuration.setLocale(newLocale);

				LocaleList localeList = new LocaleList(newLocale);
				LocaleList.setDefault(localeList);
				configuration.setLocales(localeList);

				createConfigurationContext(configuration);

			} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
				DisplayMetrics displayMetrics = res.getDisplayMetrics();
				configuration.setLocale(newLocale);
				res.updateConfiguration(configuration, displayMetrics);
			} else {
				configuration.locale = newLocale;
				res.updateConfiguration(configuration, res.getDisplayMetrics());
			}
		}

	}

	@Override
	protected void onDestroy() {
		dismissProgressDialog();
		mProgressDialog = null;
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}

	protected void onNotificationsClick() {
	}

	protected void updateIconBack() {

	}

	protected void updateNotificationState() {
		int unreadNotificationCount = PreferenceManager.getInstance(this).getUnreadNotificationCount();
		if (unreadNotificationCount > 0) {
			mTvNotifications.setText(String.valueOf(unreadNotificationCount));
			mLayoutNumberNotification.setVisibility(View.VISIBLE);
		} else {
			mLayoutNumberNotification.setVisibility(View.GONE);
		}
	}

	protected void checkPermission() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			int getAccountsPermission = checkSelfPermission(android.Manifest.permission.GET_ACCOUNTS);
			int readPhoneStatePermission = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
			List<String> missingPermissions = new ArrayList<>();
			if (getAccountsPermission != PackageManager.PERMISSION_GRANTED) {
				missingPermissions.add(Manifest.permission.GET_ACCOUNTS);
			}
			if (readPhoneStatePermission != PackageManager.PERMISSION_GRANTED) {
				missingPermissions.add(Manifest.permission.READ_PHONE_STATE);
			}
			if (!missingPermissions.isEmpty()) {
				String[] requestPermissions = new String[missingPermissions.size()];
				missingPermissions.toArray(requestPermissions);
				requestPermissions(requestPermissions, RC_CODE_ASK_PERMISSION);
			}
		}
	}

	protected void handleRequestPermissionResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		if (requestCode == RC_CODE_ASK_PERMISSION && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
			// for each permission check if the user grantet/denied them
			// you may want to group the rationale in a single dialog,
			// this is just an example
			for (int i = 0, len = permissions.length; i < len; i++) {
				if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
					AlertDialog.Builder builder = new AlertDialog.Builder(this)
							.setTitle(R.string.title_permission_denied)
							.setMessage(R.string.msg_please_enable_get_accounts_and_read_phone_state_permission)
							.setPositiveButton(R.string.settings, null)
							.setCancelable(false);
					mRequirePermissionDialog = builder.create();
					mRequirePermissionDialog.setCanceledOnTouchOutside(false);
					mRequirePermissionDialog.show();
					mRequirePermissionDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
							Uri uri = Uri.fromParts("package", getPackageName(), null);
							intent.setData(uri);
							startActivityForResult(intent, RC_CODE_ASK_PERMISSION);
						}
					});
					break;
				}
			}
		} else {
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}

	protected boolean isAllPermissionsGranted() {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
			int getAccountsPermission = checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
			int readPhoneStatePermission = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
			return getAccountsPermission == PackageManager.PERMISSION_GRANTED && readPhoneStatePermission == PackageManager.PERMISSION_GRANTED;
		}
		return true;
	}
}