package vn.lovemoney.app.activity;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.bonus.EventAnswerBody;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by DTA on 14/03/2017.
 */

public class EventActivity extends BaseActivity {

    private int mEventId;

    private EditText event_answer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEventId = getIntent().getIntExtra("event_id", -1);
        String mEventGuide = getIntent().getStringExtra("event_guide");
        String mEventTitle = getIntent().getStringExtra("event_title");
        String mEventEndAt = getIntent().getStringExtra("event_end_at");
        setTitle(mEventTitle);

        TextView mTxtGuide = (TextView) findViewById(R.id.event_guide);
        TextView mTxtEndAt = (TextView) findViewById(R.id.event_end_at);
        event_answer = (EditText) findViewById(R.id.event_answer);
        Button mBtnSendAnswer = (Button) findViewById(R.id.event_btn_send_answer);
        mTxtGuide.setText(Html.fromHtml(mEventGuide));
        mTxtEndAt.setText(mEventEndAt);

        mBtnSendAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(event_answer.getText())) {
                    Toast.makeText(getApplicationContext(), R.string.msg_please_enter_answer, Toast.LENGTH_SHORT).show();
                } else {
                    sendAnswer();
                }
            }
        });

    }

    private void sendAnswer() {
        EventAnswerBody body = new EventAnswerBody(mEventId, event_answer.getText().toString());
        Call<BaseResponse> call = RestClient.getInstance(getApplicationContext()).getUserApi().postEvents(body);
        call.enqueue(new RestCallback<BaseResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(BaseResponse response) {
                Toast.makeText(getApplicationContext(), response.getDisplayMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        recordScreenName("Event");
    }

    @Override
    public int getView() {
        return R.layout.activity_event;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if(e.getEvent() == MessageEvent.SHOW_NOTIFICATION){
            showNotification(this, e);
        }
    }

}
