package vn.lovemoney.app.activity;

import android.os.Bundle;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import vn.lovemoney.app.R;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by DTA on 24/05/2017.
 */

public class DetailFAQActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int stt = getIntent().getIntExtra("faq_stt", -1);
        String ques = getIntent().getStringExtra("faq_question");
        String ans = getIntent().getStringExtra("faq_answer");
        setTitle(String.format(getApplicationContext().getString(R.string.question_text), stt));

        TextView question = (TextView) findViewById(R.id.question);
        TextView answer = (TextView) findViewById(R.id.answer);

        question.setText(ques);
        answer.setText(ans);

    }

    @Override
    public int getView() {
        return R.layout.detail_faq;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if(e.getEvent() == MessageEvent.SHOW_NOTIFICATION){
            showNotification(this, e);
        }
    }

}
