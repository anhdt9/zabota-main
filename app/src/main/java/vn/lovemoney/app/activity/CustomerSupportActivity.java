package vn.lovemoney.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.CustomerSupportPagerAdapter;
import vn.lovemoney.app.util.FreeXuUtils;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by 8470p on 3/14/2017.
 */

public class CustomerSupportActivity extends BaseActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CustomerSupportPagerAdapter mAdapter;
    private Context mContext;
    private int mIndexTabShowCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.customer_support);
        mContext = this;
        mIndexTabShowCounter = 0;

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        mAdapter = new CustomerSupportPagerAdapter(getSupportFragmentManager(), this);

        //Set an Apater for the View Pager
        viewPager.setAdapter(mAdapter);

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
                boolean showCounter[] = new boolean[mAdapter.tabTitles.length];
                FreeXuUtils.customTab(mContext, mAdapter, tabLayout, mAdapter.tabTitles, showCounter);
            }
        });
    }

    @Override
    public int getView() {
        return R.layout.activity_customer_support;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void updateUnseenCounter(int cnt) {
        TabLayout.Tab tab = tabLayout.getTabAt(mIndexTabShowCounter);
        TextView counter = (TextView) tab.getCustomView().findViewById(R.id.tab_counter);
        if (cnt > 0) {
            counter.setVisibility(View.VISIBLE);
            counter.setText(String.valueOf(cnt));
        } else {
            counter.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if(e.getEvent() == MessageEvent.SHOW_NOTIFICATION){
            showNotification(this, e);
        }
    }
}
