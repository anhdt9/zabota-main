package vn.lovemoney.app.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import vn.lovemoney.app.R;
import vn.lovemoney.app.dialog.SupportLoginDialog;
import vn.lovemoney.app.model.user.UserCredentials;
import vn.lovemoney.app.util.GuideUtils;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by DTA on 06/04/2017.
 */

public class SupportLoginActivity extends BaseActivity {

    private ViewGroup mRootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageView img_fb_profile = (ImageView) findViewById(R.id.img_fb_profile);
        TextView tv_fb_name = (TextView) findViewById(R.id.tv_fb_name);
        LinearLayout btn_support = (LinearLayout) findViewById(R.id.btn_support);
        mRootView = (ViewGroup) findViewById(R.id.rootView);

        UserCredentials userInfo = PreferenceManager.getInstance(this).getUserCredential();
        if (userInfo != null) {
            Picasso.with(this)
                    .load(userInfo.getAvatarUrl())
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_image_empty)
                    .error(R.drawable.ic_image_empty)
                    .into(img_fb_profile);
            tv_fb_name.setText(userInfo.getName());
        }

        btn_support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SupportLoginDialog dialog = SupportLoginDialog.newInstance();
                dialog.show(getFragmentManager(), null);
            }
        });

        GuideUtils.requestShowToolTip(GuideUtils.GUIDE_SUPPORT_LOGIN_CLICK, this, mRootView);
    }

    @Override
    public int getView() {
        return R.layout.activity_support_login;
    }

    @Override
    public void onBackPressed() {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        switch (e.getEvent()) {
            case MessageEvent.SHOW_TOOLTIP:
                if (e.getMyToolTipId() == GuideUtils.GUIDE_SUPPORT_LOGIN_SMS) {
                    GuideUtils.requestShowToolTip(e.getMyToolTipId(), this, mRootView);
                }
                break;
        }
    }
}
