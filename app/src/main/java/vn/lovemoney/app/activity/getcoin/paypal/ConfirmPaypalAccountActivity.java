package vn.lovemoney.app.activity.getcoin.paypal;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.BaseActivity;
import vn.lovemoney.app.dialog.GetPapalDialog;
import vn.lovemoney.app.model.exchance.PaypalItem;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by 8470p on 5/17/2017.
 */

public class ConfirmPaypalAccountActivity extends BaseActivity {

    private EditText mEditEmail;
    private Context mContext;
    private PaypalItem mPaypalItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.confirm_account);
        init();
    }

    private void init() {
        mContext = this;
        mEditEmail = (EditText) findViewById(R.id.enter_email);
        Button mBtnConfirm = (Button) findViewById(R.id.confirm);

        mPaypalItem = getIntent().getParcelableExtra("paypalItem");
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEditEmail.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.let_enter_email), Toast.LENGTH_LONG).show();
                } else {
                    GetPapalDialog dialog = GetPapalDialog.newInstance(mPaypalItem, email);
                    dialog.show(getFragmentManager(), null);
                }

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public int getView() {
        return R.layout.activity_confirm_email;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if(e.getEvent() == MessageEvent.SHOW_NOTIFICATION){
            showNotification(this, e);
        }
    }
}
