package vn.lovemoney.app.activity;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;

public class LuckyCircleGameActivity extends BaseActivity implements View.OnClickListener {

    private ImageView ivSpin;
    private TextView txtStart, txtSpinCount;
    private Context mContext;

    private Bitmap bmpSpinnerBackground, bmpSpinnerCircle;
    private String[] labels;
    private int resultIndex;
    private int mSpinCount = 1;
    private boolean mSpinning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        resultIndex = intent.getIntExtra(ArgumentKeys.RESULT_INDEX, 0);
        labels = intent.getStringArrayExtra(ArgumentKeys.MINI_GAME_DATA);
        mContext = this;
        setTitle(R.string.game_lucky_number);
        initStart();
    }

    @Override
    public boolean shouldEnableBack() {
        return !mSpinning;
    }

    private void initStart() {
        txtStart = (TextView) findViewById(R.id.start);
        txtSpinCount = (TextView) findViewById(R.id.spin_count);
        ivSpin = (ImageView) findViewById(R.id.spin);
        txtStart.setOnClickListener(this);
        initializeSpin();
    }

    public void initializeSpin() {
        updateSpinCount();
        bmpSpinnerBackground = BitmapFactory.decodeResource(
                getResources(),
                R.drawable.minigame_bg_spin
        );

        int width = bmpSpinnerBackground.getWidth();

        bmpSpinnerCircle = Bitmap.createBitmap(
                width,
                width,
                Bitmap.Config.RGB_565
        );

        drawSpin();
        bmpSpinnerCircle = getCircleBitmap(bmpSpinnerCircle);
        ivSpin.setImageBitmap(bmpSpinnerCircle);
    }


    public void drawSpin() {

        Canvas canvas = new Canvas(bmpSpinnerCircle);

        canvas.drawColor(Color.WHITE);

        canvas.drawBitmap(bmpSpinnerBackground, 0, 0, null);
        Paint paint = new Paint();
        // draw some text using FILL style
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        //turn antialiasing on
        paint.setAntiAlias(true);
        //paint.setTextSize(120);
        paint.setTextSize(getResources().getDimensionPixelSize(R.dimen.circle_font_size));

        int width = bmpSpinnerBackground.getWidth();
        // new <code></code>
        Rect rect = new Rect(0, 0, width, width);

        int centerX = rect.right / 2;
        int centerY = rect.bottom / 2;

        int tempAngle = 15;
        int deltaY = getResources().getDimensionPixelSize(R.dimen.delta_y);
        for (int i = 0; i < labels.length; i++) {
            paint.setColor(Color.WHITE); // set this to the text color.
            canvas.rotate(tempAngle, centerX, centerX);
            Rect bounds = new Rect();
            paint.getTextBounds(labels[i], 0, labels[i].length(), bounds);
            canvas.drawText(labels[i], centerX * 7 / 4 - bounds.width(), centerY + deltaY, paint);
            tempAngle = 30;
        }
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    private void updateSpinCount() {
        txtSpinCount.setText(String.format(mContext.getResources().getString(R.string.so_luot_quay), mSpinCount));
    }

    private void verifyGameShow() {
        Call<BaseResponse> call = RestClient.getInstance(mContext).getGameApi().verifyGameShow();
        call.enqueue(new RestCallback<BaseResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
                mSpinning = false;
            }

            @Override
            public void success(BaseResponse response) {
                Toast.makeText(mContext, response.getDisplayMessage(), Toast.LENGTH_SHORT).show();
                Singleton.getInstance().setShouldUpdateProfile(true);
                EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
                mSpinning = false;
                updateIconBack();
            }
        });
    }

    @Override
    public int getView() {
        return R.layout.activity_lucky_circle_spin;
    }

    @Override
    protected void updateIconBack() {
        super.updateIconBack();

        ibBack.setVisibility(shouldEnableBack() ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        recordScreenName("Lucky Circle Games");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if(e.getEvent() == MessageEvent.SHOW_NOTIFICATION){
            showNotification(this, e);
        }
    }

    @Override
    public void onClick(View v) {
        if (mSpinCount > 0 && !mSpinning) {
            mSpinning = true;
            mSpinCount--;
            updateSpinCount();
            updateIconBack();
            txtStart.setVisibility(View.INVISIBLE);

            float rotateAngle = 75 - resultIndex * 30 + 2880;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                Runnable endAction = new Runnable() {
                    @Override
                    public void run() {
                        verifyGameShow();
                    }
                };
                ivSpin.animate().rotationBy(rotateAngle).withEndAction(endAction).setDuration(10000).setInterpolator(new DecelerateInterpolator()).start();
            } else {
                ivSpin.animate().rotationBy(rotateAngle).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        final Handler handler = new Handler();
                        verifyGameShow();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        mSpinning = false;
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).setDuration(10000).setInterpolator(new DecelerateInterpolator()).start();
            }

        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.notify_gameshow), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (mSpinning)
            return;
        super.onBackPressed();
    }
}
