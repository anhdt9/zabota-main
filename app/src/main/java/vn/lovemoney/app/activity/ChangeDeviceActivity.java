package vn.lovemoney.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.body.FbIdBody;
import vn.lovemoney.app.model.body.PhoneVerifyBody;
import vn.lovemoney.app.model.user.ChangeDeviceResponse;
import vn.lovemoney.app.model.user.UserCredentials;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.ActivityNavigator;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.PreferenceManager;

/**
 * Created by DTA on 06/04/2017.
 */

public class ChangeDeviceActivity extends BaseActivity {
    private Button btn_send_verify;
    private EditText enter_verify_code;
    private Context mContext;
    private static final long TIME_TOTAL = 30000;
    private static final long TIME_STEP_DOWN = 1000;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String phone = getIntent().getStringExtra(ArgumentKeys.PHONE);
        mContext = this;
        setTitle(R.string.change_device_title);
        TextView tv_current_phone_number = (TextView) findViewById(R.id.tv_current_phone_number);
        enter_verify_code = (EditText) findViewById(R.id.enter_verify_code);
        Button btn_confirm = (Button) findViewById(R.id.confirm);
        btn_send_verify = (Button) findViewById(R.id.btn_send_verify);
        tv_current_phone_number.setText(phone);

        btn_send_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_send_verify.setEnabled(false);
                getVerifyCode();
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enter_verify_code.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, R.string.msg_token_missing, Toast.LENGTH_SHORT).show();
                } else {
                    confirm();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        recordScreenName("Change Device");
    }

    private void confirm() {
        String fbId = AccessToken.getCurrentAccessToken().getUserId();
        String fbToken = AccessToken.getCurrentAccessToken().getToken();
        PhoneVerifyBody body = new PhoneVerifyBody(fbId, fbToken, enter_verify_code.getText().toString());

        showProgressDialog();
        Call<ChangeDeviceResponse> request = RestClient.getInstance(mContext).getUserApi().verifyChangeDevice(body);
        request.enqueue(new RestCallback<ChangeDeviceResponse>() {
            @Override
            public void failure(RestError error) {
                dismissProgressDialog();
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(ChangeDeviceResponse response) {
                dismissProgressDialog();
                UserCredentials userCredentials = response.getResult();
                PreferenceManager.getInstance(ChangeDeviceActivity.this).saveUserLogin(userCredentials);
                ActivityNavigator.startHomeActivity(ChangeDeviceActivity.this, false);
                finish();
            }
        });
    }

    private void getVerifyCode() {
        String fbid = AccessToken.getCurrentAccessToken().getUserId();
        FbIdBody body = new FbIdBody(fbid);
        Call<BaseResponse> mReponse = RestClient.getInstance(mContext).getUserApi().sendOtpChangeDevice(body);
        countDownTimer();
        mReponse.enqueue(new RestCallback<BaseResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
                btn_send_verify.setEnabled(true);
                countDownTimer.cancel();
                btn_send_verify.setText(mContext.getResources().getText(R.string.send_code));
            }

            @Override
            public void success(BaseResponse response) {
                Toast.makeText(mContext, response.getDisplayMessage(), Toast.LENGTH_SHORT).show();
                countDownTimer.cancel();
                btn_send_verify.setText(mContext.getResources().getText(R.string.send_code));
            }
        });
    }

    private void countDownTimer() {
        countDownTimer = new CountDownTimer(TIME_TOTAL, TIME_STEP_DOWN) {
            public void onTick(long millisUntilFinished) {
                btn_send_verify.setText(mContext.getResources().getText(R.string.resend) + "(" + millisUntilFinished / 1000 + "s)");
            }

            public void onFinish() {
                btn_send_verify.setEnabled(true);
                btn_send_verify.setText(mContext.getResources().getText(R.string.send_code));
            }
        };
        countDownTimer.start();
    }

    @Override
    public int getView() {
        return R.layout.activity_confirm_change_device;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
