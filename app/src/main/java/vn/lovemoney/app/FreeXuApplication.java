package vn.lovemoney.app;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.onesignal.OneSignal;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import vn.lovemoney.app.notification.FreeXuNotificationOpenedHandler;
import vn.lovemoney.app.notification.FreeXuNotificationReceivedHandler;
import vn.lovemoney.app.notification.UserIdsAvailableHandler;
import vn.lovemoney.app.util.LanguageUtils;
import vn.lovemoney.app.util.PreferenceManager;

/**
 * Created by tuannguyen on 12/18/15.
 */
public class FreeXuApplication extends Application {

	private static FreeXuApplication sInstance;

	public static FreeXuApplication getInstance(){
		return sInstance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
		sInstance = this;
		CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
						.setDefaultFontPath("fonts/Roboto-Regular.ttf")
						.setFontAttrId(R.attr.fontPath)
						.build()
		);
		OneSignal.startInit(this)
				.setNotificationOpenedHandler(new FreeXuNotificationOpenedHandler())
				.setNotificationReceivedHandler(new FreeXuNotificationReceivedHandler())
				.inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
				.init();
		OneSignal.idsAvailable(new UserIdsAvailableHandler(this));
		AppEventsLogger.activateApp(this);
	}

}
