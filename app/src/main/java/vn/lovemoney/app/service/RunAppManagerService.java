package vn.lovemoney.app.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import vn.lovemoney.app.R;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.body.RequestAppBody;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.AlarmServiceUtil;
import vn.lovemoney.app.util.AppOpenManager;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.FreeXuUtils;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by vuong_000 on 1/5/2016.
 */
public class RunAppManagerService extends IntentService {


	/**
	 * Creates an IntentService.  Invoked by your subclass's constructor.
	 *
	 */
	public RunAppManagerService() {
		super("RunAppManagerService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String packageName = intent.getStringExtra(ArgumentKeys.PACKAGE_NAME);
		int campaignId = intent.getIntExtra(ArgumentKeys.CAMPAIGN_ID, 0);
		int count = intent.getIntExtra(ArgumentKeys.COUNT, 0);
		Context context = getApplicationContext();
		if(FreeXuUtils.isAppRunning(this, packageName) || AppOpenManager.getInstance(this).getOpenedCount(packageName) > 1) {
            RequestAppBody appBody = new RequestAppBody(campaignId);
			Call<BaseResponse> call = RestClient.getInstance(getApplicationContext()).getOfferApi().reOpenApp(appBody);
			try {
				Response<BaseResponse> response = call.execute();
				if(response.isSuccessful()) {
					BaseResponse result = response.body();
					Toast.makeText(context, result.getMessage(), Toast.LENGTH_LONG).show();
					if(result.getError() == RestError.SUCCESS) {
						AppOpenManager.getInstance(this).resetOpening(packageName);
						Singleton.getInstance().setShouldUpdateReOpenAppList(true);
						Singleton.getInstance().setShouldUpdateProfile(true);
						EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
					}
					AlarmServiceUtil.getInstance(this).cancelAlarm();
					return;
				} else {
					Toast.makeText(context, R.string.msg_system_error, Toast.LENGTH_LONG).show();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (count <= 5) {
			AlarmServiceUtil.getInstance(this).startCheckingReopenApp(packageName, campaignId, count+1);
		} else {
			AlarmServiceUtil.getInstance(this).cancelAlarm();
		}
	}

}
