package vn.lovemoney.app.service;

import android.app.IntentService;
import android.content.Intent;

import vn.lovemoney.app.util.AlarmServiceUtil;
import vn.lovemoney.app.util.AppOpenManager;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.FreeXuUtils;

/**
 * Created by tuannguyen on 1/26/16.
 */
public class AuditRunningService extends IntentService {

	/**
	 * Creates an IntentService.  Invoked by your subclass's constructor.
	 *
	 *
	 */
	public AuditRunningService() {
		super("AuditRunningService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent.getExtras() != null) {
			AppOpenManager appOpenManager = AppOpenManager.getInstance(AuditRunningService.this);
			AlarmServiceUtil alarmUtil = AlarmServiceUtil.getInstance(this);
			String packageName = intent.getStringExtra(ArgumentKeys.PACKAGE_NAME);
			int campaignId = intent.getIntExtra(ArgumentKeys.CAMPAIGN_ID, 0);
			alarmUtil.cancelAlarm();
			if (FreeXuUtils.isAppRunning(AuditRunningService.this, packageName)) {
				appOpenManager.markAppOpened(packageName);
			} else {
				int count = intent.getIntExtra(ArgumentKeys.COUNT, 0);
				if (count <= 10) {
					alarmUtil.startCheckingAppOpen(packageName, campaignId, count + 1);
				}
			}
		}
	}

}
