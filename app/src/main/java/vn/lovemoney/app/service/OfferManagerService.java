package vn.lovemoney.app.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Response;
import vn.lovemoney.app.R;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.android.app.NewAppOffer;
import vn.lovemoney.app.model.body.RequestAppBody;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.receiver.FreeXuReceiver;
import vn.lovemoney.app.util.AlarmServiceUtil;
import vn.lovemoney.app.util.AppInfoManager;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.FreeXuUtils;
import vn.lovemoney.app.util.Singleton;

/**
 * Created by tuannguyen on 12/17/15.
 */
public class OfferManagerService extends IntentService {
	private static IAppInstallListener mListener;

	/**
	 * Creates an IntentService.  Invoked by your subclass's constructor.
	 */
	public OfferManagerService() {
		super("OfferManagerService");
	}

	@Override
	protected void onHandleIntent(final Intent intent) {
		final String packageName = intent.getStringExtra(ArgumentKeys.PACKAGE_NAME);
		final Context context = getApplicationContext();
		NewAppOffer appInfo = AppInfoManager.getInstance(context).getAppOffer(packageName);
		try {
			if (appInfo != null && ((/*appInfo.getDistributionType() == AppOffer.TYPE_APK_FILE && */FreeXuUtils.isAppInstalled(context, packageName)) || FreeXuUtils.isAppInstallFromGooglePlay(context, packageName))) {
//				RequestBody body = RequestBody.create(MediaType.parse("text/plain"), appInfo.getGuid());
				mListener.appInstallSuccess();
				RequestAppBody body = new RequestAppBody(appInfo.getCampaignId());
				Call<BaseResponse> call = RestClient.getInstance(context).getOfferApi().installApp(body);
				Response<BaseResponse> response = call.execute();
				if(!response.isSuccessful()){
					response = call.execute();
				}
				if(response.isSuccessful()){
					BaseResponse result = response.body();
					if(result.getError() == RestError.SUCCESS){
						Singleton.getInstance().setShouldRefreshOfferList(true);
					}
					Toast.makeText(context, result.getDisplayMessage(), Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(context, R.string.msg_system_error, Toast.LENGTH_LONG).show();
				}
				AlarmServiceUtil.getInstance(this).startCheckingAppOpen(packageName, appInfo.getCampaignId(), 0);
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		FreeXuReceiver.completeWakefulIntent(intent);
	}

	public static void setListener(IAppInstallListener listener) {
		mListener = listener;
	}

	public interface IAppInstallListener {
		void appInstallSuccess();
	}
}
