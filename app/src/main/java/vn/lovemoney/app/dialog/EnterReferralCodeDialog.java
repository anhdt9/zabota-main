package vn.lovemoney.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.model.ReferralResponse;
import vn.lovemoney.app.model.body.ReferralCodeBody;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by tuannguyen on 12/15/15.
 */
public class EnterReferralCodeDialog extends DialogFragment implements View.OnClickListener {
    private EditText mTxtCode;
    private boolean mClicked;
    private Context mContext;
    private View v;
    private Button mBtnConfirm;
    private Button mBtnClose;
    private TextView mTitle;

    public static EnterReferralCodeDialog newInstance() {
        return new EnterReferralCodeDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_enter_introduction_code, null);

        mTxtCode = (EditText) v.findViewById(R.id.enter_verify_code);
        mBtnConfirm = (Button) v.findViewById(R.id.confirm);
        mBtnClose = (Button) v.findViewById(R.id.btn_close);
        mTitle = (TextView) v.findViewById(R.id.tv_referral_dialog_title);
        mBtnConfirm.setOnClickListener(this);
        mBtnClose.setOnClickListener(this);

        builder.setView(v);
        AlertDialog dlg = builder.create();
        dlg.setCanceledOnTouchOutside(false);

//        if (!PreferenceManager.getInstance(mContext).loadBooleanKey(mContext, PreferenceManager.GUIDE_ENTER_REFERRAL_LAYOUT, false)) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showTooltip(0);
            }
        }, 500);
//        }

        return dlg;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                if (mClicked) {
                    return;
                }
                mClicked = true;
                String code = mTxtCode.getText().toString();
                ReferralCodeBody body = new ReferralCodeBody(code);
                final Activity activity = getActivity();
                Call<ReferralResponse> call = RestClient.getInstance(activity).getUserApi().referral(body);
                final Context context = activity.getApplicationContext();
                call.enqueue(new RestCallback<ReferralResponse>() {
                    @Override
                    public void failure(RestError error) {
                        mClicked = false;
                        if (error.getCode() != 0) {
                            Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void success(ReferralResponse response) {
                        mClicked = false;
                        PreferenceManager.getInstance(activity).updateBalance(response.getResult());
                        Singleton.getInstance().setShouldUpdateProfile(true);
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_DRAWER));
                        Toast.makeText(context, response.getDisplayMessage(), Toast.LENGTH_SHORT).show();
                        dismiss();
                    }
                });

                break;
            case R.id.btn_close:
                dismiss();
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public void showTooltip(final int index) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(getResources(index), null);
        final PopupWindow pw = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        pw.setOutsideTouchable(true);
        pw.setFocusable(true);
        pw.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        int[] offset = getOffset(index);
        pw.showAsDropDown(getAnchor(index), offset[0], offset[1]);

        final View next = view.findViewById(R.id.tv_next);

        pw.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isInsideView(event.getRawX(), event.getRawY(), next) && next != null) {
                    showTooltip(index + 1);
                } else {
                    PreferenceManager.getInstance(mContext).saveState(mContext, PreferenceManager.GUIDE_ENTER_REFERRAL_LAYOUT, true);
                }
                pw.dismiss();
                return true;
            }
        });

    }

    private int getResources(int index) {
        if (index == 0) {
            return R.layout.guide_enter_referral_layout;
        } else if (index == 1) {
            return R.layout.guide_enter_referral_confirm;
        } else if (index == 2) {
            return R.layout.guide_enter_referral_close;
        }
        return 0;
    }

    private View getAnchor(int index) {
        if (index == 0) {
            return mTitle;
        } else if (index == 1) {
            return mBtnConfirm;
        } else if (index == 2) {
            return mBtnClose;
        }
        return null;
    }

    private int[] getOffset(int index) {
        if (index == 0) {
            return new int[]{mContext.getResources().getDimensionPixelOffset(R.dimen.title_referral_dialog_x_offset),
                    -mContext.getResources().getDimensionPixelOffset(R.dimen.title_referral_dialog_y_offset)};
        } else {
            return new int[]{0, 0};
        }
    }

    private boolean isInsideView(float x, float y, View view) {
        if (view == null)
            return false;
        int location[] = new int[2];
        view.getLocationOnScreen(location);
        int viewX = location[0];
        int viewY = location[1];
        return ((x > viewX && x < (viewX + view.getWidth())) && (y > viewY && y < (viewY + view.getHeight())));
    }
}