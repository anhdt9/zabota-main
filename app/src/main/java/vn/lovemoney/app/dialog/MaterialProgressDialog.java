package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import vn.lovemoney.app.R;

/**
 * Created by tuannguyen on 9/29/15.
 */
public class MaterialProgressDialog extends Dialog {

    public MaterialProgressDialog(Context context) {
        super(context);
    }

    public MaterialProgressDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected MaterialProgressDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progressbar);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

}
