package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.user.ReOpenApp;
import vn.lovemoney.app.util.ActivityNavigator;
import vn.lovemoney.app.util.AlarmServiceUtil;
import vn.lovemoney.app.util.AppOpenManager;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.FreeXuUtils;

/**
 * Created by 8470p on 5/9/2017.
 */

public class ReopenAppDialog extends DialogFragment implements View.OnClickListener {

    private ReOpenApp mReOpenApp;
    private Context mContext;

    public static ReopenAppDialog newInstance(ReOpenApp reOpenApp){
        Bundle args = new Bundle();
        args.putParcelable(ArgumentKeys.REOPEN_APP, reOpenApp);
        ReopenAppDialog dlg = new ReopenAppDialog();
        dlg.setArguments(args);
        return dlg;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mReOpenApp = getArguments().getParcelable(ArgumentKeys.REOPEN_APP);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View mCustomView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_reopen_app, null);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        LinearLayout linearLayout = (LinearLayout) mCustomView.findViewById(R.id.open_history_container);
        int[] bonusCoins = mReOpenApp.getOpenAppBonuses();
        for (int i = 0; i < bonusCoins.length; i++) {
            LinearLayout dayView = (LinearLayout) inflater.inflate(R.layout.listitem_app_open_daily, linearLayout, false);
            ImageView ivState = (ImageView) dayView.findViewById(R.id.opened);
            TextView txtDay = (TextView) dayView.findViewById(R.id.day);
            ivState.setSelected(i < mReOpenApp.getOpenedCount());
            txtDay.setText(mContext.getResources().getString(R.string.daily_bonus, i + 1, bonusCoins[i]));
            linearLayout.addView(dayView);
        }

        Button btnClose = (Button) mCustomView.findViewById(R.id.btn_close);
        Button btnOpen = (Button) mCustomView.findViewById(R.id.btn_open_sms);

        btnClose.setOnClickListener(this);
        btnOpen.setOnClickListener(this);

        builder.setView(mCustomView);

        return builder.create();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_close:
                dismiss();
                break;
            case R.id.btn_open_sms:
                if (mReOpenApp.getRemainHour() > 0) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.msg_waiting_before_open_app, mReOpenApp.getRemainHour()), Toast.LENGTH_SHORT).show();
                    dismiss();
                    return;
                }
                String packageName = mReOpenApp.getPackageBundle();
                if (FreeXuUtils.isAppInstalled(mContext, packageName)) {
                    AlarmServiceUtil.getInstance(mContext).startCheckingReopenApp(packageName, mReOpenApp.getCampaignId(), 0);
                    ActivityNavigator.openApp(mContext, packageName);
                    AppOpenManager.getInstance(mContext).increaseOpening(packageName);
                } else {
                    AlertDialog dlg = new AlertDialog.Builder(mContext)
                            .setTitle(R.string.alert)
                            .setMessage(R.string.msg_reinstall_app)
                            .setPositiveButton(R.string.reinstall, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityNavigator.openInstallationLink(mContext, mReOpenApp.getInstallationLink());
                                }
                            })
                            .create();
                    dlg.show();
                }
                dismiss();
                break;
        }
    }
}
