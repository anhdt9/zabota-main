package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.user.UserInfo;
import vn.lovemoney.app.util.PreferenceManager;

/**
 * Created by 8470p on 4/9/2017.
 */

public class SuccessActivationDialog extends DialogFragment {

    private ActivateAccountLister mListener;

    public static SuccessActivationDialog newInstance() {
        return new SuccessActivationDialog();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_activate_success, null);

        UserInfo userInfo = PreferenceManager.getInstance(getActivity()).getUserInfo();
        TextView tv_account_name = (TextView) v.findViewById(R.id.tv_account_name);
        Button btn_bonus = (Button) v.findViewById(R.id.btn_bonus);
        btn_bonus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tv_account_name.setText(userInfo.getName());

        builder.setView(v);
        AlertDialog dlg = builder.create();
        dlg.setCanceledOnTouchOutside(true);

        return dlg;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        mListener.finishActivity();

    }

    public void setListener(ActivateAccountLister listener) {
        mListener = listener;
    }

    public interface ActivateAccountLister {
        void finishActivity();
    }
}
