package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import vn.lovemoney.app.R;
import vn.lovemoney.app.util.ActivityNavigator;
import vn.lovemoney.app.util.PreferenceManager;

/**
 * Created by tuandigital on 6/15/17.
 */

public class RateConfirmationDialog extends DialogFragment implements View.OnClickListener {

    public static RateConfirmationDialog newInstance() {

        Bundle args = new Bundle();

        RateConfirmationDialog fragment = new RateConfirmationDialog();
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View customView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_three_button, null);
        Button btnRate = (Button) customView.findViewById(R.id.rate);
        Button btnSkip = (Button) customView.findViewById(R.id.skip);
        Button btnLater = (Button) customView.findViewById(R.id.later);
        btnRate.setOnClickListener(this);
        btnSkip.setOnClickListener(this);
        btnLater.setOnClickListener(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(customView);
        return builder.create();
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.rate:
                ActivityNavigator.gotoAppMarket(getActivity());
                PreferenceManager.getInstance(getActivity()).saveRated();
                dismiss();
                break;
            case R.id.skip:
                PreferenceManager.getInstance(getActivity()).saveRated();
                dismiss();
                break;
            case R.id.later:
                dismiss();
                break;
        }
    }
}
