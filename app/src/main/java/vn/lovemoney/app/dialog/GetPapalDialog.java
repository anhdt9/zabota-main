package vn.lovemoney.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.body.PaypalBody;
import vn.lovemoney.app.model.exchance.PaypalItem;
import vn.lovemoney.app.model.user.UserInfo;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by tuannguyen on 12/15/15.
 */
public class GetPapalDialog extends DialogFragment implements View.OnClickListener {

    private PaypalItem mPaypalItem;
    private String mEmail;

    public static GetPapalDialog newInstance(PaypalItem paypalItem, String email) {
        Bundle args = new Bundle();
        args.putParcelable(ArgumentKeys.PAYPAL_ITEM, paypalItem);
        args.putString("email", email);
        GetPapalDialog dlg = new GetPapalDialog();
        dlg.setArguments(args);
        return dlg;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPaypalItem = getArguments().getParcelable(ArgumentKeys.PAYPAL_ITEM);
        mEmail = getArguments().getString("email");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_paypal_dialog, null);

        TextView tvPaypal = (TextView) v.findViewById(R.id.txt_msg_paypal_exchange);
        TextView tvEmail = (TextView) v.findViewById(R.id.tv_email);

        String currency = mPaypalItem.getCurrency();
        String credit = mPaypalItem.getCredit() + "" + currency;

        tvPaypal.setText(getResources().getString(R.string.paypal_description, mPaypalItem.getExchangePrice(), credit));
        tvEmail.setText(mEmail + " ?");
        Button btnClose = (Button) v.findViewById(R.id.btn_close);
        Button btnDone = (Button) v.findViewById(R.id.btn_done);
        btnClose.setOnClickListener(this);
        btnDone.setOnClickListener(this);

        builder.setView(v);
        AlertDialog dlg = builder.create();
        dlg.setCanceledOnTouchOutside(true);
        return dlg;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_close:
                dismiss();
                break;
            case R.id.btn_done:
                final Activity activity = getActivity();
                UserInfo userInfo = PreferenceManager.getInstance(activity).getUserInfo();
                int balance = userInfo.getBalance();
                if (balance < 1000) {
                    PaypalAlertDialog dialog = PaypalAlertDialog.newInstance();
                    dialog.show(getFragmentManager(), null);
                    dismiss();
                } else {
                    int id = mPaypalItem.getId();
                    PaypalBody body = new PaypalBody(mEmail);
                    Call<BaseResponse> call = RestClient.getInstance(activity).getExchangeApi().getPaypalExchange(id, body);
                    call.enqueue(new RestCallback<BaseResponse>() {
                        @Override
                        public void failure(RestError error) {
                            if(error.getCode() != 0){
                                Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void success(BaseResponse response) {
                            Toast.makeText(activity, response.getDisplayMessage(), Toast.LENGTH_LONG).show();
                            Singleton.getInstance().setShouldUpdateProfile(true);
                            EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
                        }
                    });
                    dismiss();
                }
                break;
        }
    }

}
