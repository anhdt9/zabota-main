package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.widget.LikeView;
import com.squareup.picasso.Picasso;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.facebook.FacebookItem;
import vn.lovemoney.app.util.ArgumentKeys;

public class LikeFacebookDialog extends DialogFragment implements View.OnClickListener {

    private FacebookItem item;
    private Context mContext;

    public static LikeFacebookDialog newInstance(FacebookItem facebookItem) {
        Bundle args = new Bundle();
        args.putParcelable(ArgumentKeys.FACEBOOK_ITEM, facebookItem);
        LikeFacebookDialog fragment = new LikeFacebookDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        item = args.getParcelable(ArgumentKeys.FACEBOOK_ITEM);
        getActivity().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.full_screen_dialog);
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_like_page, null);
        LikeView likeView = (LikeView) view.findViewById(R.id.like);
        ImageView ivIcon = (ImageView) view.findViewById(R.id.icon);
        TextView txtTitle = (TextView) view.findViewById(R.id.title);
        TextView txtBonus = (TextView) view.findViewById(R.id.bonus);
        TextView tvGuide = (TextView) view.findViewById(R.id.guide);
        View buttonContainer = view.findViewById(R.id.button_container);
        int type = item.getType();
        Picasso.with(mContext)
                .load(item.getIconUrl())
                .centerCrop()
                .fit()
                .into(ivIcon);
        txtTitle.setText(item.getName());
        txtBonus.setText(String.valueOf(item.getBonus()));
        if (type == FacebookItem.TYPE_POST) {
            tvGuide.setText((TextUtils.isEmpty(item.getGuide())) ? mContext.getString(R.string.like_guide) : Html.fromHtml(item.getGuide()));
            likeView.setVisibility(View.GONE);
            Button btnClose = (Button) view.findViewById(R.id.close);
            btnClose.setOnClickListener(this);
            Button mBtnConfirm = (Button) view.findViewById(R.id.confirm);
            mBtnConfirm.setOnClickListener(this);
        } else {
            tvGuide.setVisibility(View.GONE);
            buttonContainer.setVisibility(View.GONE);
            likeView.setLikeViewStyle(LikeView.Style.STANDARD);
            likeView.setAuxiliaryViewPosition(LikeView.AuxiliaryViewPosition.INLINE);
            likeView.setObjectIdAndType(item.getFacebookObjectId(), LikeView.ObjectType.DEFAULT);
        }

        builder.setView(view);
        return builder.create();
    }

    private void requestLikePost() {

    }

    @Override
    public void onAttach(Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.close) {
            dismiss();
        } else{
            requestLikePost();
            dismiss();
        }
    }
}
