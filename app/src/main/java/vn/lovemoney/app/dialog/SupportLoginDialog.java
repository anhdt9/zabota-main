package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.AccessToken;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.navigationdrawer.mail.detail.MessageItemAdapter;
import vn.lovemoney.app.model.BasePagingRequest;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.body.SupportBody;
import vn.lovemoney.app.model.support.GetMessage;
import vn.lovemoney.app.model.support.GetMessageResponse;
import vn.lovemoney.app.model.support.GetTicket;
import vn.lovemoney.app.model.support.PostTicketResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.GuideUtils;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by DTA on 12/05/2017.
 */

public class SupportLoginDialog extends DialogFragment {

    private static final int PAGE_SIZE = 10;

    private ListView lv;
    private MessageItemAdapter adapter;
    private List<GetMessage> list;
    private ImageView btn_send_sms;
    private EditText enter_sms;
    private Context mContext;
    private int mTicketId;
    private View mCustomView;

    public static SupportLoginDialog newInstance() {
        Bundle args = new Bundle();
        SupportLoginDialog fragment = new SupportLoginDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();

        list = new ArrayList<>();
        adapter = new MessageItemAdapter(mContext, list);
        lv.setAdapter(adapter);

        process();
    }

    private void process() {
        String userFacebookId = null;
        if (!PreferenceManager.getInstance(mContext).isLoggedIn()) {
            userFacebookId = AccessToken.getCurrentAccessToken().getUserId();
        }
        int tempTicketId = PreferenceManager.getInstance(mContext).getSupportTicketProcessing();
        if (tempTicketId != -1) {
            mTicketId = tempTicketId;
            getMessageForNotLogin();
            return;
        }
        String content = mContext.getResources().getString(R.string.init_question_support);
        SupportBody body = new SupportBody(1, userFacebookId, content);
        Call<PostTicketResponse> call = RestClient.getInstance(mContext).getSupportApi().postTicket(body);
        call.enqueue(new RestCallback<PostTicketResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(PostTicketResponse response) {
                Toast.makeText(mContext, response.getDisplayMessage() + ", ticketId = " + response.getResult().getTicketId(), Toast.LENGTH_SHORT).show();
                mTicketId = response.getResult().getTicketId();
                PreferenceManager.getInstance(mContext).setSupportTicketProcessing(mTicketId);
                getMessageForNotLogin();
            }
        });
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.full_screen_dialog);
        mCustomView = LayoutInflater.from(getActivity()).inflate(R.layout.support_login_dialog, null);
        btn_send_sms = (ImageView) mCustomView.findViewById(R.id.btn_send_sms);
        enter_sms = (EditText) mCustomView.findViewById(R.id.enter_sms);
        lv = (ListView) mCustomView.findViewById(R.id.lv_detail_mail);

        btn_send_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String content = enter_sms.getText().toString();
                if (content.isEmpty()) {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.enter_sms), Toast.LENGTH_SHORT).show();
                } else {
                    String userFacebookId = null;
                    if (!PreferenceManager.getInstance(mContext).isLoggedIn()) {
                        userFacebookId = AccessToken.getCurrentAccessToken().getUserId();
                    }
                    SupportBody body = new SupportBody(userFacebookId, content);
                    Call<BaseResponse> call = RestClient.getInstance(mContext).getSupportApi().postMessage(mTicketId, body);
                    call.enqueue(new RestCallback<BaseResponse>() {
                        @Override
                        public void failure(RestError error) {
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void success(BaseResponse response) {
                            Toast.makeText(mContext, response.getDisplayMessage(), Toast.LENGTH_SHORT).show();
                            getMessageForNotLogin();
                        }
                    });
                    enter_sms.setText("");
                }
            }
        });

        enter_sms.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    btn_send_sms.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_send_click));
                } else {
                    btn_send_sms.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_send_normal));
                }
            }
        });
        setCancelable(false);
        builder.setView(mCustomView);
        return builder.create();
    }

    private void getMessageForNotLogin() {
        String userFacebookId = null;
        if (!PreferenceManager.getInstance(mContext).isLoggedIn()) {
            userFacebookId = AccessToken.getCurrentAccessToken().getUserId();
        }
        BasePagingRequest mBasePagingRequest = new BasePagingRequest(0, PAGE_SIZE);
        Call<GetMessageResponse> call = RestClient.getInstance(mContext).getSupportApi().getMessage(mTicketId, mBasePagingRequest.toQueryMap(), userFacebookId);
        call.enqueue(new RestCallback<GetMessageResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(GetMessageResponse response) {
                list.clear();
                List<GetMessage> listMessage = response.getResult();
                for (int i = listMessage.size() - 1; i >= 0; i--) {
                    list.add(listMessage.get(i));
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewGroup rootView = (ViewGroup) mCustomView.findViewById(R.id.rootView);
                GuideUtils.requestShowToolTip(GuideUtils.GUIDE_SUPPORT_LOGIN_SMS, mContext, rootView);
            }
        }, 1000);

    }
}
