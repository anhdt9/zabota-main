package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import vn.lovemoney.app.R;

/**
 * Created by 8470p on 4/27/2017.
 */

public class PaypalAlertDialog extends DialogFragment {

    private Context mContext;

    public static PaypalAlertDialog newInstance() {
        return new PaypalAlertDialog();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.full_screen_dialog);
        View mCustomView = LayoutInflater.from(mContext).inflate(R.layout.dialog_alert_paypal, null);

        TextView noti = (TextView) mCustomView.findViewById(R.id.notification);
        noti.setText(mContext.getResources().getString(R.string.notifications) + " !");

        builder.setView(mCustomView);
        return builder.create();
    }
}
