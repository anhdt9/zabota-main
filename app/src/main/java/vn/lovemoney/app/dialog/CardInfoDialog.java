package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import vn.lovemoney.app.R;
import vn.lovemoney.app.model.exchance.ExchangeItem;
import vn.lovemoney.app.model.exchance.RedeemItem;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.FreeXuUtils;

/**
 * Created by tuannguyen on 12/18/15.
 */
public class CardInfoDialog extends DialogFragment implements View.OnClickListener {

    private ExchangeItem mExchangeItem;
    private RedeemItem mRedeemItem;
    private Context mContext;

    public static CardInfoDialog newInstance(ExchangeItem exchangeItem, RedeemItem redeemItem) {
        Bundle args = new Bundle();
        args.putParcelable(ArgumentKeys.EXCHANGE_ITEM, exchangeItem);
        args.putParcelable(ArgumentKeys.REDEEM_ITEM, redeemItem);
        CardInfoDialog dlg = new CardInfoDialog();
        dlg.setArguments(args);
        return dlg;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mExchangeItem = getArguments().getParcelable(ArgumentKeys.EXCHANGE_ITEM);
        mRedeemItem = getArguments().getParcelable(ArgumentKeys.REDEEM_ITEM);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View mCustomView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_card_info, null);
        TextView tvMessageExchangeSuccess = (TextView) mCustomView.findViewById(R.id.txt_msg_exchange_card_success);
        TextView tvCardInfo = (TextView) mCustomView.findViewById(R.id.card_info);
        View btnContainer = mCustomView.findViewById(R.id.button_container);
        if (mExchangeItem.getType() != ExchangeItem.TYPE_MONEY) {
            btnContainer.setVisibility(View.VISIBLE);
            Button btnUse = (Button) mCustomView.findViewById(R.id.btn_use);
            Button btnCopy = (Button) mCustomView.findViewById(R.id.btn_copy);
            btnUse.setEnabled(mExchangeItem.getType() == ExchangeItem.TYPE_MOBILE_CARD);
            btnUse.setOnClickListener(this);
            btnCopy.setOnClickListener(this);
        } else {
            btnContainer.setVisibility(View.GONE);
        }

        String msg = mContext.getString(R.string.msg_redeem_success, mRedeemItem.getExchangePrice(), mRedeemItem.getProviderName(), mRedeemItem.getCredit(), mRedeemItem.getCurrency());
        tvMessageExchangeSuccess.setText(msg);

        String cardInfo = mContext.getString(R.string.redeem_description_info_dialog, mExchangeItem.getName(), mExchangeItem.getSerial(), mExchangeItem.getPin(), FreeXuUtils.getFormattedDate(mExchangeItem.getExpiredAt()));
        tvCardInfo.setText(Html.fromHtml(cardInfo));

        builder.setView(mCustomView);

        return builder.create();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_use:
                StringBuilder builder = new StringBuilder("tel:*100*");
                builder.append(mExchangeItem.getPin().replaceAll("[^0-9+]", ""));
                builder.append(Uri.encode("#"));
                String uri = builder.toString();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(uri));
                startActivity(intent);
                dismiss();
                break;
            case R.id.btn_copy:
                String cardInfo = mContext.getString(R.string.card_information_script, mExchangeItem.getName(), mExchangeItem.getSerial(), mExchangeItem.getPin());
                FreeXuUtils.saveToClipboard(mContext, cardInfo);
                Toast.makeText(mContext, mContext.getString(R.string.copy_success), Toast.LENGTH_LONG).show();
                dismiss();
                break;
        }
    }
}
