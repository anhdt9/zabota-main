package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.model.body.RequestAppBody;
import vn.lovemoney.app.model.facebook.FacebookItem;
import vn.lovemoney.app.model.facebook.PostFacebookItem;
import vn.lovemoney.app.model.facebook.PostFacebookOfferResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.FreeXuUtils;

public class LikeCommentReviewDialog extends DialogFragment implements View.OnClickListener {

    private static final int POST = 1;

    private boolean mClicked;
    private FacebookItem itemSelected;
    private PostFacebookItem mPostFacebookItem;
    private int mAction;
    private Context mContext;

    public static LikeCommentReviewDialog newInstance(FacebookItem item, int action) {
        Bundle args = new Bundle();
        args.putParcelable(ArgumentKeys.FACEBOOK_ITEM, item);
        args.putInt("action", action);
        LikeCommentReviewDialog fragment = new LikeCommentReviewDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        itemSelected = args.getParcelable(ArgumentKeys.FACEBOOK_ITEM);
        mAction = args.getInt("action", -1);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.comment_review_dialog_layout, null);

        TextView tv_title = (TextView) v.findViewById(R.id.tv_title);
        ImageView icon = (ImageView) v.findViewById(R.id.icon);
        TextView name = (TextView) v.findViewById(R.id.name);
        TextView bonus = (TextView) v.findViewById(R.id.bonus);
        TextView guide = (TextView) v.findViewById(R.id.review_comment_guide);
        if (itemSelected.getType() == POST) {
            guide.setText((TextUtils.isEmpty(itemSelected.getGuide())) ? mContext.getString(R.string.comment_guide) : Html.fromHtml(itemSelected.getGuide()));
        } else {
            guide.setText((TextUtils.isEmpty(itemSelected.getGuide())) ? mContext.getString(R.string.review_guide) : Html.fromHtml(itemSelected.getGuide()));
        }
        tv_title.setText(getTitle(mAction));
        Picasso.with(getActivity())
                .load(itemSelected.getIconUrl())
                .centerCrop()
                .fit()
                .placeholder(R.drawable.ic_image_empty)
                .into(icon);
        name.setText(itemSelected.getName());
        bonus.setText(String.valueOf(itemSelected.getBonus()));

        Button mBtnConfirm = (Button) v.findViewById(R.id.confirm);
        Button mBtnClose = (Button) v.findViewById(R.id.btn_close);
        mBtnConfirm.setOnClickListener(this);
        mBtnClose.setOnClickListener(this);

        builder.setView(v);
        AlertDialog dlg = builder.create();
        dlg.setCanceledOnTouchOutside(false);

        return dlg;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm:
                if (mClicked) {
                    return;
                }
                mClicked = true;
                postFacebookOffers();
                break;
            case R.id.btn_close:
                dismiss();
                break;
        }
    }

    private int getTitle(int action){
        switch (action){
            case FacebookItem.ACTION_COMMENT:
                return R.string.msg_comment_post;
            case FacebookItem.ACTION_LIKE:
                return R.string.like_confirmation;
            case FacebookItem.ACTION_REVIEW:
                return R.string.msg_review_page;
            default:
                return R.string.msg_comment_post;
        }
    }

    private void postFacebookOffers() {
        RequestAppBody body = new RequestAppBody(itemSelected.getCampaignId());
        Call<PostFacebookOfferResponse> call = RestClient.getInstance(getActivity()).getFacebookApi().postFacebookOffers(mAction, body);
        call.enqueue(new RestCallback<PostFacebookOfferResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(PostFacebookOfferResponse response) {
                mPostFacebookItem = response.getResult();
                FreeXuUtils.saveToClipboard(getActivity(), mPostFacebookItem.getContent());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(itemSelected.getUrl()));
                startActivity(intent);
                Toast.makeText(getActivity(), response.getDisplayMessage(), Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
