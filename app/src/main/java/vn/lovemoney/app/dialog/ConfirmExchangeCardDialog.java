package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.BaseActivity;
import vn.lovemoney.app.model.exchance.ExchangeItem;
import vn.lovemoney.app.model.exchance.RedeemExchange;
import vn.lovemoney.app.model.exchance.RedeemItem;
import vn.lovemoney.app.model.user.UserInfo;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.GuideUtils;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by 8470p on 4/22/2017.
 */

public class ConfirmExchangeCardDialog extends DialogFragment implements View.OnClickListener {

    private RedeemItem mRedeemItem;
    private Context mContext;
    private View mCustomView;

    public static ConfirmExchangeCardDialog newInstance(RedeemItem redeemItem) {
        Bundle args = new Bundle();
        args.putParcelable(ArgumentKeys.REDEEM_ITEM, redeemItem);
        ConfirmExchangeCardDialog dlg = new ConfirmExchangeCardDialog();
        dlg.setArguments(args);
        return dlg;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mRedeemItem = getArguments().getParcelable(ArgumentKeys.REDEEM_ITEM);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        mCustomView = LayoutInflater.from(mContext).inflate(R.layout.dialog_confirm_exchange_card, null);
        ImageView imageView = (ImageView) mCustomView.findViewById(R.id.icon);
        TextView tvMessageExchange = (TextView) mCustomView.findViewById(R.id.txt_msg_exchange_card);
        TextView tvCredit = (TextView) mCustomView.findViewById(R.id.credit);
        TextView tvPrice = (TextView) mCustomView.findViewById(R.id.price);
        Button btnCancel = (Button) mCustomView.findViewById(R.id.cancel);
        Button btnConfirm = (Button) mCustomView.findViewById(R.id.ok);
        btnConfirm.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        Picasso.with(mContext)
                .load(mRedeemItem.getLogoUrl())
                .fit()
                .placeholder(R.drawable.ic_image_empty)
                .error(R.drawable.ic_image_empty)
                .into(imageView);

        String msg = mContext.getString(R.string.msg_redeem_confirmation, mRedeemItem.getExchangePrice(), mRedeemItem.getProviderName(), mRedeemItem.getCredit(), mRedeemItem.getCurrency());
        tvMessageExchange.setText(msg);
        String credit = mContext.getString(R.string.card_title, mRedeemItem.getProviderName(), mRedeemItem.getCredit(), mRedeemItem.getCurrency());
        tvCredit.setText(credit);
        String priceStr = mContext.getString(R.string.price, mRedeemItem.getExchangePrice());
        tvPrice.setText(priceStr);
        builder.setView(mCustomView);

        return builder.create();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.ok) {
            UserInfo userInfo = PreferenceManager.getInstance(mContext).getUserInfo();
            if (userInfo.getBalance() < mRedeemItem.getExchangePrice()) {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.not_enough_money), Toast.LENGTH_LONG).show();
            } else {
                exchange(mRedeemItem.getId());
            }
        } else{
            dismiss();
        }
    }

    private void exchange(int redeemId) {
        Call<RedeemExchange> call = RestClient.getInstance(getActivity()).getExchangeApi().getRedeemExchange(redeemId);
        final BaseActivity activity = (BaseActivity) getActivity();
        activity.showProgressDialog();
        call.enqueue(new RestCallback<RedeemExchange>() {
            @Override
            public void failure(RestError error) {
                activity.dismissProgressDialog();
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(RedeemExchange response) {
                ExchangeItem exchangeItem = response.getResult();
                Singleton.getInstance().setShouldUpdateProfile(true);
                EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
                Toast.makeText(mContext, response.getDisplayMessage(), Toast.LENGTH_LONG).show();

                showCardInfo(exchangeItem, mRedeemItem);
                activity.dismissProgressDialog();
                dismiss();

            }
        });
    }

    private void showCardInfo(final ExchangeItem exchangeItem, RedeemItem redeemItem) {
        CardInfoDialog dialog = CardInfoDialog.newInstance(exchangeItem, redeemItem);
        dialog.show(getFragmentManager(), null);
    }

    @Override
    public void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewGroup rootView = (ViewGroup) mCustomView.findViewById(R.id.rootView);
                GuideUtils.requestShowToolTip(GuideUtils.GUIDE_REDEEM_CONFIRM_CHANGE, mContext, rootView);
            }
        }, 1000);
    }
}
