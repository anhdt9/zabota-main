package vn.lovemoney.app.dialog;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.BaseActivity;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.android.app.InstallationInfo;
import vn.lovemoney.app.model.android.app.NewAppOffer;
import vn.lovemoney.app.model.android.app.RequestInstallResponse;
import vn.lovemoney.app.model.android.app.ReviewInfo;
import vn.lovemoney.app.model.android.app.ReviewResponse;
import vn.lovemoney.app.model.body.RequestAppBody;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.service.OfferManagerService;
import vn.lovemoney.app.util.ActivityNavigator;
import vn.lovemoney.app.util.AppInfoManager;
import vn.lovemoney.app.util.ArgumentKeys;
import vn.lovemoney.app.util.FreeXuUtils;
import vn.lovemoney.app.util.GuideUtils;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by tuannguyen on 12/14/15.
 */
public class AppOfferInfoDialog extends DialogFragment implements View.OnClickListener {

    private NewAppOffer mAppOffer;
    private Context mContext;
    private int mStatus;
    private Button btnInstall, btnReview, btnConfirm;
    private View mCustomView;

    public static AppOfferInfoDialog newInstance(NewAppOffer offer) {
        Bundle args = new Bundle();
        args.putParcelable(ArgumentKeys.APP_OFFER, offer);
        AppOfferInfoDialog dlg = new AppOfferInfoDialog();
        dlg.setArguments(args);
        return dlg;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppOffer = getArguments().getParcelable(ArgumentKeys.APP_OFFER);
        mStatus = mAppOffer.getStatus();
        getActivity().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.full_screen_dialog);
        mCustomView = LayoutInflater.from(mContext).inflate(R.layout.dialog_app_offer_info, null);
        ImageView ivIcon = (ImageView) mCustomView.findViewById(R.id.icon);
        TextView txtName = (TextView) mCustomView.findViewById(R.id.name);
        TextView txtPrice = (TextView) mCustomView.findViewById(R.id.price);
        ImageView ivState = (ImageView) mCustomView.findViewById(R.id.state);
        TextView txtStateAlert = (TextView) mCustomView.findViewById(R.id.state_alert);
        TextView txtOfferGuide = (TextView) mCustomView.findViewById(R.id.guide);
        btnInstall = (Button) mCustomView.findViewById(R.id.btn_install);
        btnReview = (Button) mCustomView.findViewById(R.id.btn_review);
        btnConfirm = (Button) mCustomView.findViewById(R.id.verify);

        btnInstall.setOnClickListener(this);
        btnReview.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);

        updateStatusButton();

        Picasso.with(mContext)
                .load(mAppOffer.getIconUrl())
                .fit()
                .placeholder(R.drawable.ic_image_empty)
                .error(R.drawable.ic_image_empty)
                .into(ivIcon);

        int state = mAppOffer.getStatus();
        txtName.setText(mAppOffer.getName());
        ivState.setSelected(state == NewAppOffer.STATUS_INSTALLED || state == NewAppOffer.STATUS_VERIFIED);
        txtStateAlert.setText(state == NewAppOffer.STATUS_INSTALLED ? R.string.confirm_now : (state == NewAppOffer.STATUS_VERIFIED ? R.string.review_now : R.string.install_now));
        txtPrice.setText(String.valueOf(mAppOffer.getBonus()));
        Spanned guide = Html.fromHtml(TextUtils.isEmpty(mAppOffer.getGuide()) ? mContext.getResources().getString(R.string.offer_description, mAppOffer.getName()) : mAppOffer.getGuide());
        txtOfferGuide.setText(guide);
        builder.setView(mCustomView);
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_install:
                dismiss();
                installAppAction();
                break;
            case R.id.btn_review:
                dismiss();
                reviewAppAction();
                break;
            case R.id.verify:
                dismiss();
                confirmAppAction();
                break;
        }
    }

    private void updateStatusButton() {
        if (mStatus == 1) {
            btnInstall.setEnabled(false);
            btnConfirm.setEnabled(true);
            btnInstall.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_button_long_selector));
            btnConfirm.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_button_short_selector));

            if (mAppOffer.isReviewed()) {
                btnReview.setEnabled(false);
            } else {
                btnReview.setEnabled(true);
            }
            btnReview.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_button_short_selector));
        } else if (mStatus == 2) {
            btnInstall.setEnabled(false);
            btnConfirm.setEnabled(false);
            btnInstall.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_button_long_selector));
            btnConfirm.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_button_short_selector));

            if (mAppOffer.isReviewed()) {
                btnReview.setEnabled(false);
            } else {
                btnReview.setEnabled(true);
            }
            btnReview.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.icon_button_short_selector));
        } else {

            btnReview.setEnabled(false);
            btnConfirm.setEnabled(false);
        }
    }

    private void installAppAction() {
        final BaseActivity activity = (BaseActivity) getActivity();
        RequestAppBody body = new RequestAppBody(mAppOffer.getCampaignId());
        Call<RequestInstallResponse> call = RestClient.getInstance(getActivity()).getOfferApi().requestInstallApp(body);
        activity.showProgressDialog();
        call.enqueue(new RestCallback<RequestInstallResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
                activity.dismissProgressDialog();
            }

            @Override
            public void success(RequestInstallResponse response) {
                InstallationInfo ii = response.getResult();
                AppInfoManager.getInstance(mContext).saveAppOffer(mAppOffer.getPackageBundle(), mAppOffer);
                try {
                    ActivityNavigator.openInstallationLink(activity, ii.getInstallationLink());

                    OfferManagerService.setListener(new OfferManagerService.IAppInstallListener() {
                        @Override
                        public void appInstallSuccess() {
                            Singleton.getInstance().setShouldRefreshOfferList(true);
                        }
                    });

                } catch (ActivityNotFoundException e) {
                    Toast.makeText(activity, R.string.msg_google_play_is_not_installed, Toast.LENGTH_SHORT).show();
                }
                activity.dismissProgressDialog();
            }
        });
    }

    private void reviewAppAction() {
        final BaseActivity activity = (BaseActivity) getActivity();
        RequestAppBody appBody = new RequestAppBody(mAppOffer.getCampaignId());
        Call<ReviewResponse> call = RestClient.getInstance(getActivity()).getOfferApi().reviewApp(appBody);
        activity.showProgressDialog();
        call.enqueue(new RestCallback<ReviewResponse>() {

            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
                activity.dismissProgressDialog();
            }

            @Override
            public void success(ReviewResponse response) {
                ReviewInfo reviewInfo = response.getResult();
                FreeXuUtils.saveToClipboard(activity, reviewInfo.getContent());
                ActivityNavigator.openInstallationLink(activity, reviewInfo.getReviewUrl());
                Singleton.getInstance().setShouldRefreshOfferList(true);
                EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_OFFER_LIST));
                Singleton.getInstance().setShouldUpdateProfile(true);
                EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
                activity.dismissProgressDialog();
            }
        });
    }

    private void confirmAppAction() {
        if (FreeXuUtils.isAppRunning(mContext, mAppOffer.getPackageBundle())) {
            try {
                // Try to open app
                PackageManager pm = mContext.getPackageManager();
                Intent intent = pm.getLaunchIntentForPackage(mAppOffer.getPackageBundle());
                mContext.startActivity(intent);

                final BaseActivity activity = (BaseActivity) getActivity();
                RequestAppBody appBody = new RequestAppBody(mAppOffer.getCampaignId());
                Call<BaseResponse> call = RestClient.getInstance(getActivity()).getOfferApi().verifyApp(appBody);
                activity.showProgressDialog();
                call.enqueue(new RestCallback<BaseResponse>() {
                    @Override
                    public void failure(RestError error) {
                        if (error.getCode() != 0) {
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        activity.dismissProgressDialog();
                    }

                    @Override
                    public void success(BaseResponse response) {
                        Toast.makeText(activity, response.getDisplayMessage(), Toast.LENGTH_SHORT).show();
                        Singleton.getInstance().setShouldRefreshOfferList(true);
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_OFFER_LIST));
                        Singleton.getInstance().setShouldUpdateReOpenAppList(true);
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_REOPEN_LIST));
                        Singleton.getInstance().setShouldUpdateProfile(true);
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.UPDATE_PROFILE));
                        activity.dismissProgressDialog();
                    }
                });
            } catch (Exception e) {
                Toast.makeText(mContext, R.string.msg_cannot_open_app, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.confirm_confirm_install_app), Toast.LENGTH_LONG).show();
        }
    }

    private void showUnknownSourceWarningDialog() {
        AlertDialog dlg = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.alert)
                .setMessage(R.string.msg_turn_on_unknown_source)
                .setPositiveButton(R.string.config_now, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_SECURITY_SETTINGS));
                    }
                })
                .create();
        dlg.show();
    }

    private boolean checkUnknownSourceSettings() {
        try {
            return Settings.Secure.getInt(getActivity().getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS) == 1;
        } catch (Settings.SettingNotFoundException e) {
            return false;
        }
    }

//    private String configUrl(String url) {
//        url = url.replace("\\/\\/", "");
//        url = "http:" + url;
//        return url;
//     }

    @Override
    public void onAttach(Context context) {
        mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewGroup rootView = (ViewGroup) mCustomView.findViewById(R.id.rootView);
                GuideUtils.requestShowToolTip(GuideUtils.GUIDE_INSTALL_DIALOG, mContext, rootView);
            }
        }, 1000);
    }
}
