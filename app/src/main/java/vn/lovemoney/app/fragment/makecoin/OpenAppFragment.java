package vn.lovemoney.app.fragment.makecoin;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.makecoin.ReOpenAppAdapter;
import vn.lovemoney.app.dialog.ReopenAppDialog;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.BasePagingRequest;
import vn.lovemoney.app.model.android.app.ReOpenResponse;
import vn.lovemoney.app.model.user.ReOpenApp;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;
import vn.lovemoney.app.view.widget.LoadMoreListView;

/**
 * Created by Laptop TCC on 3/5/2017.
 */

public class OpenAppFragment extends BaseFragment implements LoadMoreListView.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener {
    private static final int PAGE_SIZE = 10;
    private List<ReOpenApp> mReOpenApps;
    private LoadMoreListView mLvReOpenApp;
    private ReOpenAppAdapter mAdapter;
    private BasePagingRequest mRequest;
    private SwipeRefreshLayout mSwipeLayout;
    private boolean mIsFirstLoad;
    private Context mContext;

    public static OpenAppFragment newInstance() {
        return new OpenAppFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_open_app_list, container, false);
        mIsFirstLoad = savedInstanceState == null;
        mReOpenApps = new ArrayList<>();
        mRequest = new BasePagingRequest(0, PAGE_SIZE);
        mSwipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mLvReOpenApp = (LoadMoreListView) v.findViewById(R.id.list);
        mAdapter = new ReOpenAppAdapter(mContext, mReOpenApps);
        mLvReOpenApp.setAdapter(mAdapter);
        mLvReOpenApp.setOnLoadMoreListener(this);
        mLvReOpenApp.setOnItemClickListener(this);
        mSwipeLayout.setOnRefreshListener(this);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadReOpenApp();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if (e.getEvent() == MessageEvent.UPDATE_REOPEN_LIST) {
            if (Singleton.getInstance().shouldUpdateReOpenAppList()) {
                onRefresh();
            }
        }
    }

    @Override
    public void onLoadMore() {
        loadReOpenApp();
    }

    @Override
    public void onRefresh() {
        mReOpenApps.clear();
        mAdapter.notifyDataSetChanged();
        mRequest.reset();
        loadReOpenApp();
        Singleton.getInstance().setShouldUpdateReOpenAppList(false);
    }

    private void loadReOpenApp() {
        Call<ReOpenResponse> call = RestClient.getInstance(mContext).getOfferApi().getReOpenApp(mRequest.toQueryMap());
        call.enqueue(new RestCallback<ReOpenResponse>() {
            @Override
            public void failure(RestError error) {
                mLvReOpenApp.onLoadFailed();
                mSwipeLayout.setRefreshing(false);
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(ReOpenResponse response) {
                mReOpenApps.clear();
                mReOpenApps.addAll(response.getResult());
                mRequest.nextPage();

                if (response.getItemCount() <= mReOpenApps.size()) {
                    mLvReOpenApp.onFinishAll();
                } else {
                    mLvReOpenApp.onLoadMoreComplete();
                }
                mAdapter.notifyDataSetChanged();
                mSwipeLayout.setRefreshing(false);

            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        final ReOpenApp appOffer = mReOpenApps.get(position);

        ReopenAppDialog dialog = ReopenAppDialog.newInstance(appOffer);
        dialog.show(getFragmentManager(), null);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!mIsFirstLoad && Singleton.getInstance().shouldUpdateReOpenAppList()) {
            Singleton.getInstance().setShouldUpdateReOpenAppList(false);
            onRefresh();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
