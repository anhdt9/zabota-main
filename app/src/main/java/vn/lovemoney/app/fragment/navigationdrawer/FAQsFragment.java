package vn.lovemoney.app.fragment.navigationdrawer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.DetailFAQActivity;
import vn.lovemoney.app.adapter.navigationdrawer.mail.detail.FaqsAdapter;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.BasePagingRequest;
import vn.lovemoney.app.model.navigationdrawer.mail.detail.FAQs;
import vn.lovemoney.app.model.support.GetFAQsResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.view.widget.LoadMoreListView;

/**
 * Created by 8470p on 3/14/2017.
 */

public class FAQsFragment extends BaseFragment implements LoadMoreListView.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    private static final int PAGE_SIZE = 10;
    private List<FAQs> mFAQs;
    private FaqsAdapter mAdapter;
    private BasePagingRequest mRequest;
    private Context mContext;

    public static FAQsFragment newInstance() {
        return new FAQsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_fags, container, false);

        mFAQs = new ArrayList<>();
        mRequest = new BasePagingRequest(0, PAGE_SIZE);
        ListView mLvFAQs = (ListView) v.findViewById(R.id.list);
        mAdapter = new FaqsAdapter(mContext, mFAQs);
        mLvFAQs.setAdapter(mAdapter);
        mLvFAQs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FAQs f = mFAQs.get(position);
                Intent intent = new Intent(mContext, DetailFAQActivity.class);
                intent.putExtra("faq_stt",f.getId() );
                intent.putExtra("faq_question", f.getQuestion());
                intent.putExtra("faq_answer", f.getAnswer());
                startActivity(intent);
            }
});
        loadFAQs();
        return v;
    }

    @Override
    public void onLoadMore() {
        loadFAQs();
    }

    @Override
    public void onRefresh() {
        mFAQs.clear();
        mAdapter.notifyDataSetChanged();
        mRequest.reset();
        loadFAQs();
    }

    private void loadFAQs() {
        Call<GetFAQsResponse> call = RestClient.getInstance(mContext).getSupportApi().getFAQs();
        call.enqueue(new RestCallback<GetFAQsResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(GetFAQsResponse response) {
                mFAQs.clear();
                mFAQs.addAll(Arrays.asList(response.getResult()));
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        recordScreenName("FAQs");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
    }
}
