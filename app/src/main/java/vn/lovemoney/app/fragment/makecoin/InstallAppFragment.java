package vn.lovemoney.app.fragment.makecoin;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.FreeXuApplication;
import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.makecoin.OfferAdapter;
import vn.lovemoney.app.dialog.AppOfferInfoDialog;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.BasePagingRequest;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.android.app.NewAppOffer;
import vn.lovemoney.app.model.android.app.OfferListResponse;
import vn.lovemoney.app.model.body.RequestAppBody;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.FreeXuUtils;
import vn.lovemoney.app.util.GuideUtils;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;
import vn.lovemoney.app.view.widget.LoadMoreListView;

public class InstallAppFragment extends BaseFragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, LoadMoreListView.OnLoadMoreListener {
    private static final int PAGE_SIZE = 10;
    private LoadMoreListView mLvOffers;
    private OfferAdapter mAdapter;
    private List<NewAppOffer> mOffers;
    private SwipeRefreshLayout mSwipeLayout;
    private ViewSwitcher mViewSwitcher;
    private Context mContext;
    private BasePagingRequest mRequest;

    public static InstallAppFragment newInstance() {
        return new InstallAppFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        mOffers = new ArrayList<>();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Singleton.getInstance().shouldRefreshOfferList()) {
            Log.i("duynguyen", "onResume : shouldRefreshOfferList");
            Singleton.getInstance().setShouldRefreshOfferList(false);
            refreshData();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_offer_list, container, false);
        mSwipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mLvOffers = (LoadMoreListView) v.findViewById(R.id.list);
        mViewSwitcher = (ViewSwitcher) v.findViewById(R.id.view_switcher);
        mAdapter = new OfferAdapter(getActivity(), mOffers);
        mRequest = new BasePagingRequest(0, PAGE_SIZE);

        mLvOffers.setAdapter(mAdapter);
        mLvOffers.setOnLoadMoreListener(this);
        mLvOffers.setOnItemClickListener(this);
        mSwipeLayout.setOnRefreshListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadOffers();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent e) {
        if (e.getEvent() == MessageEvent.UPDATE_OFFER_LIST) {
            if (Singleton.getInstance().shouldRefreshOfferList()) {
                Singleton.getInstance().setShouldRefreshOfferList(false);
                refreshData();
            }
        }
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AppOfferInfoDialog dlg = AppOfferInfoDialog.newInstance(mOffers.get(position));
        dlg.show(getFragmentManager(), null);
    }

    @Override
    public void onRefresh() {
        refreshData();
    }

    private void refreshData() {
        mOffers.clear();
        mAdapter.notifyDataSetChanged();
        mRequest.reset();
        loadOffers();
    }

    private void loadOffers() {
        // new
        Call<OfferListResponse> appOfferList = RestClient.getInstance(getActivity()).getOfferApi().getOffers(mRequest.toQueryMap());
        appOfferList.enqueue(new RestCallback<OfferListResponse>() {
            @Override
            public void failure(RestError error) {
                mViewSwitcher.setDisplayedChild(1);
                mLvOffers.onLoadFailed();
                mSwipeLayout.setRefreshing(false);
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(OfferListResponse response) {
                mViewSwitcher.setDisplayedChild(0);
                List<NewAppOffer> appOffers = response.getResult();
                if (appOffers != null && appOffers.size() > 0) {
                    for (int i = appOffers.size() - 1; i >= 0; i--) {
                        NewAppOffer appOffer = appOffers.get(i);
                        int installationState = appOffer.getStatus();
                        if ((installationState == NewAppOffer.STATUS_NOT_REQUESTED || installationState == NewAppOffer.STATUS_REQUESTED) && FreeXuUtils.isAppInstalled(FreeXuApplication.getInstance(), appOffer.getPackageBundle())) {
                            reportAppInstalled(appOffer.getCampaignId());
                            appOffers.remove(i);
                        }
                    }
                }

                mOffers.clear();
                mOffers.addAll(appOffers);
                mRequest.nextPage();
                mLvOffers.onFinishAll();
                mAdapter.notifyDataSetChanged();
                mSwipeLayout.setRefreshing(false);
            }
        });
    }

    private void reportAppInstalled(int campaignId) {
        RequestAppBody appBody = new RequestAppBody(campaignId);
        Call<BaseResponse> call = RestClient.getInstance(getActivity()).getOfferApi().installApp(appBody);
        call.enqueue(new RestCallback<BaseResponse>() {
            @Override
            public void failure(RestError error) {

            }

            @Override
            public void success(BaseResponse response) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
    }

    @Override
    public void onLoadMore() {
        loadOffers();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mOffers.size() > 0) {
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.SHOW_TOOLTIP, GuideUtils.GUIDE_INSTALL_DOWNLOAD));
                    }
                }
            }, 300);
        }
    }
}