package vn.lovemoney.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.getcoin.RewardPagerAdapter;
import vn.lovemoney.app.util.FreeXuUtils;

/**
 * Created by Laptop TCC on 3/5/2017.
 */

public class RedeemFragment extends BaseFragment {

    public static final String TAG = RedeemFragment.class.getSimpleName();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RewardPagerAdapter mGetCoinPagerAdapter;

    public RedeemFragment() {

    }

    public static RedeemFragment newInstance() {
        Bundle args = new Bundle();
        RedeemFragment fragment = new RedeemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGetCoinPagerAdapter = new RewardPagerAdapter(getChildFragmentManager(), getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        recordScreenName("Reward");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_make_coin, container, false);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);

        //Set an Apater for the View Pager
        viewPager.setAdapter(mGetCoinPagerAdapter);
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
                boolean showCounter[] = new boolean[mGetCoinPagerAdapter.getTabTitles().length];
                FreeXuUtils.customTab(getActivity(), mGetCoinPagerAdapter, tabLayout, mGetCoinPagerAdapter.getTabTitles(), showCounter);

            }
        });

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}