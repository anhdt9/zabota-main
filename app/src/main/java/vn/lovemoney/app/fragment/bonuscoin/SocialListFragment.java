package vn.lovemoney.app.fragment.bonuscoin;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.social.FacebookActivity;
import vn.lovemoney.app.adapter.bonus.social.SocialAdapter;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.bonus.coin.social.SocialItem;

/**
 * Created by Laptop TCC on 3/12/2017.
 */

public class SocialListFragment extends BaseFragment implements AdapterView.OnItemClickListener, View.OnClickListener {

    private SocialAdapter mAdapter;
    private List<SocialItem> mSocialItems;
    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSocialItems = new ArrayList<>();
        mAdapter = new SocialAdapter(getActivity(), mSocialItems);
        loadSocialItems();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_social, container, false);
        ListView mLvSocials = (ListView) v.findViewById(R.id.list);

        mLvSocials.setAdapter(mAdapter);
        mLvSocials.setOnItemClickListener(this);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(mContext, FacebookActivity.class);
        intent.putExtra("facebook_action", position + 1);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_left, R.anim.hold);
    }

    private void loadSocialItems() {

        String[] mTitles = mContext.getResources().getStringArray(R.array.fb_titles);
        TypedArray mIcons = getResources().obtainTypedArray(R.array.fb_icons);

        for (int i = 0; i < mTitles.length; i++) {
            SocialItem socialItem = new SocialItem();
            socialItem.setTitle(mTitles[i]);
            socialItem.setIcon(mIcons.getDrawable(i));
            mSocialItems.add(socialItem);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
