package vn.lovemoney.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.makecoin.MakeCoinPagerAdapter;
import vn.lovemoney.app.util.FreeXuUtils;

/**
 * Created by Laptop TCC on 3/5/2017.
 */

public class MakeCoinFragment extends BaseFragment {

    public static final String TAG = MakeCoinFragment.class.getSimpleName();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MakeCoinPagerAdapter mMakeCoinPagerAdapter;

    public MakeCoinFragment() {

    }

    public static MakeCoinFragment newInstance() {
        Bundle args = new Bundle();
        MakeCoinFragment fragment = new MakeCoinFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMakeCoinPagerAdapter = new MakeCoinPagerAdapter(getChildFragmentManager(), getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_make_coin, container, false);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);

        //Set an Apater for the View Pager
        viewPager.setAdapter(mMakeCoinPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        boolean showCounter[] = new boolean[mMakeCoinPagerAdapter.getTabTitles().length];
        FreeXuUtils.customTab(getActivity(), mMakeCoinPagerAdapter, tabLayout, mMakeCoinPagerAdapter.getTabTitles(), showCounter);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
