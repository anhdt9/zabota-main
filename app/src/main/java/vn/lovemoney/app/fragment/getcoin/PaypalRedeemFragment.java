package vn.lovemoney.app.fragment.getcoin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.getcoin.paypal.ConfirmPaypalAccountActivity;
import vn.lovemoney.app.adapter.getcoin.PaypalAdapter;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.exchance.PaypalItem;
import vn.lovemoney.app.model.exchance.ShowPaypalListResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.view.widget.LoadMoreListView;

/**
 * Created by Laptop TCC on 3/5/2017.
 */

public class PaypalRedeemFragment extends BaseFragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, LoadMoreListView.OnLoadMoreListener {

    private LoadMoreListView mLvPaypals;
    private PaypalAdapter mAdapter;
    private List<PaypalItem> mPaypals = null;
    private SwipeRefreshLayout mSwipeLayout;
    private Context mContext;

    public static PaypalRedeemFragment newIntance() {
        return new PaypalRedeemFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPaypals = new ArrayList<>();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_paypal, container, false);

        mSwipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mLvPaypals = (LoadMoreListView) v.findViewById(R.id.list);
        View header = inflater.inflate(R.layout.listitem_header_paypal, null);
        mAdapter = new PaypalAdapter(getActivity(), mPaypals);
        mLvPaypals.setAdapter(mAdapter);
        mLvPaypals.addHeaderView(header, null, false);
        mLvPaypals.setOnItemClickListener(this);
        mLvPaypals.setOnLoadMoreListener(this);
        mSwipeLayout.setOnRefreshListener(this);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onRefresh() {
        mPaypals.clear();
        mAdapter.notifyDataSetChanged();
        getPaypalList();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PaypalItem paypalItem = mPaypals.get(position - 1);
        Intent intent = new Intent(getActivity(), ConfirmPaypalAccountActivity.class);
        intent.putExtra("paypalItem", paypalItem);
        startActivity(intent);
    }

    @Override
    public void onLoadMore() {
        getPaypalList();
    }

    private void getPaypalList() {
        Call<ShowPaypalListResponse> call = RestClient.getInstance(mContext).getExchangeApi().getPaypalList();
        call.enqueue(new RestCallback<ShowPaypalListResponse>() {
            @Override
            public void failure(RestError error) {
                mLvPaypals.onLoadFailed();
                mSwipeLayout.setRefreshing(false);
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(ShowPaypalListResponse response) {
                mPaypals.clear();
                mPaypals.addAll(response.getResult());
                mLvPaypals.onFinishAll();
                mAdapter.notifyDataSetChanged();
                mSwipeLayout.setRefreshing(false);
            }
        });
    }
}
