package vn.lovemoney.app.fragment.navigationdrawer;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.navigationdrawer.mail.detail.CoinHistoryAdapter;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.BasePagingRequest;
import vn.lovemoney.app.model.CoinHistory;
import vn.lovemoney.app.model.CoinHistoryListResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.view.widget.LoadMoreListView;

/**
 * Created by 8470p on 3/14/2017.
 */

public class TransactionHistoryAllFragment extends BaseFragment implements LoadMoreListView.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    private static final int PAGE_SIZE = 10;
    private List<CoinHistory> mCoinHistories;
    private LoadMoreListView mLvCoinHistories;
    private SwipeRefreshLayout mSwipeLayout;
    private CoinHistoryAdapter mAdapter;
    private BasePagingRequest mRequest;
    private Context mContext;

    public static TransactionHistoryAllFragment newInstance() {
        return new TransactionHistoryAllFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_transaction_history, container, false);

        mCoinHistories = new ArrayList<>();
        mRequest = new BasePagingRequest(0, PAGE_SIZE);
        mLvCoinHistories = (LoadMoreListView) v.findViewById(R.id.list);
        mSwipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mAdapter = new CoinHistoryAdapter(mContext, mCoinHistories);
        mLvCoinHistories.setAdapter(mAdapter);
        mLvCoinHistories.setOnLoadMoreListener(this);
        mSwipeLayout.setOnRefreshListener(this);

        return v;
    }

    @Override
    public void onLoadMore() {
        loadActivityHistories();
    }

    @Override
    public void onRefresh() {
        mCoinHistories.clear();
        mAdapter.notifyDataSetChanged();
        mRequest.reset();
        loadActivityHistories();
    }

    private void loadActivityHistories(){
        Call<CoinHistoryListResponse> call = RestClient.getInstance(mContext).getUserApi().getCoinHistories(mRequest.toQueryMap());
        call.enqueue(new RestCallback<CoinHistoryListResponse>() {
            @Override
            public void failure(RestError error) {
                mLvCoinHistories.onLoadFailed();
                mSwipeLayout.setRefreshing(false);
                if(error.getCode() != 0){
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(CoinHistoryListResponse response) {
                mCoinHistories.addAll(response.getResult());
                mRequest.nextPage();
                if(response.getItemCount() <= mCoinHistories.size()){
                    mLvCoinHistories.onFinishAll();
                } else{
                    mLvCoinHistories.onLoadMoreComplete();
                }
                mAdapter.notifyDataSetChanged();
                mSwipeLayout.setRefreshing(false);

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        recordScreenName("Transaction History");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
    }
}
