package vn.lovemoney.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.google.firebase.analytics.FirebaseAnalytics;

import vn.lovemoney.app.FreeXuApplication;

/**
 * Created by tuannguyen on 12/23/15.
 */
public class BaseFragment extends Fragment {

	private FirebaseAnalytics mFirebaseAnalytics;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

	}

	protected void recordScreenName(String screenName){
		mFirebaseAnalytics.setCurrentScreen(getActivity(), screenName, null);
	}
}
