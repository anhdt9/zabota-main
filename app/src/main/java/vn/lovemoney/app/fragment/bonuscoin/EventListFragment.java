package vn.lovemoney.app.fragment.bonuscoin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.EventActivity;
import vn.lovemoney.app.adapter.EventAdapter;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.EventListResponse;
import vn.lovemoney.app.model.bonus.coin.event.EventItem;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;

/**
 * Created by Laptop TCC on 3/11/2017.
 */

public class EventListFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    private ListView lv;
    private ArrayList<EventItem> list;
    private EventAdapter adapter;
    private Context mContext;
    private TextView noItem;

    public EventListFragment() {
    }

    public static EventListFragment newInstance() {
        Bundle args = new Bundle();
        EventListFragment fragment = new EventListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_social, container, false);
        lv = (ListView) v.findViewById(R.id.list);
        noItem = (TextView) v.findViewById(R.id.noItem);
        adapter = new EventAdapter(getActivity(), list);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = new ArrayList<>();
        loadData();

    }

    private void loadData() {
        Call<EventListResponse> call = RestClient.getInstance(mContext).getUserApi().getEvents();
        call.enqueue(new RestCallback<EventListResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                    setView(true);
                }
            }

            @Override
            public void success(EventListResponse response) {
                if (response.getResult().length == 0) {
                    setView(true);
                    return;
                }
                list.clear();
                list.addAll(Arrays.asList(response.getResult()));
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        recordScreenName("Event List");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        EventItem event = list.get(position);
        Intent intent = new Intent(mContext, EventActivity.class);
        intent.putExtra("event_id", event.getId());
        intent.putExtra("event_title", event.getTitle());
        intent.putExtra("event_guide", event.getGuide());
        intent.putExtra("event_end_at", event.getEndAt());
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_left, R.anim.hold);
    }

    private void setView(boolean isNoItem) {
        noItem.setVisibility(isNoItem ? View.VISIBLE : View.GONE);
        lv.setVisibility(isNoItem ? View.GONE : View.VISIBLE);
    }
}
