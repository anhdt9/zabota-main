package vn.lovemoney.app.fragment.navigationdrawer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.navigation.mail.detail.DetailMessageActivity;
import vn.lovemoney.app.adapter.navigationdrawer.mail.detail.SendMessageAdapter;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.body.SupportBody;
import vn.lovemoney.app.model.support.GetSupportThead;
import vn.lovemoney.app.model.support.GetSupportThreadResponse;
import vn.lovemoney.app.model.support.PostTicketResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.PreferenceManager;

/**
 * Created by 8470p on 3/14/2017.
 */

public class SendMessageFragment extends BaseFragment implements View.OnClickListener {

    private static final int PROCESSING = 1;

    private SendMessageAdapter mSendMessageAdapter;
    private List<GetSupportThead> mListQuestion;
    private Context mContext;
    private int mPreviousSelected = -1;
    private int mCurrentSelected = -1;
    private View mView;
    private int mThreadSelected = -1;
    private int mTicketProcessing = -1;

    public static SendMessageFragment newInstance() {
        return new SendMessageFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_send_message, container, false);

        mPreviousSelected = mCurrentSelected = mThreadSelected = mTicketProcessing = -1;

        mListQuestion = new ArrayList<>();
        ListView mListview = (ListView) v.findViewById(R.id.list_question);
        RelativeLayout mNewMessage = (RelativeLayout) v.findViewById(R.id.layout_new_message);
        RelativeLayout layout_have_sms = (RelativeLayout) v.findViewById(R.id.layout_have_sms);
        TextView text = (TextView) v.findViewById(R.id.text);
        Button btn_open_sms = (Button) v.findViewById(R.id.btn_open_sms);

        mSendMessageAdapter = new SendMessageAdapter(mContext, mListQuestion);
        mListview.setAdapter(mSendMessageAdapter);

        mTicketProcessing = PreferenceManager.getInstance(mContext).getTicketProcessing();
        if (mTicketProcessing != -1) {
            text.setVisibility(View.GONE);
            mNewMessage.setVisibility(View.GONE);
            mListview.setVisibility(View.GONE);
            layout_have_sms.setVisibility(View.VISIBLE);
            btn_open_sms.setOnClickListener(this);
        } else {
            text.setVisibility(View.VISIBLE);
            mNewMessage.setVisibility(View.VISIBLE);
            mListview.setVisibility(View.VISIBLE);
            layout_have_sms.setVisibility(View.GONE);

            getData();
            mNewMessage.setOnClickListener(this);

            mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mPreviousSelected = mCurrentSelected;
                    mCurrentSelected = position;
                    mThreadSelected = mListQuestion.get(position).getThreadId();

                    boolean current_state = mListQuestion.get(mCurrentSelected).isSelected();
                    current_state = !current_state;
                    mListQuestion.get(mCurrentSelected).setSelected(current_state);

                    if (mPreviousSelected != -1) {
                        boolean previous_state = mListQuestion.get(mPreviousSelected).isSelected();
                        previous_state = !previous_state;
                        mListQuestion.get(mPreviousSelected).setSelected(previous_state);
                    }

                    mSendMessageAdapter.notifyDataSetChanged();
                }
            });

        }
        return v;
    }

    private void getTicketId(int threadId) {
        String userFacebookId = null;
        if (!PreferenceManager.getInstance(mContext).isLoggedIn()) {
            userFacebookId = AccessToken.getCurrentAccessToken().getUserId();
        }
        SupportBody body = new SupportBody(threadId, userFacebookId);
        Call<PostTicketResponse> call = RestClient.getInstance(mContext).getSupportApi().postTicket(body);
        call.enqueue(new RestCallback<PostTicketResponse>() {
            @Override
            public void failure(RestError error) {
                if(error.getCode() != 0){
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(PostTicketResponse response) {
                openTicket(PROCESSING, response.getResult().getTicketId());
            }
        });
    }

    private void openTicket(int status, int ticketId) {
        Intent intent = new Intent(mContext, DetailMessageActivity.class);
        intent.putExtra("ticket_status", status);
        intent.putExtra("ticket_id", ticketId);
        startActivity(intent);
    }

    private void getData() {
        Call<GetSupportThreadResponse> call = RestClient.getInstance(mContext).getSupportApi().getSupport();
        call.enqueue(new RestCallback<GetSupportThreadResponse>() {
            @Override
            public void failure(RestError error) {
                if(error.getCode() != 0){
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(GetSupportThreadResponse response) {
                mListQuestion.clear();
                mListQuestion.addAll(response.getResult());
                mSendMessageAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_open_sms:
                openTicket(PROCESSING, mTicketProcessing);
                break;
            case R.id.layout_new_message:
                if (mCurrentSelected == -1) {
                    Toast.makeText(mContext, R.string.choose_thread, Toast.LENGTH_SHORT).show();
                } else {
                    getTicketId(mThreadSelected);
                }
        }
    }
}
