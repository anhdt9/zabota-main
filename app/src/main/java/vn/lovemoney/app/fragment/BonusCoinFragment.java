package vn.lovemoney.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.BonusCoinPagerAdapter;
import vn.lovemoney.app.util.FreeXuUtils;

/**
 * Created by Laptop TCC on 3/5/2017.
 */

public class BonusCoinFragment extends BaseFragment {

    public static final String TAG = BonusCoinFragment.class.getSimpleName();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private BonusCoinPagerAdapter mBonusCoinPagerAdapter;

    public BonusCoinFragment() {

    }

    public static BonusCoinFragment newInstance() {
        Bundle args = new Bundle();
        BonusCoinFragment fragment = new BonusCoinFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBonusCoinPagerAdapter = new BonusCoinPagerAdapter(getChildFragmentManager(), getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_make_coin, container, false);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);

        //Set an Apater for the View Pager
        viewPager.setAdapter(mBonusCoinPagerAdapter);
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
                boolean showCounter[] = new boolean[mBonusCoinPagerAdapter.getTabTitles().length];
                FreeXuUtils.customTab(getActivity(), mBonusCoinPagerAdapter, tabLayout, mBonusCoinPagerAdapter.getTabTitles(), showCounter);

            }
        });

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
