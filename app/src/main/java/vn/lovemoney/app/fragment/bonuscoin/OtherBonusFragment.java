package vn.lovemoney.app.fragment.bonuscoin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAdOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyUserMetadata;

import java.util.ArrayList;
import java.util.List;

import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.HomeCoinActivity;
import vn.lovemoney.app.activity.bonus.other.share.InviteFacebookFriendActivity;
import vn.lovemoney.app.activity.bonus.other.share.MiniGameListActivity;
import vn.lovemoney.app.adapter.bonus.other.OtherBonusAdapter;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.bonus.coin.other.OtherItem;
import vn.lovemoney.app.util.LanguageUtils;

/**
 * Created by Laptop TCC on 3/11/2017.
 */

public class OtherBonusFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    private static final String TAG = OtherBonusFragment.class.getSimpleName();
    private OtherBonusAdapter mAdapter;
    private List<OtherItem> list;
    private Context mContext;

    public static OtherBonusFragment newInstance() {
        Bundle args = new Bundle();
        OtherBonusFragment fragment = new OtherBonusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = new ArrayList<>();
        mAdapter = new OtherBonusAdapter(getActivity(), list);
        loadItems();
    }

    @Override
    public void onResume() {
        super.onResume();
        recordScreenName("Other Bonus");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_social, container, false);
        ListView lv = (ListView) v.findViewById(R.id.list);

        lv.setAdapter(mAdapter);
        lv.setOnItemClickListener(this);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void loadItems() {

        String[] mTitles = mContext.getResources().getStringArray(R.array.bonus_other_titles);
        TypedArray mIcons = getResources().obtainTypedArray(R.array.bonus_other_icons);

        for (int i = 0; i < mTitles.length; i++) {
            OtherItem otherItem = new OtherItem();
            otherItem.setTitle(mTitles[i]);
            otherItem.setIcon(mIcons.getDrawable(i));
            otherItem.setNextIcon(ContextCompat.getDrawable(mContext, R.drawable.icon_next_normal));
            list.add(otherItem);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                startActivity(new Intent(mContext, InviteFacebookFriendActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_left, R.anim.hold);
                break;
            case 1:
                startActivity(new Intent(mContext, MiniGameListActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_left, R.anim.hold);
                break;
            case 2:
                ((HomeCoinActivity)getActivity()).showRewardedVideo();
                break;
        }
    }
}
