package vn.lovemoney.app.fragment.navigationdrawer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.activity.CustomerSupportActivity;
import vn.lovemoney.app.activity.navigation.mail.detail.DetailMessageActivity;
import vn.lovemoney.app.adapter.navigationdrawer.mail.detail.MessageAdapter;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.BasePagingRequest;
import vn.lovemoney.app.model.support.GetTicket;
import vn.lovemoney.app.model.support.GetTicketResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.view.widget.LoadMoreListView;

/**
 * Created by 8470p on 3/14/2017.
 */

public class InboxFragment extends BaseFragment implements LoadMoreListView.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    private static final int PAGE_SIZE = 10;
    private List<GetTicket> mMailboxes;
    private LoadMoreListView mLvMails;
    private SwipeRefreshLayout mSwipeLayout;
    private MessageAdapter mAdapter;
    private BasePagingRequest mRequest;
    private Context mContext;
    private View v;
    private static final int PROCESSING = 1;

    public static InboxFragment newInstance() {
        return new InboxFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_mailbox, container, false);
        mMailboxes = new ArrayList<>();
        mRequest = new BasePagingRequest(0, PAGE_SIZE);
        mLvMails = (LoadMoreListView) v.findViewById(R.id.list);
        mSwipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mAdapter = new MessageAdapter(mContext, mMailboxes);
        mLvMails.setAdapter(mAdapter);
        mLvMails.setOnLoadMoreListener(this);
        mLvMails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GetTicket ticket = mMailboxes.get(position);
                Intent intent = new Intent(mContext, DetailMessageActivity.class);
                intent.putExtra("ticket_status", ticket.getStatus());
                intent.putExtra("ticket_id", ticket.getTicketId());
                intent.putExtra("unseen_count", ticket.getUnseenCount());
                startActivity(intent);
            }
        });
        mSwipeLayout.setOnRefreshListener(this);

        return v;
    }

    @Override
    public void onLoadMore() {
        getMails();
    }

    @Override
    public void onRefresh() {
        mMailboxes.clear();
        mRequest.reset();
        getMails();
        mAdapter.notifyDataSetChanged();
    }

    private void getMails() {
        BasePagingRequest mBasePagingRequest = new BasePagingRequest(0, 10);
        String userFacebookId = null;
        if (!PreferenceManager.getInstance(mContext).isLoggedIn()) {
            userFacebookId = AccessToken.getCurrentAccessToken().getUserId();
        }
        final Activity activity = getActivity();
        Call<GetTicketResponse> call = RestClient.getInstance(mContext).getSupportApi().getTicket(mBasePagingRequest.toQueryMap(), userFacebookId);
        call.enqueue(new RestCallback<GetTicketResponse>() {
            @Override
            public void failure(RestError error) {
                mLvMails.onLoadFailed();
                mSwipeLayout.setRefreshing(false);
                if (error.getCode() != 0) {
                    Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(GetTicketResponse response) {
                LinearLayout empty = (LinearLayout) v.findViewById(R.id.empty_mail);
                LinearLayout haveMail = (LinearLayout) v.findViewById(R.id.not_empty_mail);
                if (response.getResult().size() == 0) {
                    empty.setVisibility(View.VISIBLE);
                    haveMail.setVisibility(View.GONE);
                } else {
                    empty.setVisibility(View.GONE);
                    haveMail.setVisibility(View.VISIBLE);

                    mMailboxes.clear();
                    for (int i = 0; i < response.getResult().size(); i++) {
                        GetTicket item = response.getResult().get(i);
                        mMailboxes.add(item);
                        if (item.getStatus() == PROCESSING) {
                            PreferenceManager.getInstance(mContext).setTicketProcessing(item.getTicketId());
                        }
                    }

                    mRequest.nextPage();
                    if (response.getItemCount() <= mMailboxes.size()) {
                        mLvMails.onFinishAll();
                    } else {
                        mLvMails.onLoadMoreComplete();
                    }
                    mAdapter.notifyDataSetChanged();
                    mSwipeLayout.setRefreshing(false);
                }
                ((CustomerSupportActivity) activity).updateUnseenCounter(response.getTotalUnseenCount());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        recordScreenName("Inbox");
        getMails();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
