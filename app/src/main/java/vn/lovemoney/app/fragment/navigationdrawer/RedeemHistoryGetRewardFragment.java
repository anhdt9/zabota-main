package vn.lovemoney.app.fragment.navigationdrawer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.navigationdrawer.mail.detail.RedeemHistoryAdapter;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.BasePagingRequest;
import vn.lovemoney.app.model.exchance.RedeemExchangeHistoryItem;
import vn.lovemoney.app.model.exchance.RedeemExchangeHistoryResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.view.widget.LoadMoreListView;

/**
 * Created by 8470p on 3/14/2017.
 */

public class RedeemHistoryGetRewardFragment extends BaseFragment implements LoadMoreListView.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    private static final int PAGE_SIZE = 10;
    private List<RedeemExchangeHistoryItem> mRedeems;
    private LoadMoreListView mLvRedeems;
    private SwipeRefreshLayout mSwipeLayout;
    private RedeemHistoryAdapter mAdapter;
    private BasePagingRequest mRequest;
    private Context mContext;

    public static RedeemHistoryGetRewardFragment newInstance() {
        return new RedeemHistoryGetRewardFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_all_coin_history, container, false);

        mRedeems = new ArrayList<>();
        mRequest = new BasePagingRequest(0, PAGE_SIZE);
        mLvRedeems = (LoadMoreListView) v.findViewById(R.id.list);
        mSwipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mAdapter = new RedeemHistoryAdapter(mContext, mRedeems);
        mLvRedeems.setAdapter(mAdapter);


        mLvRedeems.setOnLoadMoreListener(this);
        mSwipeLayout.setOnRefreshListener(this);

        TextView tvCard = (TextView) v.findViewById(R.id.coin_card);
        tvCard.setText(mContext.getResources().getString(R.string.card));

        return v;
    }


    @Override
    public void onLoadMore() {
        loadRedeemHistories();
    }

    @Override
    public void onRefresh() {
        mRedeems.clear();
        mAdapter.notifyDataSetChanged();
        mRequest.reset();
        loadRedeemHistories();
    }

    private void loadRedeemHistories(){
        Call<RedeemExchangeHistoryResponse> call = RestClient.getInstance(mContext).getExchangeApi().getRedeemExchangeHistory(mRequest.toQueryMap());
        call.enqueue(new RestCallback<RedeemExchangeHistoryResponse>() {
            @Override
            public void failure(RestError error) {
                mLvRedeems.onLoadFailed();
                mSwipeLayout.setRefreshing(false);
                if(error.getCode() != 0){
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(RedeemExchangeHistoryResponse response) {
                mRedeems.addAll(response.getResult());
                mRequest.nextPage();
                if(response.getItemCount() <= mRedeems.size()){
                    mLvRedeems.onFinishAll();
                } else{
                    mLvRedeems.onLoadMoreComplete();
                }

                mAdapter.notifyDataSetChanged();
                mSwipeLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        recordScreenName("Reward History");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
    }
}
