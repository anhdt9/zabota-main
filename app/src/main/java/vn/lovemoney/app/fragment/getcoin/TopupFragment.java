package vn.lovemoney.app.fragment.getcoin;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.getcoin.RedeemAdapter;
import vn.lovemoney.app.dialog.ConfirmExchangeCardDialog;
import vn.lovemoney.app.fragment.BaseFragment;
import vn.lovemoney.app.model.exchance.RedeemItem;
import vn.lovemoney.app.model.exchance.ShowRedeemListResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.GuideUtils;
import vn.lovemoney.app.util.event.MessageEvent;
import vn.lovemoney.app.view.widget.LoadMoreListView;

/**
 * Created by Laptop TCC on 3/5/2017.
 */

public class TopupFragment extends BaseFragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, LoadMoreListView.OnLoadMoreListener {
    private LoadMoreListView mLvRedeems;
    private RedeemAdapter mAdapter;
    private List<RedeemItem> mRedeems = null;
    private SwipeRefreshLayout mSwipeLayout;
    private ViewSwitcher mViewSwitcher;
    private Context mContext;

    public static TopupFragment newInstance() {
        return new TopupFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        mRedeems = new ArrayList<>();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_topup_list, container, false);
        mSwipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mLvRedeems = (LoadMoreListView) v.findViewById(R.id.list);
        mViewSwitcher = (ViewSwitcher) v.findViewById(R.id.view_switcher);
        mAdapter = new RedeemAdapter(getActivity(), mRedeems);
        mLvRedeems.setAdapter(mAdapter);
        mLvRedeems.setOnItemClickListener(this);
        mLvRedeems.setOnLoadMoreListener(this);
        mSwipeLayout.setOnRefreshListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getRedeemList();
    }

    @Override
    public void onRefresh() {
        mRedeems.clear();
        mAdapter.notifyDataSetChanged();
        getRedeemList();
    }

    @Override
    public void onLoadMore() {
        getRedeemList();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RedeemItem redeemItem = mAdapter.getItem(position);
        ConfirmExchangeCardDialog dialog = ConfirmExchangeCardDialog.newInstance(redeemItem);
        dialog.show(getFragmentManager(), null);
    }

    private void getRedeemList() {
        Call<ShowRedeemListResponse> call = RestClient.getInstance(mContext).getExchangeApi().getRedeemList();
        call.enqueue(new RestCallback<ShowRedeemListResponse>() {
            @Override
            public void failure(RestError error) {
                mViewSwitcher.setDisplayedChild(1);
                mLvRedeems.onLoadFailed();
                mSwipeLayout.setRefreshing(false);
                if(error.getCode() != 0){
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(ShowRedeemListResponse response) {
                mViewSwitcher.setDisplayedChild(0);
                mRedeems.clear();
                mRedeems.addAll(response.getResult());
                mLvRedeems.onFinishAll();
                mAdapter.notifyDataSetChanged();
                mSwipeLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRedeems.clear();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            EventBus.getDefault().post(new MessageEvent(MessageEvent.SHOW_TOOLTIP, GuideUtils.GUIDE_REDEEM_TOPUP));
        }
    }
}
