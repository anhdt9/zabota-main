package vn.lovemoney.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.NotificationAdapter;
import vn.lovemoney.app.model.BasePagingRequest;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.other.NotificationItem;
import vn.lovemoney.app.model.other.NotificationListResponse;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.view.widget.LoadMoreListView;

/**
 * Created by Laptop TCC on 3/5/2017.
 */

public class NotificationListFragment extends BaseFragment implements LoadMoreListView.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener {


    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private DrawerFragment.FragmentDrawerListener mDrawerListener;

    Context mContext;
    private static final int PAGE_SIZE = 20;
    private SwipeRefreshLayout mSwipeLayout;
    private LoadMoreListView mLvNotifications;
    private List<NotificationItem> mNotifications;
    private NotificationAdapter mAdapter;
    private BasePagingRequest mRequest;
    private ImageView mBtnBack;

    public NotificationListFragment() {

    }

    public void setDrawerListener(DrawerFragment.FragmentDrawerListener listener) {
        this.mDrawerListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_notification_list, container, false);

        mBtnBack = (ImageView) layout.findViewById(android.R.id.home);
        initControl();
        mNotifications = new ArrayList<>();
        mSwipeLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipe_container);
        mLvNotifications = (LoadMoreListView) layout.findViewById(R.id.list);
        mAdapter = new NotificationAdapter(getActivity(), mNotifications);
        mLvNotifications.setAdapter(mAdapter);
        mLvNotifications.setOnLoadMoreListener(this);
        mSwipeLayout.setOnRefreshListener(this);
        mRequest = new BasePagingRequest(0, PAGE_SIZE);
        mLvNotifications.setOnItemClickListener(this);
        PreferenceManager.getInstance(getActivity()).clearUnreadNotificationCount();

        return layout;
    }

    private void initControl() {
        if (mBtnBack != null) {
            mBtnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeDrawer();
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
                    int unreadNotificationCount = PreferenceManager.getInstance(mContext).getUnreadNotificationCount();
                    if (unreadNotificationCount > 0) {
                        PreferenceManager.getInstance(getActivity()).clearUnreadNotificationCount();
                        onRefresh();
                    }
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
                // mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                if (newState == DrawerLayout.STATE_DRAGGING) {
                }
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    public void openDrawer() {
        //mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerLayout.openDrawer(GravityCompat.END);
        int unreadNotificationCount = PreferenceManager.getInstance(mContext).getUnreadNotificationCount();
        if (unreadNotificationCount > 0) {
            markSeenStatusNotification();
            PreferenceManager.getInstance(getActivity()).clearUnreadNotificationCount();
            onRefresh();
        }
    }

    public void closeDrawer() {
        //  mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mDrawerLayout.closeDrawer(GravityCompat.END);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        final NotificationItem notification = mNotifications.get(position);
        AlertDialog dlg = new AlertDialog.Builder(mContext)
                .setTitle(R.string.title_detail)
                .setMessage(notification.getMessage())
                .setPositiveButton(R.string.ok, null)
                .create();
        dlg.show();
    }

    @Override
    public void onRefresh() {
        mNotifications.clear();
        mAdapter.notifyDataSetChanged();
        mRequest.reset();
        loadNotifications();
    }

    @Override
    public void onLoadMore() {
        loadNotifications();
    }

    private void loadNotifications() {
        Call<NotificationListResponse> call = RestClient.getInstance(mContext).getOtherApi().getListNotification(mRequest.toQueryMap());
        call.enqueue(new RestCallback<NotificationListResponse>() {
            @Override
            public void failure(RestError error) {
                mLvNotifications.onLoadFailed();
                if (mSwipeLayout.isRefreshing()) {
                    mSwipeLayout.setRefreshing(false);
                }
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(NotificationListResponse response) {
//                mNotifications.clear();
                mNotifications.addAll(response.getResult());
                mRequest.nextPage();
                if (response.getItemCount() <= mNotifications.size()) {
                    mLvNotifications.onFinishAll();
                } else {
                    mLvNotifications.onLoadMoreComplete();
                }
                mAdapter.notifyDataSetChanged();
                if (mSwipeLayout.isRefreshing()) {
                    mSwipeLayout.setRefreshing(false);
                }
            }
        });
    }

    private void markSeenStatusNotification() {
        Call<BaseResponse> call = RestClient.getInstance(mContext).getOtherApi().postStatusNotification();
        call.enqueue(new RestCallback<BaseResponse>() {
            @Override
            public void failure(RestError error) {
                if (error.getCode() != 0) {
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(BaseResponse response) {
            }
        });
    }
}
