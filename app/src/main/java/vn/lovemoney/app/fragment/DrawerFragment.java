package vn.lovemoney.app.fragment;

/*
 * Created by Ravi on 29/07/15.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import vn.lovemoney.app.BuildConfig;
import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.NavigationDrawerAdapter;
import vn.lovemoney.app.model.NavDrawerItem;
import vn.lovemoney.app.model.user.UserInfo;
import vn.lovemoney.app.util.ActivityNavigator;
import vn.lovemoney.app.util.GuideUtils;
import vn.lovemoney.app.util.PreferenceManager;

public class DrawerFragment extends BaseFragment implements AdapterView.OnItemClickListener, SharedPreferences.OnSharedPreferenceChangeListener, View.OnClickListener {

    private ListView mListView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerAdapter mAdapter;
    private List<NavDrawerItem> mItems;
    private FragmentDrawerListener mDrawerListener;
    private View mHeader;
    private ImageView mIvVerified;
    private TextView mTxtLevel;
    private boolean mNeedRefreshDrawer;
    private Context mContext;
    private View layout;
    private ViewGroup rootView;

    public DrawerFragment() {

    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.mDrawerListener = listener;
    }

    private void updateDrawerItems() {
        boolean isVerified = false;
        boolean isEnteredReferral = false;

        UserInfo userInfo = PreferenceManager.getInstance(getActivity()).getUserInfo();
        if (userInfo != null) {
            isVerified = userInfo.isVerified();
            isEnteredReferral = userInfo.isEnteredReferral();
        }

        String[] mTitles;
        TypedArray mIcons;
        if (isVerified && isEnteredReferral) {
            mTitles = getResources().getStringArray(R.array.nav_drawer_labels_new_verified_enteredReferral);
            mIcons = getResources().obtainTypedArray(R.array.nav_drawer_imgs_new_verified_enteredReferral);
        } else if (isVerified) {
            mTitles = getResources().getStringArray(R.array.nav_drawer_labels_new_verified);
            mIcons = getResources().obtainTypedArray(R.array.nav_drawer_imgs_new_verified);
        } else {
            mTitles = getResources().getStringArray(R.array.nav_drawer_labels_new);
            mIcons = getResources().obtainTypedArray(R.array.nav_drawer_imgs_new);
        }

        mItems.clear();
        for (int i = 0; i < mTitles.length; i++) {
            NavDrawerItem navItem = new NavDrawerItem();
            navItem.setTitle(mTitles[i]);
            navItem.setImg(mIcons.getDrawable(i));
            mItems.add(navItem);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mItems = new ArrayList<>();
        PreferenceManager.getInstance(getActivity()).registerListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mListView = (ListView) layout.findViewById(R.id.list);
        mHeader = inflater.inflate(R.layout.listitem_drawer_header_new, container, false);
        Button btnEnterReferralCode = (Button) mHeader.findViewById(R.id.btn_share_code);
        mIvVerified = (ImageView) mHeader.findViewById(R.id.verified);
        mTxtLevel = (TextView) mHeader.findViewById(R.id.tv_level);
        rootView = (ViewGroup) layout.findViewById(R.id.rootView);

        btnEnterReferralCode.setOnClickListener(this);

        View mFooter = inflater.inflate(R.layout.listitem_drawer_footer, container, false);
        TextView txtVersion = (TextView) mFooter.findViewById(R.id.version);
        mAdapter = new NavigationDrawerAdapter(getActivity(), mItems);
        mListView.addHeaderView(mHeader, null, false);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        mIvVerified.setOnClickListener(this);
        txtVersion.setText(BuildConfig.VERSION_NAME);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                updateHeaderDisplay();
            }
        }, 500);
        updateDrawerItems();
        return layout;
    }

    @Override
    public void onDestroy() {
        PreferenceManager.getInstance(getActivity()).unregisterListener(this);
        super.onDestroy();
    }

    public void updateHeaderDisplay() {
        ImageView ivAvatar = (ImageView) mHeader.findViewById(R.id.avatar);
        TextView txtName = (TextView) mHeader.findViewById(R.id.name);
        TextView txtBalance = (TextView) mHeader.findViewById(R.id.balance);
        TextView txtEarnedMoney = (TextView) mHeader.findViewById(R.id.earned_money);
        TextView txtReferralCode = (TextView) mHeader.findViewById(R.id.referral_code);
        UserInfo userInfo = PreferenceManager.getInstance(getActivity()).getUserInfo();

        ImageView btnBack = (ImageView) mHeader.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
            }
        });
        if (userInfo != null) {
            Picasso.with(getActivity())
                    .load(userInfo.getAvatarUrl())
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_image_empty)
                    .error(R.drawable.ic_image_empty)
                    .into(ivAvatar);
            txtName.setText(userInfo.getName());
            txtBalance.setText(String.valueOf(userInfo.getBalance()));
            txtEarnedMoney.setText(String.valueOf(userInfo.getEarning()));
            txtReferralCode.setText(mContext.getResources().getString(R.string.referral_code, userInfo.getReferralCode()));
            mIvVerified.setSelected(userInfo.isVerified());
            mTxtLevel.setText(mContext.getResources().getString(R.string.rank, userInfo.getRank()));
        }
    }


    public void setUp(DrawerLayout drawerLayout, final Toolbar toolbar) {
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
                GuideUtils.requestShowToolTip(GuideUtils.GUIDE_SHARE_CODE, mContext, rootView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
                if (mNeedRefreshDrawer) {
                    updateDrawerItems();
                    mNeedRefreshDrawer = false;
                }
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    public void toggle() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        toggle();
        if (mDrawerListener != null) {
            mDrawerListener.onDrawerItemSelected(view, position - mListView.getHeaderViewsCount());
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(PreferenceManager.USER_INFO)) {
            updateHeaderDisplay();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_share_code:
                String description = mContext.getResources().getString(R.string.description_share_code, PreferenceManager.getInstance(getActivity()).getUserCredential().getReferralCode());
                String content = String.format("%s\n%s", description, "https://zabota.vn");
                ActivityNavigator.startSharingCode(getContext(), content);
                break;
            case R.id.verified:
                Activity activity = getActivity();
                if (activity != null) {
                    UserInfo userInfo = PreferenceManager.getInstance(getActivity()).getUserInfo();
                    if (userInfo != null && !userInfo.isVerified()) {
                        ActivityNavigator.startActivateActivity(activity);
                    }
                }
                break;
        }
    }

    public interface FragmentDrawerListener {
        void onDrawerItemSelected(View view, int position);
    }

    public void setNeedRefreshDrawer(boolean mNeedRefreshDrawer) {
        this.mNeedRefreshDrawer = mNeedRefreshDrawer;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
