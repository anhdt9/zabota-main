package vn.lovemoney.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import vn.lovemoney.app.R;
import vn.lovemoney.app.adapter.TopChartAdapter;
import vn.lovemoney.app.model.user.RankResponse;
import vn.lovemoney.app.model.user.ScriptUserInfo;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.util.Singleton;
import vn.lovemoney.app.util.event.MessageEvent;
import vn.lovemoney.app.view.widget.LoadMoreListView;

/**
 * Created by tuandigital on 8/25/16.
 */
public class TopChartFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, LoadMoreListView.OnLoadMoreListener {


    private static final String CHART_NAME = "chart_name";
    private TopChartAdapter mAdapter;
    private List<ScriptUserInfo> mItems;
    private String mChartName;
    private SwipeRefreshLayout mSwipeContainer;
    private ViewSwitcher mViewSwitcher;
    private LoadMoreListView mLvTopItems;
    private Context mContext;

    public static TopChartFragment newInstance(String chartName) {
        Bundle args = new Bundle();
        args.putString(CHART_NAME, chartName);
        TopChartFragment fragment = new TopChartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mChartName = getArguments().getString(CHART_NAME, "daily");
        mItems = new ArrayList<>();
        mAdapter = new TopChartAdapter(getActivity(), mItems);
        loadItems();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_top_chart, container, false);
        mLvTopItems = (LoadMoreListView) v.findViewById(R.id.list);
        mViewSwitcher = (ViewSwitcher) v.findViewById(R.id.view_switcher);
        mLvTopItems.setAdapter(mAdapter);
        mLvTopItems.setOnLoadMoreListener(this);
        mSwipeContainer = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mSwipeContainer.setOnRefreshListener(this);
        return v;
    }

    private void loadItems() {
        Call<RankResponse> call = RestClient.getInstance(getActivity()).getUserApi().getRank(mChartName);
        call.enqueue(new RestCallback<RankResponse>() {
            @Override
            public void failure(RestError error) {
                mSwipeContainer.setRefreshing(false);
                mViewSwitcher.setDisplayedChild(1);
                mLvTopItems.onLoadFailed();
                if(error.getCode() != 0){
                    Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void success(RankResponse response) {
                mViewSwitcher.setDisplayedChild(0);
                mItems.clear();
                mItems.addAll(Arrays.asList(response.getResult()));

                mLvTopItems.onLoadMoreComplete();
                mAdapter.notifyDataSetChanged();
                mSwipeContainer.setRefreshing(false);

                if (mChartName.equals("monthly")) {
                    for (ScriptUserInfo userInfo  : mItems) {
                        if (!userInfo.isMe())
                            continue;

                        break;
                    }
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        loadItems();
    }

    @Override
    public void onLoadMore() {
        loadItems();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }
}
