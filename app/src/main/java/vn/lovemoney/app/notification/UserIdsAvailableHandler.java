package vn.lovemoney.app.notification;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.onesignal.OneSignal;

import retrofit2.Call;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.OneSignalInfo;
import vn.lovemoney.app.model.body.OneSignalIdBody;
import vn.lovemoney.app.network.RestCallback;
import vn.lovemoney.app.network.RestClient;
import vn.lovemoney.app.network.RestError;
import vn.lovemoney.app.util.PreferenceManager;

/**
 * Created by tuannguyen on 1/8/16.
 */
public class UserIdsAvailableHandler implements OneSignal.IdsAvailableHandler {

	private Context mContext;
	private static final String TAG = UserIdsAvailableHandler.class.getSimpleName();

	public UserIdsAvailableHandler(Context context){
		mContext = context;
	}

	@Override
	public void idsAvailable(String userId, String registrationId) {
		Log.d(TAG, "onesignal id is available");
		final PreferenceManager pm = PreferenceManager.getInstance(mContext);
		OneSignalInfo info = new OneSignalInfo(userId, registrationId);
		pm.saveOneSignalInfo(info);
		OneSignalIdBody oneSignalId = new OneSignalIdBody(userId);
		if(!pm.isSavedOneSignalInfo() && pm.isLoggedIn()) {
			Call<BaseResponse> call = RestClient.getInstance(mContext).getUserApi().savePushInfo(oneSignalId);
			call.enqueue(new RestCallback<BaseResponse>() {
				@Override
				public void failure(RestError error) {
					Log.d(TAG, "Cannot save push notification info");
					if(error.getCode() != 0){
						Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
					}
				}

				@Override
				public void success(BaseResponse response) {
					pm.setSavedOneSignalInfo(true);
				}
			});
		}
	}
}
