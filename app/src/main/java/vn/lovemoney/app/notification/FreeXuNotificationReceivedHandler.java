package vn.lovemoney.app.notification;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.greenrobot.eventbus.EventBus;

import vn.lovemoney.app.util.event.MessageEvent;

/**
 * Created by DTA on 25/05/2017.
 */

public class FreeXuNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

    @Override
    public void notificationReceived(OSNotification notification) {

        EventBus.getDefault().post(new MessageEvent(MessageEvent.SHOW_NOTIFICATION, notification.payload.additionalData, notification.payload.body));

    }
}
