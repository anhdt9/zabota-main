package vn.lovemoney.app.network.api;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.body.PaypalBody;
import vn.lovemoney.app.model.exchance.RedeemExchange;
import vn.lovemoney.app.model.exchance.RedeemExchangeHistoryResponse;
import vn.lovemoney.app.model.exchance.ShowPaypalListResponse;
import vn.lovemoney.app.model.exchance.ShowRedeemListResponse;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public interface ExchangeApi {

    @GET("/v1/redeems")
    Call<ShowRedeemListResponse> getRedeemList();

    @POST("/v1/redeems/exchange/{id}")
    Call<RedeemExchange> getRedeemExchange(@Path("id") int id);

    @POST("/v1/redeems/exchange/{id}")
    Call<BaseResponse> getPaypalExchange(@Path("id") int id, @Body PaypalBody paypalBody);

    @GET("/v1/redeems/exchange")
    Call<RedeemExchangeHistoryResponse> getRedeemExchangeHistory(@QueryMap Map<String, String> queries);

    @GET("/v1/redeems/paypal")
    Call<ShowPaypalListResponse> getPaypalList();

}
