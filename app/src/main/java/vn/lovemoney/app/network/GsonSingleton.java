package vn.lovemoney.app.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Calendar;

/**
 * Created by tuannguyen on 12/9/15.
 */
public class GsonSingleton {
	private static volatile GsonSingleton sInstance;
	private Gson mGson;

	public static GsonSingleton getInstance() {
		if (sInstance == null) {
			synchronized (GsonSingleton.class) {
				if (sInstance == null) {
					sInstance = new GsonSingleton();
				}
			}
		}
		return sInstance;
	}

	private GsonSingleton() {
		mGson = new GsonBuilder()
				.registerTypeHierarchyAdapter(Calendar.class, new CalendarTypeAdapter().nullSafe())
				.registerTypeAdapter(boolean.class, new BooleanTypeAdapter().nullSafe())
				.registerTypeHierarchyAdapter(Boolean.class, new BooleanTypeAdapter().nullSafe())
				.create();
	}

	public Gson getGson() {
		return mGson;
	}

	public String toJson(Object src) {
		return mGson.toJson(src);
	}

	public <T> T fromJson(String json, Class<T> clazz) {
		T ret = null;
		try {
			ret = mGson.fromJson(json, clazz);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
