package vn.lovemoney.app.network;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by tuannguyen on 10/5/15.
 */
class CalendarTypeAdapter extends TypeAdapter<Calendar> {

	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	static {
		DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("Asia/Ho_Chi_Minh"));
	}

	@Override
	public void write(JsonWriter out, Calendar value) throws IOException {
		out.value(DATE_FORMATTER.format(value.getTime()));
	}

	@Override
	public Calendar read(JsonReader in) throws IOException {
		if (in.peek() == JsonToken.NULL) {
			in.nextNull();
			return null;
		}
		String dateStr = in.nextString();
		Calendar cal = null;
		try {
			cal = Calendar.getInstance();
			cal.setTime(DATE_FORMATTER.parse(dateStr));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return cal;
	}
}
