package vn.lovemoney.app.network.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import vn.lovemoney.app.network.ProgressResponseBody;

/**
 * Created by tuannguyen on 2/26/16.
 */
public class ProgressInterceptor implements Interceptor {

	private ProgressResponseBody.ProgressListener mProgressListener;

	public ProgressInterceptor(ProgressResponseBody.ProgressListener progressListener) {
		this.mProgressListener = progressListener;
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		Response originalResponse = chain.proceed(chain.request());
		return originalResponse.newBuilder()
				.body(new ProgressResponseBody(originalResponse.body(), mProgressListener))
				.build();
	}
}
