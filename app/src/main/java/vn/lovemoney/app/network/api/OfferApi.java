package vn.lovemoney.app.network.api;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.android.app.OfferListResponse;
import vn.lovemoney.app.model.android.app.ReOpenResponse;
import vn.lovemoney.app.model.android.app.RequestInstallResponse;
import vn.lovemoney.app.model.android.app.ReviewResponse;
import vn.lovemoney.app.model.body.RequestAppBody;

/**
 * Created by 8470p on 4/1/2017.
 */

public interface OfferApi {

    @GET("/v1/android/offers/apps")
    Call<OfferListResponse> getOffers(@QueryMap Map<String, String> queries);

    @POST("/v1/android/offers/apps/request-install")
    Call<RequestInstallResponse> requestInstallApp(@Body RequestAppBody body);

    @POST("/v1/android/offers/apps/install")
    Call<BaseResponse> installApp(@Body RequestAppBody body);

    @POST("/v1/android/offers/apps/verify")
    Call<BaseResponse> verifyApp(@Body RequestAppBody body);

    @POST("/v1/android/offers/apps/review")
    Call<ReviewResponse> reviewApp(@Body RequestAppBody body);

    @GET("/v1/android/offers/apps/re-open")
    Call<ReOpenResponse> getReOpenApp(@QueryMap Map<String, String> queries);

    @POST("/v1/android/offers/apps/re-open")
    Call<BaseResponse> reOpenApp(@Body RequestAppBody body);

}
