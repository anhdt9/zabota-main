package vn.lovemoney.app.network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Calendar;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import vn.lovemoney.app.BuildConfig;
import vn.lovemoney.app.network.api.ExchangeApi;
import vn.lovemoney.app.network.api.FacebookApi;
import vn.lovemoney.app.network.api.GameApi;
import vn.lovemoney.app.network.api.OfferApi;
import vn.lovemoney.app.network.api.OtherApi;
import vn.lovemoney.app.network.api.SupportApi;
import vn.lovemoney.app.network.api.UserApi;
import vn.lovemoney.app.network.interceptor.HeaderInterceptor;
import vn.lovemoney.app.network.interceptor.LoggingInterceptor;
import vn.lovemoney.app.network.interceptor.ProgressInterceptor;

/**
 * Created by tuannguyen on 9/29/15.
 */
public class RestClient implements ProgressResponseBody.ProgressListener {

    private static final String BASE_URL = "https://api.zabota.vn";
    private static volatile RestClient sInstance;
    private Retrofit mRetrofit;
    private UserApi mUserApi;
    private OfferApi mOfferApi;
    private OtherApi mOtherApi;
    private FacebookApi mFacebookApi;
    private ExchangeApi mExchangeApi;
    private ProgressResponseBody.ProgressListener mProgressListener;
    private SupportApi mSupportApi;
    private GameApi mGameApi;

    public static RestClient getInstance(Context context) {
        if (sInstance == null) {
            synchronized (RestClient.class) {
                if (sInstance == null) {
                    sInstance = new RestClient(context);
                }
            }
        }
        return sInstance;
    }

    private RestClient(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new HeaderInterceptor(context));
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(new LoggingInterceptor());
        }
        Gson gson = new GsonBuilder()
                .registerTypeHierarchyAdapter(Calendar.class, new CalendarTypeAdapter().nullSafe())
                .registerTypeAdapter(boolean.class, new BooleanTypeAdapter().nullSafe())
                .registerTypeHierarchyAdapter(Boolean.class, new BooleanTypeAdapter().nullSafe())
                .create();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(builder.build())
                .build();
        OkHttpClient.Builder downloadBuilder = new OkHttpClient.Builder()
                .addInterceptor(new ProgressInterceptor(this));
    }

    public OfferApi getOfferApi() {
        if (mOfferApi == null) {
            mOfferApi = mRetrofit.create(OfferApi.class);
        }
        return mOfferApi;
    }

    public ExchangeApi getExchangeApi() {
        if (mExchangeApi == null) {
            mExchangeApi = mRetrofit.create(ExchangeApi.class);
        }
        return mExchangeApi;
    }

    public UserApi getUserApi() {
        if (mUserApi == null) {
            mUserApi = mRetrofit.create(UserApi.class);
        }
        return mUserApi;
    }

    public OtherApi getOtherApi() {
        if (mOtherApi == null) {
            mOtherApi = mRetrofit.create(OtherApi.class);
        }
        return mOtherApi;
    }

    public FacebookApi getFacebookApi() {
        if (mFacebookApi == null) {
            mFacebookApi = mRetrofit.create(FacebookApi.class);
        }
        return mFacebookApi;
    }

    public SupportApi getSupportApi() {
        if (mSupportApi == null) {
            mSupportApi = mRetrofit.create(SupportApi.class);
        }
        return mSupportApi;
    }

    public GameApi getGameApi() {
        if (mGameApi == null) {
            mGameApi = mRetrofit.create(GameApi.class);
        }
        return mGameApi;
    }

    @Override
    public void update(long bytesRead, long contentLength, boolean done) {
        if (mProgressListener != null) {
            mProgressListener.update(bytesRead, contentLength, done);
        }
    }
}
