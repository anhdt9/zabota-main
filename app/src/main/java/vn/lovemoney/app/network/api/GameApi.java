package vn.lovemoney.app.network.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.game.GameMiniResponse;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public interface GameApi {

    @GET("/v1/gameshows/wheel/request")
    Call<GameMiniResponse> getGameMiniInfo();

    @POST("/v1/gameshows/wheel/verify")
    Call<BaseResponse> verifyGameShow();

}
