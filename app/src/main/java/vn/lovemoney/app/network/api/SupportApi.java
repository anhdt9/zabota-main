package vn.lovemoney.app.network.api;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.body.SupportBody;
import vn.lovemoney.app.model.support.GetFAQsResponse;
import vn.lovemoney.app.model.support.GetMessageResponse;
import vn.lovemoney.app.model.support.GetSupportThreadResponse;
import vn.lovemoney.app.model.support.GetTicketResponse;
import vn.lovemoney.app.model.support.PostTicketResponse;

/**
 * Created by dta on 30/04/2017.
 */

public interface SupportApi {

    @GET("/v1/supports/threads")
    Call<GetSupportThreadResponse> getSupport();

    @POST("/v1/supports/tickets")
    Call<PostTicketResponse> postTicket(@Body SupportBody body);

    @GET("/v1/supports/tickets")
    Call<GetTicketResponse> getTicket(@QueryMap Map<String, String> queries, @Query("userFacebookId") String userFacebookId);

    @POST("/v1/supports/tickets/{id}/seen")
    Call<BaseResponse> markSeenMessage(@Path("id") int id);

    @GET("/v1/supports/tickets/{id}/messages")
    Call<GetMessageResponse> getMessage(@Path("id") int id, @QueryMap Map<String, String> queries, @Query("userFacebookId") String userFacebookId);

    @POST("/v1/supports/tickets/{id}/messages")
    Call<BaseResponse> postMessage(@Path("id") int id, @Body SupportBody body);

    @GET("/v1/commons/faq")
    Call<GetFAQsResponse> getFAQs();

}
