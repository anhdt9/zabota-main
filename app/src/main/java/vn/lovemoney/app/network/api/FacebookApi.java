package vn.lovemoney.app.network.api;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import vn.lovemoney.app.model.FacebookAppRequest;
import vn.lovemoney.app.model.IntegerResponse;
import vn.lovemoney.app.model.body.RequestAppBody;
import vn.lovemoney.app.model.facebook.GetFacebookOfferResponse;
import vn.lovemoney.app.model.facebook.PostFacebookOfferResponse;
import vn.lovemoney.app.model.facebook.PutFacebookBody;
import vn.lovemoney.app.model.facebook.PutFacebookOffersResponse;

/**
 * Created by Laptop TCC on 4/1/2017.
 */

public interface FacebookApi {
    @GET("/v1/offers/facebook/{action}")
    Call<GetFacebookOfferResponse> getFacebookOffers(@Path("action") int action, @QueryMap Map<String, String> queries);

    @POST("/v1/offers/facebook/{action}")
    Call<PostFacebookOfferResponse> postFacebookOffers(@Path("action") int action, @Body RequestAppBody requestBody);

    @PUT("/v1/offers/facebook/{action}")
    Call<PutFacebookOffersResponse> putFacebookOffers(@Path("action") int action, @Body PutFacebookBody requestBody);

    @POST("/v1/commons/response-fb-app-request")
    Call<IntegerResponse> submitFacebookAppRequest(@Body FacebookAppRequest appRequest);

}
