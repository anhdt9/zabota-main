package vn.lovemoney.app.network;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Created by tuannguyen on 2/3/16.
 */
class BooleanTypeAdapter extends TypeAdapter<Boolean> {
	@Override
	public void write(JsonWriter out, Boolean value) throws IOException {
		if (value == null) {
			out.nullValue();
		} else {
			out.value(value ? 1 : 0);
		}
	}

	@Override
	public Boolean read(JsonReader in) throws IOException {
		JsonToken peek = in.peek();
		switch (peek) {
			case BOOLEAN:
				return in.nextBoolean();
			case NULL:
				in.nextNull();
				return null;
			case NUMBER:
				return in.nextInt() != 0;
			case STRING:
				return in.nextString().equalsIgnoreCase("1");
			default:
				throw new IllegalStateException(
						"Expected BOOLEAN or NUMBER but was " + peek);
		}
	}
}
