package vn.lovemoney.app.network;

import com.google.gson.JsonObject;

/**
 * Created by tuannguyen on 12/14/15.
 */
public class RestError {
    public static final int SUCCESS = 0;
    private static final int EXCEPTION = 999;
    public static final int TOKEN_EXPIRED = 9;
    public static final int NEED_CHANGE_DEVICE = 10;
    private String message;
    private JsonObject result;
    private int code = EXCEPTION;

    public RestError(String message) {
        this.message = message;
    }

    public RestError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public JsonObject getResult() {
        return result;
    }

    public void setResult(JsonObject result) {
        this.result = result;
    }
}
