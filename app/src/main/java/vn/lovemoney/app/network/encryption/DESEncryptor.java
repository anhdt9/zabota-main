package vn.lovemoney.app.network.encryption;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by tuannguyen on 12/24/15.
 */
public class DESEncryptor {

	private static volatile DESEncryptor sInstance;

	private Cipher mCipher;

	public static DESEncryptor getInstance() {
		if (sInstance == null) {
			synchronized (DESEncryptor.class) {
				if (sInstance == null) {
					sInstance = new DESEncryptor();
				}
			}
		}
		return sInstance;
	}

	private DESEncryptor() {
		try {
			String md5Key = getMD5("&5h@$6'*").substring(0, 24);
			byte[] key = md5Key.getBytes();
			SecretKeySpec spec = new SecretKeySpec(key, "DESede");
			mCipher = Cipher.getInstance("DESede");
			mCipher.init(Cipher.ENCRYPT_MODE, spec);
			int a = mCipher.getBlockSize();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
	}

	public String encrypt(String source) {
		byte[] utf8;
		try {
			utf8 = source.getBytes("UTF8");
			byte[] enc = mCipher.doFinal(utf8);
			return Base64.encodeToString(enc, Base64.NO_WRAP);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getMD5(String source) {
		byte[] defaultBytes = source.getBytes();
		try {
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(defaultBytes);
			byte messageDigest[] = algorithm.digest();
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}
			return hexString.toString();
		} catch (NoSuchAlgorithmException nsae) {
			return null;
		}
	}


}
