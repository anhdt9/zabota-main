package vn.lovemoney.app.network;

import android.content.Context;
import android.widget.Toast;

import com.facebook.login.LoginManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.lovemoney.app.FreeXuApplication;
import vn.lovemoney.app.R;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.util.ActivityNavigator;
import vn.lovemoney.app.util.AppInfoManager;
import vn.lovemoney.app.util.PreferenceManager;

/**
 * Created by tuannguyen on 12/14/15.
 */
public abstract class RestCallback<T extends BaseResponse> implements Callback<T> {
    public abstract void failure(RestError error);

    public abstract void success(T response);

    private Context mContext = FreeXuApplication.getInstance();

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        String msg = mContext.getResources().getString(R.string.check_network_again);
        RestError error = new RestError(msg);
        failure(error);
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        T result = response.body();
        if (!response.isSuccessful()) {
            RestError error = new RestError(response.code(), mContext.getResources().getString(R.string.server_error));
            if (response.code() == RestError.TOKEN_EXPIRED) {
                logout();
            } else {
                failure(error);
            }
        } else if (result.getError() != RestError.SUCCESS) {
            RestError error = new RestError(result.getError(), result.getDisplayMessage());
            if (result.getError() == RestError.TOKEN_EXPIRED) {
                logout();
            } else {
                failure(error);
            }
        } else {
            success(result);
        }
    }

    private void logout() {
        PreferenceManager.getInstance(mContext).setSavedOneSignalInfo(false);
        LoginManager.getInstance().logOut();
        PreferenceManager.getInstance(mContext).logout();
        ActivityNavigator.startLoginActivity(mContext);
        AppInfoManager.getInstance(mContext).clear();
        Toast.makeText(mContext, mContext.getString(R.string.needed_login_again), Toast.LENGTH_LONG).show();
    }

}
