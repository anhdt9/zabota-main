package vn.lovemoney.app.network.api;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.CoinHistoryListResponse;
import vn.lovemoney.app.model.EventListResponse;
import vn.lovemoney.app.model.ReferralResponse;
import vn.lovemoney.app.model.body.FbIdBody;
import vn.lovemoney.app.model.body.OneSignalIdBody;
import vn.lovemoney.app.model.body.PhoneBody;
import vn.lovemoney.app.model.body.PhoneVerifyBody;
import vn.lovemoney.app.model.body.ReferralCodeBody;
import vn.lovemoney.app.model.bonus.EventAnswerBody;
import vn.lovemoney.app.model.user.ChangeDeviceResponse;
import vn.lovemoney.app.model.user.LoginResponse;
import vn.lovemoney.app.model.user.LoginRqBody;
import vn.lovemoney.app.model.user.ProfileResponse;
import vn.lovemoney.app.model.user.RankResponse;

/**
 * Created by 8470p on 4/1/2017.
 */

public interface UserApi {
    @POST("/v1/users/login")
    Call<LoginResponse> login(@Body LoginRqBody token);

    @POST("/v1/users/otp")
    Call<BaseResponse> sendOtp(@Body PhoneBody phoneBody);

    @POST("/v1/users/verify")
    Call<BaseResponse> verify(@Body PhoneVerifyBody body);

    @POST("/v1/users/otp/change-device")
    Call<BaseResponse> sendOtpChangeDevice(@Body FbIdBody body);

    @POST("/v1/users/verify/change-device")
    Call<ChangeDeviceResponse> verifyChangeDevice(@Body PhoneVerifyBody body);

    @GET("/v1/users/profile")
    Call<ProfileResponse> getProfile();

    @POST("/v1/users/referral")
    Call<ReferralResponse> referral(@Body ReferralCodeBody body);

    @POST("/v1/users/save-push-info")
    Call<BaseResponse> savePushInfo(@Body OneSignalIdBody body);

    @GET("/v1/top/{sortBy}")
    Call<RankResponse> getRank(@Path("sortBy") String sortBy);

    @GET("/v1/users/transactions")
    Call<CoinHistoryListResponse> getCoinHistories(@QueryMap Map<String, String> queries);

    @GET("/v1/events")
    Call<EventListResponse> getEvents();

    @POST("/v1/events")
    Call<BaseResponse> postEvents(@Body EventAnswerBody body);
}
