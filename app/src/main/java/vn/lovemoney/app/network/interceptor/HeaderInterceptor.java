package vn.lovemoney.app.network.interceptor;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import vn.lovemoney.app.BuildConfig;
import vn.lovemoney.app.network.encryption.DESEncryptor;
import vn.lovemoney.app.util.FreeXuUtils;
import vn.lovemoney.app.util.PreferenceManager;
import vn.lovemoney.app.z.T;

/**
 * Created by tuannguyen on 12/24/15.
 */
public class HeaderInterceptor implements Interceptor {

	public static final String TAG = HeaderInterceptor.class.getSimpleName();

	private Context mContext;

	public HeaderInterceptor(Context context) {
		mContext = context;
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		PreferenceManager pm = PreferenceManager.getInstance(mContext.getApplicationContext());
		String tokenKey = pm.getToken();
		String userId = pm.getId();

		HttpUrl url = chain.request().url();
		StringBuilder sb = new StringBuilder(url.encodedPath());
		String query = url.query();
		if (!TextUtils.isEmpty(query)) {
			sb.append(query);
		}
		RequestBody body = chain.request().body();
		if (body != null) {
			Buffer sink = new Buffer();
			body.writeTo(sink);
			sb.append(sink.readUtf8());
		}
		String deviceKey = FreeXuUtils.getDeviceId(mContext);
		sb.append(deviceKey);
		long timestamp = Calendar.getInstance().getTime().getTime();
		String secretKey = generateSecretKey(sb.toString(), timestamp);
		Request.Builder builder = chain.request().newBuilder();
		if (!TextUtils.isEmpty(userId)) {
			builder.addHeader("a", userId);
		}
		if (!TextUtils.isEmpty(tokenKey)) {
			builder.addHeader("b", tokenKey);
		}
		builder.addHeader("d", deviceKey);
		builder.addHeader("c", secretKey);
		builder.addHeader("v", String.valueOf(BuildConfig.VERSION_CODE));
		builder.addHeader("e", Base64.encodeToString(FreeXuUtils.getDeviceInfo(mContext).getBytes(), Base64.NO_WRAP));

		builder.addHeader("l", pm.getLanguage());
		//android 1:
		builder.addHeader("p", "1");

		builder.addHeader("t", String.valueOf(timestamp));

		Request request = builder.build();
		DESEncryptor.getInstance();
		return chain.proceed(request);
	}

	private String generateSecretKey(String requestDesc, long timestamp) {
		String extra = T.perform(mContext, timestamp);
		String builder = requestDesc + extra;
		return getHash(builder);
	}

	private String getHash(String src) {
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			digest.reset();
			return bin2hex(digest.digest(src.getBytes()));
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}

	private static String bin2hex(byte[] data) {
		return String.format("%0" + (data.length * 2) + "x", new BigInteger(1, data));
	}

}
