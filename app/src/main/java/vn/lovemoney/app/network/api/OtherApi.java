package vn.lovemoney.app.network.api;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.QueryMap;
import vn.lovemoney.app.model.BaseResponse;
import vn.lovemoney.app.model.LocalPushListResponse;
import vn.lovemoney.app.model.body.NotificationIdBody;
import vn.lovemoney.app.model.other.NotificationListResponse;
import vn.lovemoney.app.model.other.RemoteConfigResponse;

/**
 * Created by tuannguyen on 12/17/15.
 */
public interface OtherApi {

	@GET("/v1/commons/notification")
	Call<NotificationListResponse> getListNotification(@QueryMap Map<String, String> queries);

	@PUT("/v1/commons/notification")
	Call<BaseResponse> postStatusNotification();

	@GET("/FreeXu/public/other/local-pushs")
	Call<LocalPushListResponse> getLocalPushs(); // TODO anhdt

	@GET("/v1/others/remote_config")
	Call<RemoteConfigResponse> getRemoteConfig();

}
