# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\AndroidSDK\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

##---------------Begin: proguard configuration for OneSignal  ----------

-dontwarn com.onesignal.**

-keep class com.google.android.gms.common.api.GoogleApiClient {
    void connect();
    void disconnect();
}

-keep public interface android.app.OnActivityPausedListener {*;}

##---------------End: proguard configuration for OneSignal  ----------

##---------------Begin: proguard configuration for Retrofit  ----------

# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

#-dontwarn retrofit2.**
#-keep class retrofit2.** { *; }
#-keepattributes Signature
#-keepattributes Exceptions
-dontwarn okio.**

##---------------End: proguard configuration for Retrofit  ----------

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keepclassmembers class vn.lovemoney.app.model.** { <fields>; }

##---------------End: proguard configuration for Gson  ----------

##---------------Begin: proguard configuration for Picasso  ----------

-dontwarn okhttp3.**
-dontwarn com.squareup.okhttp.**

##---------------End: proguard configuration for Picasso  ----------

##---------------Begin: proguard configuration for EventBus  ----------

-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}

-keepclassmembers enum org.greenrobot.eventbus.** {
    <fields>;
}

-keep class vn.lovemoney.app.z.t { *; }
##---------------End: proguard configuration for EventBus  ----------

-dontwarn com.immersion.**
-dontnote com.immersion.**

-keep class com.vungle.** { public *; }
-keep class javax.inject.*
-keep class dagger.*

-keepclassmembers class * implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator *;
}
-keep class com.supersonic.mediationsdk.** { *;}
-keep class com.supersonic.adapters.** { *;}

-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception

# For communication with AdColony's WebView
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# For removing warnings due to lack of Multi-Window support
-dontwarn android.app.Activity